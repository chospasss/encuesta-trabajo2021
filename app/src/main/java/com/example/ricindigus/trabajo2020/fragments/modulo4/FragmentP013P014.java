package com.example.ricindigus.trabajo2020.fragments.modulo4;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo4;
import com.example.ricindigus.trabajo2020.modelo.pojos.Residente;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InputFilterSoloLetras;
import com.example.ricindigus.trabajo2020.util.NumericKeyBoardTransformationMethod;


public class FragmentP013P014 extends FragmentPagina {
    String idEncuestado;
    Context context;
    String idInformante;
    String idHogar;
    String idVivienda;

//nuevos

    LinearLayout layout_d_p013,layout_d_p014,layout_d_p014_a,layout_d_p014_b;

    RadioGroup radiogroup_d_P013,radiogroup_d_P014,radiogroup_d_P014A,radiogroup_d_P014B;

    //nuevos
    private String  p313;
    private String  p314;
    private String  p314a;
    private String  p314b;
    private String  obs_d;



    public FragmentP013P014() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentP013P014(String idEncuestado, Context context) {
        this.idEncuestado = idEncuestado;
        this.context = context;
        Data data = new Data(context);
        data.open();
        Residente residente = data.getResidente(idEncuestado);
        idHogar = residente.getId_hogar();
        idVivienda = residente.getId_vivienda();
        idInformante = "";
        data.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_d_p013_p014, container, false);

        //nuevos
        layout_d_p013 = rootView.findViewById(R.id.layout_d_p013);
        layout_d_p014 = rootView.findViewById(R.id.layout_d_p014);
        layout_d_p014_a = rootView.findViewById(R.id.layout_d_p014_a);
        layout_d_p014_b = rootView.findViewById(R.id.layout_d_p014_b);

        radiogroup_d_P013 = rootView.findViewById(R.id.radiogroup_d_P013);
        radiogroup_d_P014 = rootView.findViewById(R.id.radiogroup_d_P014);
        radiogroup_d_P014A = rootView.findViewById(R.id.radiogroup_d_P014A);
        radiogroup_d_P014B = rootView.findViewById(R.id.radiogroup_d_P014B);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        llenarVista();
        cargarDatos();
    }



    @Override
    public void guardarDatos() {
        Data data = new Data(context);
        data.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo4_id_informante,idInformante);
        contentValues.put(SQLConstantes.seccion_d_p313,p313);
        contentValues.put(SQLConstantes.seccion_d_p314,p314);
        contentValues.put(SQLConstantes.seccion_d_p314a,p314a);
        contentValues.put(SQLConstantes.seccion_d_p314b,p314b);
        contentValues.put(SQLConstantes.seccion_d_obs_d,obs_d);


        data.actualizarElemento(getNombreTabla(),contentValues,idEncuestado);
        //Ya valido y guardo correctamente el fragment, ahora actualizamos el valor de la cobertura del fragment a correcto(1)
        data.actualizarValor(SQLConstantes.tablacoberturafragments,SQLConstantes.cobertura_fragments_cp408p410,"1",idEncuestado);
        //verificamos la cobertura del capitulo y actualizamos su valor de cobertura.
        if (verificarCoberturaCapitulo()) data.actualizarValor(getNombreTabla(),SQLConstantes.modulo4_COB400,"1",idEncuestado);
        else data.actualizarValor(getNombreTabla(),SQLConstantes.modulo4_COB400,"0",idEncuestado);
        data.actualizarValor(SQLConstantes.tablaresidentes,SQLConstantes.residentes_encuestado_cobertura,"0",idEncuestado);
        data.close();
    }

    @Override
    public void llenarVariables() {
        //nuevos

        p313 = radiogroup_d_P013.indexOfChild(radiogroup_d_P013.findViewById(radiogroup_d_P013.getCheckedRadioButtonId()))+"";
        p314 = radiogroup_d_P014.indexOfChild(radiogroup_d_P014.findViewById(radiogroup_d_P014.getCheckedRadioButtonId()))+"";
        p314a = radiogroup_d_P014A.indexOfChild(radiogroup_d_P014A.findViewById(radiogroup_d_P014A.getCheckedRadioButtonId()))+"";
        p314b = radiogroup_d_P014B.indexOfChild(radiogroup_d_P014B.findViewById(radiogroup_d_P014B.getCheckedRadioButtonId()))+"";

        //fin nuevos

    }

    @Override
    public void cargarDatos() {

        Data data = new Data(context);
        data.open();
        if (data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo4 modulo4 = data.getModulo4(idEncuestado);

            //nuevos

            if( !modulo4.getP313().equals("-1") && !modulo4.getP313().equals(""))((RadioButton)radiogroup_d_P013.getChildAt(Integer.parseInt(modulo4.getP313()))).setChecked(true);
            if( !modulo4.getP314().equals("-1") && !modulo4.getP314().equals(""))((RadioButton)radiogroup_d_P014.getChildAt(Integer.parseInt(modulo4.getP314()))).setChecked(true);
            if( !modulo4.getP314a().equals("-1") && !modulo4.getP314a().equals(""))((RadioButton)radiogroup_d_P014A.getChildAt(Integer.parseInt(modulo4.getP314a()))).setChecked(true);
            if( !modulo4.getP314b().equals("-1") && !modulo4.getP314b().equals(""))((RadioButton)radiogroup_d_P014B.getChildAt(Integer.parseInt(modulo4.getP314b()))).setChecked(true);

            //fin de nuevos
        }

        data.close();

    }

    @Override
    public void llenarVista() {

    }

    @Override
    public boolean validarDatos() {
        llenarVariables();
        return true;
    }

    @Override
    public String getNombreTabla() {
        return SQLConstantes.tablamodulo4;
    }

    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void configurarEditText(final EditText editText, final View view, int tipo,int longitud){
        switch (tipo){
            case 0:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud), new InputFilterSoloLetras()});break;
            case 1:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud)});break;
            case 2:editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)});
                editText.setTransformationMethod(new NumericKeyBoardTransformationMethod());break;
        }

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ocultarTeclado(editText);
                    view.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void controlarEspecifiqueRadio(RadioGroup group, int checkedId, int opcionEsp, EditText editTextEspecifique) {
        int seleccionado = group.indexOfChild(group.findViewById(checkedId));
        if(seleccionado == opcionEsp){
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_enabled);
            editTextEspecifique.setEnabled(true);
        }else{
            editTextEspecifique.setText("");
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_disabled);
            editTextEspecifique.setEnabled(false);
        }
    }

    public void ocultarOtrosLayouts(){

    }

    public boolean verificarCoberturaCapitulo(){

        return true;
    }
}
