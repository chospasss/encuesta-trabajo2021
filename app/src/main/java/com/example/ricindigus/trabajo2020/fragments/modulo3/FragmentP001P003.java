package com.example.ricindigus.trabajo2020.fragments.modulo3;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo3;
import com.example.ricindigus.trabajo2020.modelo.pojos.Residente;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InputFilterSoloLetras;
import com.example.ricindigus.trabajo2020.util.NumericKeyBoardTransformationMethod;


public class FragmentP001P003 extends FragmentPagina {
    String idEncuestado;
    Context context;
    String idInformante;
    String idHogar;
    String idVivienda;

    //agregado
    TextView textView_c_p001_2o3, textView_c_p001_2o3_g;
    CheckBox checkbox_c_p003_1, checkbox_c_p003_2, checkbox_c_p003_3, checkbox_c_p003_4, checkbox_c_p003_5, checkbox_c_p003_6,
            checkbox_c_p003_7, checkbox_c_p003_8, checkbox_c_p003_9, checkbox_c_p003_10, checkbox_c_p003_11;
    RadioGroup radiogroup_c_p001, radiogroup_c_p001_ce, radiogroup_c_p002;
    EditText edittext_c_p001_2o3, edittext_c_p003_11_O, edittext_c_p001_2o3_g;
    LinearLayout layout_c_p001,layout_c_001_subpregunta,layout_c_p002,layout_c_p003;


    //agregado//
    int p301;
    String p301_203;
    String p301_203_g;
    int p301_ce;
    int p302;
    String p303_1;
    String p303_2;
    String p303_3;
    String p303_4;
    String p303_5;
    String p303_6;
    String p303_7;
    String p303_8;
    String p303_9;
    String p303_10;
    String p303_11;
    String p303_11_O;


    public FragmentP001P003() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentP001P003(String idEncuestado, Context context) {
        this.idEncuestado = idEncuestado;
        this.context = context;

        Data data = new Data(context);
        data.open();
        Residente residente = data.getResidente(idEncuestado);
        idHogar = residente.getId_hogar();
        idVivienda = residente.getId_vivienda();
        idInformante = "";
        data.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_c_p001_p003, container, false);

        layout_c_p001 = (LinearLayout) rootView.findViewById(R.id.layout_c_p001);
        layout_c_001_subpregunta = (LinearLayout) rootView.findViewById(R.id.layout_c_001_subpregunta);
        layout_c_p002 = (LinearLayout) rootView.findViewById(R.id.layout_c_p002);
        layout_c_p003 = (LinearLayout) rootView.findViewById(R.id.layout_c_p003);

        //agregado//
        layout_c_p001 = (LinearLayout) rootView.findViewById(R.id.layout_c_p001);
        layout_c_001_subpregunta = (LinearLayout) rootView.findViewById(R.id.layout_c_001_subpregunta);
        layout_c_p002 = (LinearLayout) rootView.findViewById(R.id.layout_c_p002);
        layout_c_p003 = (LinearLayout) rootView.findViewById(R.id.layout_c_p003);

        radiogroup_c_p001 = (RadioGroup) rootView.findViewById(R.id.radiogroup_c_P001);
        textView_c_p001_2o3 = (TextView) rootView.findViewById(R.id.textView_c_P001_2o3);
        edittext_c_p001_2o3 = (EditText) rootView.findViewById(R.id.edittext_c_P001_2o3);
        textView_c_p001_2o3_g = (TextView) rootView.findViewById(R.id.textView_c_P001_2o3);
        edittext_c_p001_2o3_g = (EditText) rootView.findViewById(R.id.edittext_c_P001_2o3);
        radiogroup_c_p001_ce = (RadioGroup) rootView.findViewById(R.id.radiogroup_c_P001_ce);

        radiogroup_c_p002 = (RadioGroup) rootView.findViewById(R.id.radiogroup_c_P002);

        checkbox_c_p003_1 = (CheckBox) rootView.findViewById(R.id.checkbox_c_P003_1);
        checkbox_c_p003_2 = (CheckBox) rootView.findViewById(R.id.checkbox_c_P003_2);
        checkbox_c_p003_3 = (CheckBox) rootView.findViewById(R.id.checkbox_c_P003_3);
        checkbox_c_p003_4 = (CheckBox) rootView.findViewById(R.id.checkbox_c_P003_4);
        checkbox_c_p003_5 = (CheckBox) rootView.findViewById(R.id.checkbox_c_P003_5);
        checkbox_c_p003_6 = (CheckBox) rootView.findViewById(R.id.checkbox_c_P003_6);
        checkbox_c_p003_7 = (CheckBox) rootView.findViewById(R.id.checkbox_c_P003_7);
        checkbox_c_p003_8 = (CheckBox) rootView.findViewById(R.id.checkbox_c_P003_8);
        checkbox_c_p003_9 = (CheckBox) rootView.findViewById(R.id.checkbox_c_P003_9);
        checkbox_c_p003_10 = (CheckBox) rootView.findViewById(R.id.checkbox_c_P003_10);
        checkbox_c_p003_11 = (CheckBox) rootView.findViewById(R.id.checkbox_c_P003_11);
        edittext_c_p003_11_O = (EditText) rootView.findViewById(R.id.edittext_c_p003_sp11_O);
        //agregado//

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Data data =  new Data(context);
        data.open();

        //agregado
        controlarChecked(checkbox_c_p003_11,edittext_c_p003_11_O);

        llenarVista();
        cargarDatos();
    }



    @Override
    public void guardarDatos() {
        Data data = new Data(context);
        data.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo3_id_informante,idInformante);
        //agregado//
        contentValues.put(SQLConstantes.seccion_c_p301_nivel,p301);
        contentValues.put(SQLConstantes.seccion_c_p301_anio,p301_203);
        contentValues.put(SQLConstantes.seccion_c_p301_grado,p301_ce);
        contentValues.put(SQLConstantes.seccion_c_p301a,p301_203_g);
        contentValues.put(SQLConstantes.seccion_c_p302,p302);
        contentValues.put(SQLConstantes.seccion_c_p303_1,p303_1);
        contentValues.put(SQLConstantes.seccion_c_p303_2,p303_2);
        contentValues.put(SQLConstantes.seccion_c_p303_3,p303_3);
        contentValues.put(SQLConstantes.seccion_c_p303_4,p303_4);
        contentValues.put(SQLConstantes.seccion_c_p303_5,p303_5);
        contentValues.put(SQLConstantes.seccion_c_p303_6,p303_6);
        contentValues.put(SQLConstantes.seccion_c_p303_7,p303_7);
        contentValues.put(SQLConstantes.seccion_c_p303_8,p303_8);
        contentValues.put(SQLConstantes.seccion_c_p303_9,p303_9);
        contentValues.put(SQLConstantes.seccion_c_p303_10,p303_10);
        contentValues.put(SQLConstantes.seccion_c_p303_11,p303_11);
        contentValues.put(SQLConstantes.seccion_c_p303_11_o,p303_11_O);
        //fin agregado//
        if(!data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo3 modulo3 = new Modulo3();
            modulo3.set_id(idEncuestado);
            modulo3.setIdHogar(idHogar);
            modulo3.setIdVivienda(idVivienda);
            data.insertarElemento(getNombreTabla(),modulo3.toValues());
        }
        data.actualizarElemento(getNombreTabla(),contentValues,idEncuestado);
        //Ya valido y guardo correctamente el fragment, ahora actualizamos el valor de la cobertura del fragment a correcto(1)
        //data.actualizarValor(SQLConstantes.tablacoberturafragments,SQLConstantes.cobertura_fragments_cp301p305,"1",idEncuestado);
        //ocultamos o mostramos preguntas o fragments
        //ocultarOtrosLayouts();
        //verificamos la cobertura del capitulo y actualizamos su valor de cobertura.
        if (verificarCoberturaCapitulo()) data.actualizarValor(getNombreTabla(),SQLConstantes.modulo3_COB300,"1",idEncuestado);
        else data.actualizarValor(getNombreTabla(),SQLConstantes.modulo3_COB300,"0",idEncuestado);
        data.actualizarValor(SQLConstantes.tablaresidentes,SQLConstantes.residentes_encuestado_cobertura,"0",idEncuestado);
        data.close();
    }

    @Override
    public void llenarVariables() {
        //agregado//
        p301 = radiogroup_c_p001.indexOfChild(radiogroup_c_p001.findViewById(radiogroup_c_p001.getCheckedRadioButtonId()));
        p301_203 = edittext_c_p001_2o3.getText().toString();
        p301_203_g = edittext_c_p001_2o3_g.getText().toString();
        p301_ce = radiogroup_c_p001_ce.indexOfChild(radiogroup_c_p001_ce.findViewById(radiogroup_c_p001_ce.getCheckedRadioButtonId()));

        p302 = radiogroup_c_p002.indexOfChild(radiogroup_c_p002.findViewById(radiogroup_c_p002.getCheckedRadioButtonId()));

        if(checkbox_c_p003_1.isChecked()) p303_1 = "1"; else p303_1 = "0";
        if(checkbox_c_p003_2.isChecked()) p303_2 = "1"; else p303_2 = "0";
        if(checkbox_c_p003_3.isChecked()) p303_3 = "1"; else p303_3 = "0";
        if(checkbox_c_p003_4.isChecked()) p303_4 = "1"; else p303_4 = "0";
        if(checkbox_c_p003_5.isChecked()) p303_5 = "1"; else p303_5 = "0";
        if(checkbox_c_p003_6.isChecked()) p303_6 = "1"; else p303_6 = "0";
        if(checkbox_c_p003_7.isChecked()) p303_7 = "1"; else p303_7 = "0";
        if(checkbox_c_p003_8.isChecked()) p303_8 = "1"; else p303_8 = "0";
        if(checkbox_c_p003_9.isChecked()) p303_9 = "1"; else p303_9 = "0";
        if(checkbox_c_p003_10.isChecked()) p303_10 = "1"; else p303_10 = "0";
        if(checkbox_c_p003_11.isChecked()) p303_11 = "1"; else p303_11 = "0";
        p303_11_O = edittext_c_p003_11_O.getText().toString();
    }

    @Override
    public void cargarDatos() {
        Data data = new Data(context);
        data.open();
        if (data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo3 modulo3 = data.getModulo3(idEncuestado);

//agregado//
            if(!modulo3.getP301().equals("-1") && !modulo3.getP301().equals(""))((RadioButton)radiogroup_c_p001.getChildAt(Integer.parseInt(modulo3.getP301()))).setChecked(true);
            edittext_c_p001_2o3.setText(modulo3.getP301_203());
            edittext_c_p001_2o3_g.setText(modulo3.getP301_203_g());
            if(!modulo3.getP301_ce().equals("-1") && !modulo3.getP301_ce().equals(""))((RadioButton)radiogroup_c_p001_ce.getChildAt(Integer.parseInt(modulo3.getP301_ce()))).setChecked(true);
            if(!modulo3.getP302().equals("-1") && !modulo3.getP302().equals(""))((RadioButton)radiogroup_c_p002.getChildAt(Integer.parseInt(modulo3.getP302()))).setChecked(true);
            if(modulo3.getP303_1().equals("1")) checkbox_c_p003_1.setChecked(true);
            if(modulo3.getP303_2().equals("1")) checkbox_c_p003_2.setChecked(true);
            if(modulo3.getP303_3().equals("1")) checkbox_c_p003_3.setChecked(true);
            if(modulo3.getP303_4().equals("1")) checkbox_c_p003_4.setChecked(true);
            if(modulo3.getP303_5().equals("1")) checkbox_c_p003_5.setChecked(true);
            if(modulo3.getP303_6().equals("1")) checkbox_c_p003_6.setChecked(true);
            if(modulo3.getP303_7().equals("1")) checkbox_c_p003_7.setChecked(true);
            if(modulo3.getP303_8().equals("1")) checkbox_c_p003_8.setChecked(true);
            if(modulo3.getP303_9().equals("1")) checkbox_c_p003_9.setChecked(true);
            if(modulo3.getP303_10().equals("1")) checkbox_c_p003_10.setChecked(true);
            if(modulo3.getP303_11().equals("1")) checkbox_c_p003_11.setChecked(true);
            edittext_c_p003_11_O.setText(modulo3.getP303_11_O());

        }
        data.close();


    }

    @Override
    public void llenarVista() {

    }

    @Override
    public boolean validarDatos() {
        llenarVariables();
        return true;
    }

    @Override
    public String getNombreTabla() {
        return SQLConstantes.tablamodulo3;
    }

    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void configurarEditText(final EditText editText, final View view, int tipo,int longitud){
        switch (tipo){
            case 0:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud), new InputFilterSoloLetras()});break;
            case 1:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud)});break;
            case 2:editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)});
                editText.setTransformationMethod(new NumericKeyBoardTransformationMethod());break;
        }

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ocultarTeclado(editText);
                    view.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void controlarEspecifiqueRadio(RadioGroup group, int checkedId, int opcionEsp, EditText editTextEspecifique) {
        int seleccionado = group.indexOfChild(group.findViewById(checkedId));
        if(seleccionado == opcionEsp){
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_enabled);
            editTextEspecifique.setEnabled(true);
        }else{
            editTextEspecifique.setText("");
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_disabled);
            editTextEspecifique.setEnabled(false);
        }
    }

    public void ocultarOtrosLayouts(){

    }

    public boolean verificarCoberturaCapitulo(){

        return true;
    }



    public void controlarChecked(CheckBox checkBox,final EditText editText){
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    editText.setBackgroundResource(R.drawable.input_text_enabled);
                    editText.setEnabled(true);
                }else{
                    editText.setText("");
                    editText.setBackgroundResource(R.drawable.input_text_disabled);
                    editText.setEnabled(false);
                }
            }
        });
    }
}
