package com.example.ricindigus.trabajo2020.fragments.modulo5;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo5;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InputFilterSoloLetras;
import com.example.ricindigus.trabajo2020.util.NumericKeyBoardTransformationMethod;


public class FragmentP021P025 extends FragmentPagina {
    String idEncuestado;
    Context context;
    String idInformante;
    String idHogar;
    String idVivienda;
    LinearLayout layout_e_p021;
    LinearLayout layout_e_p022;
    LinearLayout layout_e_p023;
    LinearLayout layout_e_p024;
    LinearLayout layout_e_p025;
    RadioGroup rg_e_P021;
    Spinner    sp_e_P022_m;
    Spinner    sp_e_P022_a;
    Spinner    sp_e_P023_m;
    Spinner    sp_e_P023_a;
    CheckBox   cb_e_P023;
    AutoCompleteTextView et_e_P024;
    TextView   et_e_P024_dist;
    TextView   et_e_P024_prov;
    TextView   et_e_P024_dept;
    Spinner    sp_e_P025_d;
    Spinner    sp_e_P025_h;

    String p021;
    String p022_m;
    String p022_a;
    String p022_n;
    String p023_m;
    String p023_a;
    String p023_t;
    String p023_n;
    String p024;
    String p024a;
    String p024a_cod;
    String p024b;
    String p024b_cod;
    String p024c;
    String p024c_cod;
    String p024d;
    String p024d_cod;
    String p025_d;
    String p025_h;


    public FragmentP021P025() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentP021P025(String idEncuestado, Context context) {
        this.idEncuestado = idEncuestado;
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_e_p021_p025, container, false);
        layout_e_p021 = (LinearLayout) rootView.findViewById(R.id.layout_e_p021);
        layout_e_p022 = (LinearLayout) rootView.findViewById(R.id.layout_e_p022);
        layout_e_p023 = (LinearLayout) rootView.findViewById(R.id.layout_e_p023);
        layout_e_p024 = (LinearLayout) rootView.findViewById(R.id.layout_e_p024);
        layout_e_p025 = (LinearLayout) rootView.findViewById(R.id.layout_e_p025);

         rg_e_P021      = rootView.findViewById(R.id.rg_e_p021);
         sp_e_P022_m    = rootView.findViewById(R.id.sp_e_p022_m);
         sp_e_P022_a    = rootView.findViewById(R.id.sp_e_p022_a);
         sp_e_P023_m    = rootView.findViewById(R.id.sp_e_p023_m);
         sp_e_P023_a    = rootView.findViewById(R.id.sp_e_p023_a);
         cb_e_P023      = rootView.findViewById(R.id.cb_e_p023_ta);
         et_e_P024      = rootView.findViewById(R.id.seccion_e_p024_autocompletetextview);
         et_e_P024_dist = rootView.findViewById(R.id.seccion_e_p024_txtDistrito);
         et_e_P024_prov = rootView.findViewById(R.id.seccion_e_p024_txtProvincia);
         et_e_P024_dept = rootView.findViewById(R.id.seccion_e_p024_txtDepartamento);
         sp_e_P025_d = rootView.findViewById(R.id.sp_e_p025_m);
         sp_e_P025_h = rootView.findViewById(R.id.sp_e_p025_a);
         return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        llenarVista();
        cargarDatos();
    }

    @Override
    public void guardarDatos() {
        Data data = new Data(context);
        data.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo5_id_informante,idInformante);
        contentValues.put(SQLConstantes.seccion_e_p321,p021);
        contentValues.put(SQLConstantes.seccion_e_p322_a,p022_a);
        contentValues.put(SQLConstantes.seccion_e_p322_m,p022_m);
        contentValues.put(SQLConstantes.seccion_e_p323_a,p023_a);
        contentValues.put(SQLConstantes.seccion_e_p323_m,p023_m);
        contentValues.put(SQLConstantes.seccion_e_p323_n,p023_n);
        contentValues.put(SQLConstantes.seccion_e_p324,p024);
        contentValues.put(SQLConstantes.seccion_e_p325_d,p025_d);
        contentValues.put(SQLConstantes.seccion_e_p325_h,p025_h);

        data.actualizarElemento(getNombreTabla(),contentValues,idEncuestado);
        data.close();
    }

    @Override
    public void llenarVariables() {
        p021 = rg_e_P021.indexOfChild(rg_e_P021.findViewById(rg_e_P021.getCheckedRadioButtonId()))+"";
        p022_m = sp_e_P022_a.getSelectedItemPosition()+"";
        p022_a = sp_e_P022_m.getSelectedItemPosition()+"";
        p023_m = sp_e_P023_m.getSelectedItemPosition()+"";
        p023_a = sp_e_P023_a.getSelectedItemPosition()+"";
        if (cb_e_P023.isChecked()) p023_t= "1"; else p023_t="0";
        p024   = et_e_P024.getText().toString();
        p025_d = sp_e_P025_h.getSelectedItemPosition()+"";
        p025_h = sp_e_P022_m.getSelectedItemPosition()+"";
    }

    @Override
    public void cargarDatos() {
        Data data = new Data(context);
        data.open();
        if (data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo5 modulo5 = data.getModulo5(idEncuestado);
            if(!modulo5.getP321().equals("-1") && !modulo5.getP321().equals(""))((RadioButton)rg_e_P021.getChildAt(Integer.parseInt(modulo5.getP321()))).setChecked(true);
            if(!modulo5.getP322_m().equals("")){sp_e_P022_m.setSelection(Integer.parseInt(modulo5.getP322_m()));}
            if(!modulo5.getP322_a().equals("")){sp_e_P022_a.setSelection(Integer.parseInt(modulo5.getP322_a()));}
            if (modulo5.getP322_n().equals("1")) cb_e_P023.setChecked(true);
            if(!modulo5.getP323_m().equals("")){sp_e_P023_m.setSelection(Integer.parseInt(modulo5.getP323_m()));}
            if(!modulo5.getP323_a().equals("")){sp_e_P023_a.setSelection(Integer.parseInt(modulo5.getP323_a()));}
            et_e_P024.setText(modulo5.getP324());
            if(!modulo5.getP325_d().equals("")){sp_e_P025_d.setSelection(Integer.parseInt(modulo5.getP325_d()));}
            if(!modulo5.getP325_h().equals("")){sp_e_P025_h.setSelection(Integer.parseInt(modulo5.getP325_h()));}
        }
        data.close();
    }

    @Override
    public void llenarVista() {

    }

    @Override
    public boolean validarDatos() {
        llenarVariables();
        return true;
    }

    @Override
    public String getNombreTabla() {
        return SQLConstantes.tablamodulo5;
    }

    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void configurarEditText(final EditText editText, final View view, int tipo,int longitud){
        switch (tipo){
            case 0:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud), new InputFilterSoloLetras()});break;
            case 1:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud)});break;
            case 2:editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)});
                editText.setTransformationMethod(new NumericKeyBoardTransformationMethod());break;
        }

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ocultarTeclado(editText);
                    view.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void controlarEspecifiqueRadio(RadioGroup group, int checkedId, int opcionEsp, EditText editTextEspecifique) {
        int seleccionado = group.indexOfChild(group.findViewById(checkedId));
        if(seleccionado == opcionEsp){
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_enabled);
            editTextEspecifique.setEnabled(true);
        }else{
            editTextEspecifique.setText("");
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_disabled);
            editTextEspecifique.setEnabled(false);
        }
    }

    public void ocultarOtrosLayouts(){

    }

    public boolean verificarCoberturaCapitulo(){

        return true;
    }
}
