package com.example.ricindigus.trabajo2020.modelo.pojos;

import android.content.ContentValues;

import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;

public class Modulo1H {
    private String _id;
    private String idVivienda;
    // private String c1_p108;
    // private String c1_p108_o;
    // private String c1_p109;
    // private String c1_p109_o;
    // private String c1_p110;
    //private String c1_p110_o;
    //private String c1_p111;
    //private String c1_p111_o;
    // private String c1_p112;
    //private String c1_p112_o;
    //private String c1_p113_1;
    //private String c1_p113_2;
    //private String c1_p113_3;
    //private String c1_p113_4;
    //private String c1_p113_5;
    //private String c1_p113_6;
    //private String c1_p113_7;
    //private String c1_p113_8;
    //private String c1_p113_9;
    //private String c1_p113_7_o;
    // private String c1_p113_8_o;
    //private String c1_p113_9_o;
    private String COB100B;
    //nuevos
    private String p107;
    private String p108_1;
    private String p108_2;
    private String p108_3;
    private String p108_4;
    private String p108_5;
    private String p108_6;
    private String p108_7;
    private String p108_8;
    private String p108_9;
    private String p108_10;
    private String p108_11;
    private String p108_12;
    private String p108_12_o;
    private String p109_1;
    private String p110_1;
    private String p109_2;
    private String p110_2;
    private String p109_3;
    private String p110_3;
    private String p109_4;
    private String p110_4;
    private String p109_5;
    private String p110_5;
    private String p109_6;
    private String p110_6;
    private String p109_7;
    private String p110_7;
    private String p109_8;
    private String p110_8;
    private String p109_9;
    private String p109_9_o;
    private String p110_9;
    private String p109_10;
    private String p109_10_o;
    private String p110_10;
    private String obs_a;

    public Modulo1H() {
        _id="";
        idVivienda="";
        // c1_p108="";
        //c1_p108_o="";
        // c1_p109="";
        // c1_p109_o="";
        // c1_p110="";
        // c1_p110_o="";
        // c1_p111="";
        // c1_p111_o="";
        // c1_p112="";
        //  c1_p112_o="";
        // c1_p113_1="";
        // c1_p113_2="";
        //  c1_p113_3="";
        // c1_p113_4="";
        // c1_p113_5="";
        // c1_p113_6="";
        // c1_p113_7="";
        // c1_p113_8="";
        // c1_p113_9="";
        // c1_p113_7_o="";
        // c1_p113_8_o="";
        // c1_p113_9_o="";
        COB100B = "2";
        //nuevos
        p107="";
        p108_1="";
        p108_2="";
        p108_3="";
        p108_4="";
        p108_5="";
        p108_6="";
        p108_7="";
        p108_8="";
        p108_9="";
        p108_10="";
        p108_11="";
        p108_12="";
        p108_12_o="";
        p109_1="";
        p110_1="";
        p109_2="";
        p110_2="";
        p109_3="";
        p110_3="";
        p109_4="";
        p110_4="";
        p109_5="";
        p110_5="";
        p109_6="";
        p110_6="";
        p109_7="";
        p110_7="";
        p109_8="";
        p110_8="";
        p109_9="";
        p109_9_o="";
        p110_9="";
        p109_10="";
        p109_10_o="";
        p110_10="";
        obs_a="";
    }

    public String getCOB100B() {
        return COB100B;
    }

    public void setCOB100B(String COB100B) {
        this.COB100B = COB100B;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getIdVivienda() {
        return idVivienda;
    }

    public void setIdVivienda(String idVivienda) {
        this.idVivienda = idVivienda;
    }

    /*
     public String getC1_p108() {
        return c1_p108;
    }

    public void setC1_p108(String c1_p108) {
        this.c1_p108 = c1_p108;
    }

    public String getC1_p108_o() {
        return c1_p108_o;
    }

    public void setC1_p108_o(String c1_p108_o) {
        this.c1_p108_o = c1_p108_o;
    }

    public String getC1_p109() {
        return c1_p109;
    }

    public void setC1_p109(String c1_p109) {
        this.c1_p109 = c1_p109;
    }

    public String getC1_p109_o() {
        return c1_p109_o;
    }

    public void setC1_p109_o(String c1_p109_o) {
        this.c1_p109_o = c1_p109_o;
    }

    public String getC1_p110() {
        return c1_p110;
    }

    public void setC1_p110(String c1_p110) {
        this.c1_p110 = c1_p110;
    }

    public String getC1_p110_o() {
        return c1_p110_o;
    }

    public void setC1_p110_o(String c1_p110_o) {
        this.c1_p110_o = c1_p110_o;
    }

    public String getC1_p111() {
        return c1_p111;
    }

    public void setC1_p111(String c1_p111) {
        this.c1_p111 = c1_p111;
    }

    public String getC1_p111_o() {
        return c1_p111_o;
    }

    public void setC1_p111_o(String c1_p111_o) {
        this.c1_p111_o = c1_p111_o;
    }

    public String getC1_p112() {
        return c1_p112;
    }

    public void setC1_p112(String c1_p112) {
        this.c1_p112 = c1_p112;
    }

    public String getC1_p112_o() {
        return c1_p112_o;
    }

    public void setC1_p112_o(String c1_p112_o) {
        this.c1_p112_o = c1_p112_o;
    }

    public String getC1_p113_1() {
        return c1_p113_1;
    }

    public void setC1_p113_1(String c1_p113_1) {
        this.c1_p113_1 = c1_p113_1;
    }

    public String getC1_p113_2() {
        return c1_p113_2;
    }

    public void setC1_p113_2(String c1_p113_2) {
        this.c1_p113_2 = c1_p113_2;
    }

    public String getC1_p113_3() {
        return c1_p113_3;
    }

    public void setC1_p113_3(String c1_p113_3) {
        this.c1_p113_3 = c1_p113_3;
    }

    public String getC1_p113_4() {
        return c1_p113_4;
    }

    public void setC1_p113_4(String c1_p113_4) {
        this.c1_p113_4 = c1_p113_4;
    }

    public String getC1_p113_5() {
        return c1_p113_5;
    }

    public void setC1_p113_5(String c1_p113_5) {
        this.c1_p113_5 = c1_p113_5;
    }

    public String getC1_p113_6() {
        return c1_p113_6;
    }

    public void setC1_p113_6(String c1_p113_6) {
        this.c1_p113_6 = c1_p113_6;
    }

    public String getC1_p113_7() {
        return c1_p113_7;
    }

    public void setC1_p113_7(String c1_p113_7) {
        this.c1_p113_7 = c1_p113_7;
    }

    public String getC1_p113_8() {
        return c1_p113_8;
    }

    public void setC1_p113_8(String c1_p113_8) {
        this.c1_p113_8 = c1_p113_8;
    }

    public String getC1_p113_9() {
        return c1_p113_9;
    }

    public void setC1_p113_9(String c1_p113_9) {
        this.c1_p113_9 = c1_p113_9;
    }

    public String getC1_p113_7_o() {
        return c1_p113_7_o;
    }

    public void setC1_p113_7_o(String c1_p113_7_o) {
        this.c1_p113_7_o = c1_p113_7_o;
    }

    public String getC1_p113_8_o() {
        return c1_p113_8_o;
    }

    public void setC1_p113_8_o(String c1_p113_8_o) {
        this.c1_p113_8_o = c1_p113_8_o;
    }

    public String getC1_p113_9_o() {
        return c1_p113_9_o;
    }

    public void setC1_p113_9_o(String c1_p113_9_o) {
        this.c1_p113_9_o = c1_p113_9_o;
    }

     */


    public String getP107() {
        return p107;
    }

    public void setP107(String p107) {
        this.p107 = p107;
    }

    public String getP108_1() {
        return p108_1;
    }

    public void setP108_1(String p108_1) {
        this.p108_1 = p108_1;
    }

    public String getP108_2() {
        return p108_2;
    }

    public void setP108_2(String p108_2) {
        this.p108_2 = p108_2;
    }

    public String getP108_3() {
        return p108_3;
    }

    public void setP108_3(String p108_3) {
        this.p108_3 = p108_3;
    }

    public String getP108_4() {
        return p108_4;
    }

    public void setP108_4(String p108_4) {
        this.p108_4 = p108_4;
    }

    public String getP108_5() {
        return p108_5;
    }

    public void setP108_5(String p108_5) {
        this.p108_5 = p108_5;
    }

    public String getP108_6() {
        return p108_6;
    }

    public void setP108_6(String p108_6) {
        this.p108_6 = p108_6;
    }

    public String getP108_7() {
        return p108_7;
    }

    public void setP108_7(String p108_7) {
        this.p108_7 = p108_7;
    }

    public String getP108_8() {
        return p108_8;
    }

    public void setP108_8(String p108_8) {
        this.p108_8 = p108_8;
    }

    public String getP108_9() {
        return p108_9;
    }

    public void setP108_9(String p108_9) {
        this.p108_9 = p108_9;
    }

    public String getP108_10() {
        return p108_10;
    }

    public void setP108_10(String p108_10) {
        this.p108_10 = p108_10;
    }

    public String getP108_11() {
        return p108_11;
    }

    public void setP108_11(String p108_11) {
        this.p108_11 = p108_11;
    }

    public String getP108_12() {
        return p108_12;
    }

    public void setP108_12(String p108_12) {
        this.p108_12 = p108_12;
    }

    public String getP108_12_o() {
        return p108_12_o;
    }

    public void setP108_12_o(String p108_12_o) {
        this.p108_12_o = p108_12_o;
    }

    public String getP109_1() {
        return p109_1;
    }

    public void setP109_1(String p109_1) {
        this.p109_1 = p109_1;
    }

    public String getP110_1() {
        return p110_1;
    }

    public void setP110_1(String p110_1) {
        this.p110_1 = p110_1;
    }

    public String getP109_2() {
        return p109_2;
    }

    public void setP109_2(String p109_2) {
        this.p109_2 = p109_2;
    }

    public String getP110_2() {
        return p110_2;
    }

    public void setP110_2(String p110_2) {
        this.p110_2 = p110_2;
    }

    public String getP109_3() {
        return p109_3;
    }

    public void setP109_3(String p109_3) {
        this.p109_3 = p109_3;
    }

    public String getP110_3() {
        return p110_3;
    }

    public void setP110_3(String p110_3) {
        this.p110_3 = p110_3;
    }

    public String getP109_4() {
        return p109_4;
    }

    public void setP109_4(String p109_4) {
        this.p109_4 = p109_4;
    }

    public String getP110_4() {
        return p110_4;
    }

    public void setP110_4(String p110_4) {
        this.p110_4 = p110_4;
    }

    public String getP109_5() {
        return p109_5;
    }

    public void setP109_5(String p109_5) {
        this.p109_5 = p109_5;
    }

    public String getP110_5() {
        return p110_5;
    }

    public void setP110_5(String p110_5) {
        this.p110_5 = p110_5;
    }

    public String getP109_6() {
        return p109_6;
    }

    public void setP109_6(String p109_6) {
        this.p109_6 = p109_6;
    }

    public String getP110_6() {
        return p110_6;
    }

    public void setP110_6(String p110_6) {
        this.p110_6 = p110_6;
    }

    public String getP109_7() {
        return p109_7;
    }

    public void setP109_7(String p109_7) {
        this.p109_7 = p109_7;
    }

    public String getP110_7() {
        return p110_7;
    }

    public void setP110_7(String p110_7) {
        this.p110_7 = p110_7;
    }

    public String getP109_8() {
        return p109_8;
    }

    public void setP109_8(String p109_8) {
        this.p109_8 = p109_8;
    }

    public String getP110_8() {
        return p110_8;
    }

    public void setP110_8(String p110_8) {
        this.p110_8 = p110_8;
    }

    public String getP109_9() {
        return p109_9;
    }

    public void setP109_9(String p109_9) {
        this.p109_9 = p109_9;
    }

    public String getP109_9_o() {
        return p109_9_o;
    }

    public void setP109_9_o(String p109_9_o) {
        this.p109_9_o = p109_9_o;
    }

    public String getP110_9() {
        return p110_9;
    }

    public void setP110_9(String p110_9) {
        this.p110_9 = p110_9;
    }

    public String getP109_10() {
        return p109_10;
    }

    public void setP109_10(String p109_10) {
        this.p109_10 = p109_10;
    }

    public String getP109_10_o() {
        return p109_10_o;
    }

    public void setP109_10_o(String p109_10_o) {
        this.p109_10_o = p109_10_o;
    }

    public String getP110_10() {
        return p110_10;
    }

    public void setP110_10(String p110_10) {
        this.p110_10 = p110_10;
    }

    public String getObs_a() {
        return obs_a;
    }

    public void setObs_a(String obs_a) {
        this.obs_a = obs_a;
    }

    public ContentValues toValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo1_h_id,_id);
        contentValues.put(SQLConstantes.modulo1_h_idVivienda,idVivienda);
        // contentValues.put(SQLConstantes.modulo1_h_c1_p108,c1_p108);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p108_o,c1_p108_o);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p109,c1_p109);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p109_o,c1_p109_o);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p110,c1_p110);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p110_o,c1_p110_o);
        // contentValues.put(SQLConstantes.modulo1_h_c1_p111,c1_p111);
        // contentValues.put(SQLConstantes.modulo1_h_c1_p111_o,c1_p111_o);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p112,c1_p112);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p112_o,c1_p112_o);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_1,c1_p113_1);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_2,c1_p113_2);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_3,c1_p113_3);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_4,c1_p113_4);
        // contentValues.put(SQLConstantes.modulo1_h_c1_p113_5,c1_p113_5);
        // contentValues.put(SQLConstantes.modulo1_h_c1_p113_6,c1_p113_6);
        // contentValues.put(SQLConstantes.modulo1_h_c1_p113_7,c1_p113_7);
        // contentValues.put(SQLConstantes.modulo1_h_c1_p113_8,c1_p113_8);
        // contentValues.put(SQLConstantes.modulo1_h_c1_p113_9,c1_p113_9);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_7_o,c1_p113_7_o);
        // contentValues.put(SQLConstantes.modulo1_h_c1_p113_8_o,c1_p113_8_o);
        // contentValues.put(SQLConstantes.modulo1_h_c1_p113_9_o,c1_p113_9_o);
        contentValues.put(SQLConstantes.modulo1_h_COB100B,COB100B);
        contentValues.put(SQLConstantes.seccion_a_p107, p107);
        contentValues.put(SQLConstantes.seccion_a_p108_1, p108_1);
        contentValues.put(SQLConstantes.seccion_a_p108_2, p108_2);
        contentValues.put(SQLConstantes.seccion_a_p108_3, p108_3);
        contentValues.put(SQLConstantes.seccion_a_p108_4, p108_4);
        contentValues.put(SQLConstantes.seccion_a_p108_5, p108_5);
        contentValues.put(SQLConstantes.seccion_a_p108_6, p108_6);
        contentValues.put(SQLConstantes.seccion_a_p108_7, p108_7);
        contentValues.put(SQLConstantes.seccion_a_p108_8, p108_8);
        contentValues.put(SQLConstantes.seccion_a_p108_9, p108_9);
        contentValues.put(SQLConstantes.seccion_a_p108_10, p108_10);
        contentValues.put(SQLConstantes.seccion_a_p108_11, p108_11);
        contentValues.put(SQLConstantes.seccion_a_p108_12, p108_12);
        contentValues.put(SQLConstantes.seccion_a_p108_12_o, p108_12_o);
        contentValues.put(SQLConstantes.seccion_a_p109_1, p109_1);
        contentValues.put(SQLConstantes.seccion_a_p109_2, p109_2);
        contentValues.put(SQLConstantes.seccion_a_p109_3, p109_3);
        contentValues.put(SQLConstantes.seccion_a_p109_4, p109_4);
        contentValues.put(SQLConstantes.seccion_a_p109_5, p109_5);
        contentValues.put(SQLConstantes.seccion_a_p109_6, p109_6);
        contentValues.put(SQLConstantes.seccion_a_p109_7, p109_7);
        contentValues.put(SQLConstantes.seccion_a_p109_8, p109_8);
        contentValues.put(SQLConstantes.seccion_a_p109_9, p109_9);
        contentValues.put(SQLConstantes.seccion_a_p109_9_o, p109_9_o);
        contentValues.put(SQLConstantes.seccion_a_p109_10, p109_10);
        contentValues.put(SQLConstantes.seccion_a_p109_10_o, p109_10_o);

        contentValues.put(SQLConstantes.seccion_a_obs_a, obs_a);
        return contentValues;
    }
}
