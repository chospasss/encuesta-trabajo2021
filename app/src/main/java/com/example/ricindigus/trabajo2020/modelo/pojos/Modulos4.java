package com.example.ricindigus.trabajo2020.modelo.pojos;

import android.content.ContentValues;

import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;

public class Modulos4 {

        private String id;
        private String id_vivienda;
        private String numero_hogar;
        private String numero_residente;
        private String inf_300;
        private String p307;
        private String p308a;
        private String p308a_cod;
        private String p308b;
        private String p308b_cod;
        private String p308c;
        private String p308c_cod;
        private String p308d;
        private String p308d_cod;
        private String p309;
        private String p310;
        private String p311;
        private String p312_1;
        private String p312_2;
        private String p312_3;
        private String p312_4;
        private String p312_5;
        private String p312_6;
        private String p312_7;
        private String p312_8;
        private String p312_9;
        private String p312_10;
        private String p312_11;
        private String p312_11_o;
        private String p313;
        private String p314;
        private String p314a;
        private String p314b;
        private String obs_d;


    public Modulos4() {

        id= "";
        id_vivienda= "";
        numero_hogar= "";
        numero_residente = "";
        inf_300= "";
        p307= "";
        p308a= "";
        p308a_cod= "";
        p308b= "";
        p308b_cod= "";
        p308c= "";
        p308c_cod= "";
        p308d= "";
        p308d_cod= "";
        p309= "";
        p310= "";
        p311= "";
        p312_1= "";
        p312_2= "";
        p312_3= "";
        p312_4= "";
        p312_5= "";
        p312_6= "";
        p312_7= "";
        p312_8= "";
        p312_9= "";
        p312_10= "";
        p312_11= "";
        p312_11_o= "";
        p313= "";
        p314= "";
        p314a= "";
        p314b= "";
        obs_d= "";

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_vivienda() {
        return id_vivienda;
    }

    public void setId_vivienda(String id_vivienda) {
        this.id_vivienda = id_vivienda;
    }

    public String getNumero_hogar() {
        return numero_hogar;
    }

    public void setNumero_hogar(String numero_hogar) {
        this.numero_hogar = numero_hogar;
    }

    public String getNumero_residente() {
        return numero_residente;
    }

    public void setNumero_residente(String numero_residente) {
        this.numero_residente = numero_residente;
    }

    public String getInf_300() {
        return inf_300;
    }

    public void setInf_300(String inf_300) {
        this.inf_300 = inf_300;
    }

    public String getP307() {
        return p307;
    }

    public void setP307(String p307) {
        this.p307 = p307;
    }

    public String getP308a() {
        return p308a;
    }

    public void setP308a(String p308a) {
        this.p308a = p308a;
    }

    public String getP308a_cod() {
        return p308a_cod;
    }

    public void setP308a_cod(String p308a_cod) {
        this.p308a_cod = p308a_cod;
    }

    public String getP308b() {
        return p308b;
    }

    public void setP308b(String p308b) {
        this.p308b = p308b;
    }

    public String getP308b_cod() {
        return p308b_cod;
    }

    public void setP308b_cod(String p308b_cod) {
        this.p308b_cod = p308b_cod;
    }

    public String getP308c() {
        return p308c;
    }

    public void setP308c(String p308c) {
        this.p308c = p308c;
    }

    public String getP308c_cod() {
        return p308c_cod;
    }

    public void setP308c_cod(String p308c_cod) {
        this.p308c_cod = p308c_cod;
    }

    public String getP308d() {
        return p308d;
    }

    public void setP308d(String p308d) {
        this.p308d = p308d;
    }

    public String getP308d_cod() {
        return p308d_cod;
    }

    public void setP308d_cod(String p308d_cod) {
        this.p308d_cod = p308d_cod;
    }

    public String getP309() {
        return p309;
    }

    public void setP309(String p309) {
        this.p309 = p309;
    }

    public String getP310() {
        return p310;
    }

    public void setP310(String p310) {
        this.p310 = p310;
    }

    public String getP311() {
        return p311;
    }

    public void setP311(String p311) {
        this.p311 = p311;
    }

    public String getP312_1() {
        return p312_1;
    }

    public void setP312_1(String p312_1) {
        this.p312_1 = p312_1;
    }

    public String getP312_2() {
        return p312_2;
    }

    public void setP312_2(String p312_2) {
        this.p312_2 = p312_2;
    }

    public String getP312_3() {
        return p312_3;
    }

    public void setP312_3(String p312_3) {
        this.p312_3 = p312_3;
    }

    public String getP312_4() {
        return p312_4;
    }

    public void setP312_4(String p312_4) {
        this.p312_4 = p312_4;
    }

    public String getP312_5() {
        return p312_5;
    }

    public void setP312_5(String p312_5) {
        this.p312_5 = p312_5;
    }

    public String getP312_6() {
        return p312_6;
    }

    public void setP312_6(String p312_6) {
        this.p312_6 = p312_6;
    }

    public String getP312_7() {
        return p312_7;
    }

    public void setP312_7(String p312_7) {
        this.p312_7 = p312_7;
    }

    public String getP312_8() {
        return p312_8;
    }

    public void setP312_8(String p312_8) {
        this.p312_8 = p312_8;
    }

    public String getP312_9() {
        return p312_9;
    }

    public void setP312_9(String p312_9) {
        this.p312_9 = p312_9;
    }

    public String getP312_10() {
        return p312_10;
    }

    public void setP312_10(String p312_10) {
        this.p312_10 = p312_10;
    }

    public String getP312_11() {
        return p312_11;
    }

    public void setP312_11(String p312_11) {
        this.p312_11 = p312_11;
    }

    public String getP312_11_o() {
        return p312_11_o;
    }

    public void setP312_11_o(String p312_11_o) {
        this.p312_11_o = p312_11_o;
    }

    public String getP313() {
        return p313;
    }

    public void setP313(String p313) {
        this.p313 = p313;
    }

    public String getP314() {
        return p314;
    }

    public void setP314(String p314) {
        this.p314 = p314;
    }

    public String getP314a() {
        return p314a;
    }

    public void setP314a(String p314a) {
        this.p314a = p314a;
    }

    public String getP314b() {
        return p314b;
    }

    public void setP314b(String p314b) {
        this.p314b = p314b;
    }

    public String getObs_d() {
        return obs_d;
    }

    public void setObs_d(String obs_d) {
        this.obs_d = obs_d;
    }

    public ContentValues toValues() {
        ContentValues contentValues = new ContentValues();

        contentValues.put(SQLConstantes.seccion_d_inf_300,inf_300);
        contentValues.put(SQLConstantes.seccion_d_p307,p307);
        contentValues.put(SQLConstantes.seccion_d_p308a,p308a);
        contentValues.put(SQLConstantes.seccion_d_p308a_cod,p308a_cod);
        contentValues.put(SQLConstantes.seccion_d_p308b,p308b);
        contentValues.put(SQLConstantes.seccion_d_p308b_cod,p308b_cod);
        contentValues.put(SQLConstantes.seccion_d_p308c,p308c);
        contentValues.put(SQLConstantes.seccion_d_p308c_cod,p308c_cod);
        contentValues.put(SQLConstantes.seccion_d_p308d,p308d);
        contentValues.put(SQLConstantes.seccion_d_p308d_cod,p308d_cod);
        contentValues.put(SQLConstantes.seccion_d_p309,p309);
        contentValues.put(SQLConstantes.seccion_d_p310,p310);
        contentValues.put(SQLConstantes.seccion_d_p311,p311);
        contentValues.put(SQLConstantes.seccion_d_p312_1,p312_1);
        contentValues.put(SQLConstantes.seccion_d_p312_2,p312_2);
        contentValues.put(SQLConstantes.seccion_d_p312_3,p312_3);
        contentValues.put(SQLConstantes.seccion_d_p312_4,p312_4);
        contentValues.put(SQLConstantes.seccion_d_p312_5,p312_5);
        contentValues.put(SQLConstantes.seccion_d_p312_6,p312_6);
        contentValues.put(SQLConstantes.seccion_d_p312_7,p312_7);
        contentValues.put(SQLConstantes.seccion_d_p312_8,p312_8);
        contentValues.put(SQLConstantes.seccion_d_p312_9,p312_9);
        contentValues.put(SQLConstantes.seccion_d_p312_10,p312_10);
        contentValues.put(SQLConstantes.seccion_d_p312_11,p312_11);
        contentValues.put(SQLConstantes.seccion_d_p312_11_o,p312_11_o);
        contentValues.put(SQLConstantes.seccion_d_p313,p313);
        contentValues.put(SQLConstantes.seccion_d_p314,p314);
        contentValues.put(SQLConstantes.seccion_d_p314a,p314a);
        contentValues.put(SQLConstantes.seccion_d_p314b,p314b);
        contentValues.put(SQLConstantes.seccion_d_obs_d,obs_d);
        return contentValues;
    }


}
