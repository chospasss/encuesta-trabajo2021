package com.example.ricindigus.trabajo2020.modelo.pojos;

import android.content.ContentValues;

import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;

public class Modulo6 {
    private String _id;
    private String idInformante;
    private String idHogar;
    private String idVivienda;
    /*
    private String c6_p601;
    private String c6_p602;
    private String c6_p603;
    private String c6_p604_1;
    private String c6_p604_2;
    private String c6_p604_3;
    private String c6_p604_4;
    private String c6_p604_5;
    private String c6_p604_6;
    private String c6_p604_7;
    private String c6_p604_8;
    private String c6_p604_9;
    private String c6_p604_10;
    private String c6_p604_11;
    private String c6_p604_o;
    private String c6_p605;
    private String c6_p606;
    private String c6_p607;
    private String c6_p608;
    private String c6_p608_o;
    private String c6_p609;
    private String c6_p610_pd;
    private String c6_p610_pl;
    private String c6_p610_pm;
    private String c6_p610_pmi;
    private String c6_p610_pj;
    private String c6_p610_pv;
    private String c6_p610_ps;
    private String c6_p610_pt;
    private String c6_p610_sd;
    private String c6_p610_sl;
    private String c6_p610_sm;
    private String c6_p610_smi;
    private String c6_p610_sj;
    private String c6_p610_sv;
    private String c6_p610_ss;
    private String c6_p610_st;
    private String c6_p610_t;
    private String c6_p611;
    private String c6_p611a;
    private String c6_p611b;
    private String c6_p612;
    private String c6_p612_nro;
    private String c6_p613;
    private String c6_p614_mon;
    private String c6_p614_esp;
    private String c6_p615_mon;
    private String c6_p615_esp;
    private String c6_p616_mon;
    private String c6_p616_esp;
    private String c6_p616_nas;
    private String c6_p617;
    private String c6_p617_dist;
    private String c6_p617_prov;
    private String c6_p617_dep;
    private String c6_p618;
    private String c6_p619;
    private String c6_p619_o;
    private String c6_p620;
    private String c6_p621;
    private String c6_p622;
    private String c6_p622_o;
    private String c6_p623;
    private String c6_p623_o;
    private String c6_p624;
    private String c6_p625_1;
    private String c6_p625_2;
    private String c6_p625_3;
    private String c6_p625_4;
    private String c6_p625_5;
    private String c6_p625_6;
    private String c6_p625_o;
    private String c6_p626;
    private String c6_p627;
    private String c6_p628;
    private String c6_p629_1;
    private String c6_p629_2;
    private String c6_p629_3;
    private String c6_p629_4;
    private String c6_p629_o;
    private String c6_p629_1_f;
    private String c6_p629_1_m;
    private String c6_p629_2_f;
    private String c6_p629_2_m;
    private String c6_p629_3_f;
    private String c6_p629_3_m;
    private String c6_p629_4_f;
    private String c6_p629_4_m;
    private String c6_p630_1;
    private String c6_p630_1med;
    private String c6_p630_1o;
    private String c6_p630_1frec;
    private String c6_p630_1frec_o;
    private String c6_p630_1mont;
    private String c6_p630_2;
    private String c6_p630_2med;
    private String c6_p630_2o;
    private String c6_p630_2mont;
    private String c6_p630_2frec;
    private String c6_p630_2frec_o;
    private String obs_cap6;
     */
    private String COB600;
    private String inf_300;
    private String p326;
    private String p327;
    private String p327_o;
    private String p328;
    private String p329_1;
    private String p329_2;
    private String p329_3;
    private String p329_4;
    private String p329_5;
    private String p329_6;
    private String p329_7;
    private String p329_8;
    private String p329_8_o;
    private String p330;
    private String p331;
    private String p332;
    private String p333_1;
    private String p333_2;
    private String p333_3;
    private String p333_4;
    private String p333_5;
    private String p333_5_o;
    private String p334;
    private String p335;
    private String p336_1;
    private String p337_1;
    private String p336_2;
    private String p337_2;
    private String p336_3;
    private String p337_3;
    private String p336_4;
    private String p337_4;
    private String p336_5;
    private String p337_5;
    private String p336_6;
    private String p337_6;
    private String p336_7;
    private String p336_7_o;
    private String p337_7;
    private String p336_8;
    private String p336_8_o;
    private String p337_8;
    private String obs_f;


    public Modulo6() {
        _id = "";
        idHogar = "";
        idVivienda = "";
        idInformante= "";
        /*
         c6_p601= "";
        c6_p602= "";
        c6_p603= "";
        c6_p604_1= "";
        c6_p604_2= "";
        c6_p604_3= "";
        c6_p604_4= "";
        c6_p604_5= "";
        c6_p604_6= "";
        c6_p604_7= "";
        c6_p604_8= "";
        c6_p604_9= "";
        c6_p604_10= "";
        c6_p604_11= "";
        c6_p604_o= "";
        c6_p605= "";
        c6_p606= "";
        c6_p607= "";
        c6_p608= "";
        c6_p608_o= "";
        c6_p609= "";
        c6_p610_pd= "";
        c6_p610_pl= "";
        c6_p610_pm= "";
        c6_p610_pmi= "";
        c6_p610_pj= "";
        c6_p610_pv= "";
        c6_p610_ps= "";
        c6_p610_pt= "";
        c6_p610_sd= "";
        c6_p610_sl= "";
        c6_p610_sm= "";
        c6_p610_smi= "";
        c6_p610_sj= "";
        c6_p610_sv= "";
        c6_p610_ss= "";
        c6_p610_st= "";
        c6_p610_t= "";
        c6_p611= "";
        c6_p611a= "";
        c6_p611b= "";
        c6_p612= "";
        c6_p612_nro= "";
        c6_p613= "";
        c6_p614_mon= "";
        c6_p614_esp= "";
        c6_p615_mon= "";
        c6_p615_esp= "";
        c6_p616_mon= "";
        c6_p616_esp= "";
        c6_p616_nas= "";
        c6_p617= "";
        c6_p617_dist= "";
        c6_p617_prov= "";
        c6_p617_dep= "";
        c6_p625_1= "";
        c6_p625_2= "";
        c6_p625_3= "";
        c6_p625_4= "";
        c6_p625_5= "";
        c6_p625_6= "";
        c6_p625_o= "";
        c6_p618= "";
        c6_p619= "";
        c6_p619_o= "";
        c6_p620= "";
        c6_p621= "";
        c6_p622= "";
        c6_p622_o= "";
        c6_p623= "";
        c6_p623_o= "";
        c6_p624= "";
        c6_p626= "";
        c6_p627= "";
        c6_p628= "";
        c6_p629_1= "";
        c6_p629_2= "";
        c6_p629_3= "";
        c6_p629_4= "";
        c6_p629_o= "";
        c6_p629_1_f= "";
        c6_p629_1_m= "";
        c6_p629_2_f= "";
        c6_p629_2_m= "";
        c6_p629_3_f= "";
        c6_p629_3_m= "";
        c6_p629_4_f= "";
        c6_p629_4_m= "";
        c6_p630_1= "";
        c6_p630_1med= "";
        c6_p630_1o= "";
        c6_p630_1frec= "";
        c6_p630_1frec_o= "";
        c6_p630_1mont= "";
        c6_p630_2= "";
        c6_p630_2med= "";
        c6_p630_2o= "";
        c6_p630_2mont= "";
        c6_p630_2frec= "";
        c6_p630_2frec_o= "";
        obs_cap6= "";
         */
        COB600= "0";
        //NUEVOS
        p326= "";
        p327= "";
        p327_o= "";
        p328= "";
        p329_1= "";
        p329_2= "";
        p329_3= "";
        p329_4= "";
        p329_5= "";
        p329_6= "";
        p329_7= "";
        p329_8= "";
        p329_8_o= "";
        p330= "";
        p331= "";
        p332= "";
        p333_1= "";
        p333_2= "";
        p333_3= "";
        p333_4= "";
        p333_5= "";
        p333_5_o= "";
        p334= "";
        p335= "";
        p336_1= "";
        p337_1= "";
        p336_2= "";
        p337_2= "";
        p336_3= "";
        p337_3= "";
        p336_4= "";
        p337_4= "";
        p336_5= "";
        p337_5= "";
        p336_6= "";
        p337_6= "";
        p336_7= "";
        p336_7_o= "";
        p337_7= "";
        p336_8= "";
        p336_8_o= "";
        p337_8= "";
        obs_f= "";
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    /*
    public String getC6_p630_2frec() {
        return c6_p630_2frec;
    }

    public void setC6_p630_2frec(String c6_p630_2frec) {
        this.c6_p630_2frec = c6_p630_2frec;
    }

     public String getC6_p601() {
        return c6_p601;
    }

    public void setC6_p601(String c6_p601) {
        this.c6_p601 = c6_p601;
    }

    public String getC6_p602() {
        return c6_p602;
    }

    public void setC6_p602(String c6_p602) {
        this.c6_p602 = c6_p602;
    }

    public String getC6_p603() {
        return c6_p603;
    }

    public void setC6_p603(String c6_p603) {
        this.c6_p603 = c6_p603;
    }

    public String getC6_p604_1() {
        return c6_p604_1;
    }

    public void setC6_p604_1(String c6_p604_1) {
        this.c6_p604_1 = c6_p604_1;
    }

    public String getC6_p604_2() {
        return c6_p604_2;
    }

    public void setC6_p604_2(String c6_p604_2) {
        this.c6_p604_2 = c6_p604_2;
    }

    public String getC6_p604_3() {
        return c6_p604_3;
    }

    public void setC6_p604_3(String c6_p604_3) {
        this.c6_p604_3 = c6_p604_3;
    }

    public String getC6_p604_4() {
        return c6_p604_4;
    }

    public void setC6_p604_4(String c6_p604_4) {
        this.c6_p604_4 = c6_p604_4;
    }

    public String getC6_p604_5() {
        return c6_p604_5;
    }

    public void setC6_p604_5(String c6_p604_5) {
        this.c6_p604_5 = c6_p604_5;
    }

    public String getC6_p604_6() {
        return c6_p604_6;
    }

    public void setC6_p604_6(String c6_p604_6) {
        this.c6_p604_6 = c6_p604_6;
    }

    public String getC6_p604_7() {
        return c6_p604_7;
    }

    public void setC6_p604_7(String c6_p604_7) {
        this.c6_p604_7 = c6_p604_7;
    }

    public String getC6_p604_8() {
        return c6_p604_8;
    }

    public void setC6_p604_8(String c6_p604_8) {
        this.c6_p604_8 = c6_p604_8;
    }

    public String getC6_p604_9() {
        return c6_p604_9;
    }

    public void setC6_p604_9(String c6_p604_9) {
        this.c6_p604_9 = c6_p604_9;
    }

    public String getC6_p604_10() {
        return c6_p604_10;
    }

    public void setC6_p604_10(String c6_p604_10) {
        this.c6_p604_10 = c6_p604_10;
    }

    public String getC6_p604_11() {
        return c6_p604_11;
    }

    public void setC6_p604_11(String c6_p604_11) {
        this.c6_p604_11 = c6_p604_11;
    }

    public String getC6_p604_o() {
        return c6_p604_o;
    }

    public void setC6_p604_o(String c6_p604_o) {
        this.c6_p604_o = c6_p604_o;
    }

    public String getC6_p605() {
        return c6_p605;
    }

    public void setC6_p605(String c6_p605) {
        this.c6_p605 = c6_p605;
    }

    public String getC6_p606() {
        return c6_p606;
    }

    public void setC6_p606(String c6_p606) {
        this.c6_p606 = c6_p606;
    }

    public String getC6_p607() {
        return c6_p607;
    }

    public void setC6_p607(String c6_p607) {
        this.c6_p607 = c6_p607;
    }

    public String getC6_p608() {
        return c6_p608;
    }

    public void setC6_p608(String c6_p608) {
        this.c6_p608 = c6_p608;
    }

    public String getC6_p608_o() {
        return c6_p608_o;
    }

    public void setC6_p608_o(String c6_p608_o) {
        this.c6_p608_o = c6_p608_o;
    }

    public String getC6_p609() {
        return c6_p609;
    }

    public void setC6_p609(String c6_p609) {
        this.c6_p609 = c6_p609;
    }

    public String getC6_p610_pd() {
        return c6_p610_pd;
    }

    public void setC6_p610_pd(String c6_p610_pd) {
        this.c6_p610_pd = c6_p610_pd;
    }

    public String getC6_p610_pl() {
        return c6_p610_pl;
    }

    public void setC6_p610_pl(String c6_p610_pl) {
        this.c6_p610_pl = c6_p610_pl;
    }

    public String getC6_p610_pm() {
        return c6_p610_pm;
    }

    public void setC6_p610_pm(String c6_p610_pm) {
        this.c6_p610_pm = c6_p610_pm;
    }

    public String getC6_p610_pmi() {
        return c6_p610_pmi;
    }

    public void setC6_p610_pmi(String c6_p610_pmi) {
        this.c6_p610_pmi = c6_p610_pmi;
    }

    public String getC6_p610_pj() {
        return c6_p610_pj;
    }

    public void setC6_p610_pj(String c6_p610_pj) {
        this.c6_p610_pj = c6_p610_pj;
    }

    public String getC6_p610_pv() {
        return c6_p610_pv;
    }

    public void setC6_p610_pv(String c6_p610_pv) {
        this.c6_p610_pv = c6_p610_pv;
    }

    public String getC6_p610_ps() {
        return c6_p610_ps;
    }

    public void setC6_p610_ps(String c6_p610_ps) {
        this.c6_p610_ps = c6_p610_ps;
    }

    public String getC6_p610_pt() {
        return c6_p610_pt;
    }

    public void setC6_p610_pt(String c6_p610_pt) {
        this.c6_p610_pt = c6_p610_pt;
    }

    public String getC6_p610_sd() {
        return c6_p610_sd;
    }

    public void setC6_p610_sd(String c6_p610_sd) {
        this.c6_p610_sd = c6_p610_sd;
    }

    public String getC6_p610_sl() {
        return c6_p610_sl;
    }

    public void setC6_p610_sl(String c6_p610_sl) {
        this.c6_p610_sl = c6_p610_sl;
    }

    public String getC6_p610_sm() {
        return c6_p610_sm;
    }

    public void setC6_p610_sm(String c6_p610_sm) {
        this.c6_p610_sm = c6_p610_sm;
    }

    public String getC6_p610_smi() {
        return c6_p610_smi;
    }

    public void setC6_p610_smi(String c6_p610_smi) {
        this.c6_p610_smi = c6_p610_smi;
    }

    public String getC6_p610_sj() {
        return c6_p610_sj;
    }

    public void setC6_p610_sj(String c6_p610_sj) {
        this.c6_p610_sj = c6_p610_sj;
    }

    public String getC6_p610_sv() {
        return c6_p610_sv;
    }

    public void setC6_p610_sv(String c6_p610_sv) {
        this.c6_p610_sv = c6_p610_sv;
    }

    public String getC6_p610_ss() {
        return c6_p610_ss;
    }

    public void setC6_p610_ss(String c6_p610_ss) {
        this.c6_p610_ss = c6_p610_ss;
    }

    public String getC6_p610_st() {
        return c6_p610_st;
    }

    public void setC6_p610_st(String c6_p610_st) {
        this.c6_p610_st = c6_p610_st;
    }

    public String getC6_p610_t() {
        return c6_p610_t;
    }

    public void setC6_p610_t(String c6_p610_t) {
        this.c6_p610_t = c6_p610_t;
    }

    public int getC6_p610_total_horas_secundarias(){
        return valor_numero(c6_p610_sd) + valor_numero(c6_p610_sl) +
                valor_numero(c6_p610_sm) + valor_numero(c6_p610_smi) +
                valor_numero(c6_p610_sj)+valor_numero(c6_p610_sv) + valor_numero(c6_p610_ss);
    }

    public String getC6_p611() {
        return c6_p611;
    }

    public void setC6_p611(String c6_p611) {
        this.c6_p611 = c6_p611;
    }

    public String getC6_p611a() {
        return c6_p611a;
    }

    public void setC6_p611a(String c6_p611a) {
        this.c6_p611a = c6_p611a;
    }

    public String getC6_p611b() {
        return c6_p611b;
    }

    public void setC6_p611b(String c6_p611b) {
        this.c6_p611b = c6_p611b;
    }

    public String getC6_p612() {
        return c6_p612;
    }

    public void setC6_p612(String c6_p612) {
        this.c6_p612 = c6_p612;
    }

    public String getC6_p612_nro() {
        return c6_p612_nro;
    }

    public void setC6_p612_nro(String c6_p612_nro) {
        this.c6_p612_nro = c6_p612_nro;
    }

    public String getC6_p613() {
        return c6_p613;
    }

    public void setC6_p613(String c6_p613) {
        this.c6_p613 = c6_p613;
    }

    public String getC6_p614_mon() {
        return c6_p614_mon;
    }

    public void setC6_p614_mon(String c6_p614_mon) {
        this.c6_p614_mon = c6_p614_mon;
    }

    public String getC6_p614_esp() {
        return c6_p614_esp;
    }

    public void setC6_p614_esp(String c6_p614_esp) {
        this.c6_p614_esp = c6_p614_esp;
    }

    public String getC6_p615_mon() {
        return c6_p615_mon;
    }

    public void setC6_p615_mon(String c6_p615_mon) {
        this.c6_p615_mon = c6_p615_mon;
    }

    public String getC6_p615_esp() {
        return c6_p615_esp;
    }

    public void setC6_p615_esp(String c6_p615_esp) {
        this.c6_p615_esp = c6_p615_esp;
    }

    public String getC6_p616_mon() {
        return c6_p616_mon;
    }

    public void setC6_p616_mon(String c6_p616_mon) {
        this.c6_p616_mon = c6_p616_mon;
    }

    public String getC6_p616_esp() {
        return c6_p616_esp;
    }

    public void setC6_p616_esp(String c6_p616_esp) {
        this.c6_p616_esp = c6_p616_esp;
    }

    public String getC6_p616_nas() {
        return c6_p616_nas;
    }

    public void setC6_p616_nas(String c6_p616_nas) {
        this.c6_p616_nas = c6_p616_nas;
    }

    public String getC6_p617() {
        return c6_p617;
    }

    public void setC6_p617(String c6_p617) {
        this.c6_p617 = c6_p617;
    }

    public String getC6_p617_dist() {
        return c6_p617_dist;
    }

    public void setC6_p617_dist(String c6_p617_dist) {
        this.c6_p617_dist = c6_p617_dist;
    }

    public String getC6_p617_prov() {
        return c6_p617_prov;
    }

    public void setC6_p617_prov(String c6_p617_prov) {
        this.c6_p617_prov = c6_p617_prov;
    }

    public String getC6_p617_dep() {
        return c6_p617_dep;
    }

    public void setC6_p617_dep(String c6_p617_dep) {
        this.c6_p617_dep = c6_p617_dep;
    }

    public String getC6_p625_1() {
        return c6_p625_1;
    }

    public void setC6_p625_1(String c6_p625_1) {
        this.c6_p625_1 = c6_p625_1;
    }

    public String getC6_p625_2() {
        return c6_p625_2;
    }

    public void setC6_p625_2(String c6_p625_2) {
        this.c6_p625_2 = c6_p625_2;
    }

    public String getC6_p625_3() {
        return c6_p625_3;
    }

    public void setC6_p625_3(String c6_p625_3) {
        this.c6_p625_3 = c6_p625_3;
    }

    public String getC6_p625_4() {
        return c6_p625_4;
    }

    public void setC6_p625_4(String c6_p625_4) {
        this.c6_p625_4 = c6_p625_4;
    }

    public String getC6_p625_5() {
        return c6_p625_5;
    }

    public void setC6_p625_5(String c6_p625_5) {
        this.c6_p625_5 = c6_p625_5;
    }

    public String getC6_p625_6() {
        return c6_p625_6;
    }

    public void setC6_p625_6(String c6_p625_6) {
        this.c6_p625_6 = c6_p625_6;
    }

    public String getC6_p625_o() {
        return c6_p625_o;
    }

    public void setC6_p625_o(String c6_p625_o) {
        this.c6_p625_o = c6_p625_o;
    }

    public String getC6_p618() {
        return c6_p618;
    }

    public void setC6_p618(String c6_p618) {
        this.c6_p618 = c6_p618;
    }

    public String getC6_p619() {
        return c6_p619;
    }

    public void setC6_p619(String c6_p619) {
        this.c6_p619 = c6_p619;
    }

    public String getC6_p619_o() {
        return c6_p619_o;
    }

    public void setC6_p619_o(String c6_p619_o) {
        this.c6_p619_o = c6_p619_o;
    }

    public String getC6_p620() {
        return c6_p620;
    }

    public void setC6_p620(String c6_p620) {
        this.c6_p620 = c6_p620;
    }

    public String getC6_p621() {
        return c6_p621;
    }

    public void setC6_p621(String c6_p621) {
        this.c6_p621 = c6_p621;
    }

    public String getC6_p622() {
        return c6_p622;
    }

    public void setC6_p622(String c6_p622) {
        this.c6_p622 = c6_p622;
    }

    public String getC6_p622_o() {
        return c6_p622_o;
    }

    public void setC6_p622_o(String c6_p622_o) {
        this.c6_p622_o = c6_p622_o;
    }

    public String getC6_p623() {
        return c6_p623;
    }

    public void setC6_p623(String c6_p623) {
        this.c6_p623 = c6_p623;
    }

    public String getC6_p623_o() {
        return c6_p623_o;
    }

    public void setC6_p623_o(String c6_p623_o) {
        this.c6_p623_o = c6_p623_o;
    }

    public String getC6_p624() {
        return c6_p624;
    }

    public void setC6_p624(String c6_p624) {
        this.c6_p624 = c6_p624;
    }

    public String getC6_p626() {
        return c6_p626;
    }

    public void setC6_p626(String c6_p626) {
        this.c6_p626 = c6_p626;
    }

    public String getC6_p627() {
        return c6_p627;
    }

    public void setC6_p627(String c6_p627) {
        this.c6_p627 = c6_p627;
    }

    public String getC6_p628() {
        return c6_p628;
    }

    public void setC6_p628(String c6_p628) {
        this.c6_p628 = c6_p628;
    }

    public String getC6_p629_1() {
        return c6_p629_1;
    }

    public void setC6_p629_1(String c6_p629_1) {
        this.c6_p629_1 = c6_p629_1;
    }

    public String getC6_p629_2() {
        return c6_p629_2;
    }

    public void setC6_p629_2(String c6_p629_2) {
        this.c6_p629_2 = c6_p629_2;
    }

    public String getC6_p629_3() {
        return c6_p629_3;
    }

    public void setC6_p629_3(String c6_p629_3) {
        this.c6_p629_3 = c6_p629_3;
    }

    public String getC6_p629_4() {
        return c6_p629_4;
    }

    public void setC6_p629_4(String c6_p629_4) {
        this.c6_p629_4 = c6_p629_4;
    }

    public String getC6_p629_o() {
        return c6_p629_o;
    }

    public void setC6_p629_o(String c6_p629_o) {
        this.c6_p629_o = c6_p629_o;
    }

    public String getC6_p629_1_f() {
        return c6_p629_1_f;
    }

    public void setC6_p629_1_f(String c6_p629_1_f) {
        this.c6_p629_1_f = c6_p629_1_f;
    }

    public String getC6_p629_1_m() {
        return c6_p629_1_m;
    }

    public void setC6_p629_1_m(String c6_p629_1_m) {
        this.c6_p629_1_m = c6_p629_1_m;
    }

    public String getC6_p629_2_f() {
        return c6_p629_2_f;
    }

    public void setC6_p629_2_f(String c6_p629_2_f) {
        this.c6_p629_2_f = c6_p629_2_f;
    }

    public String getC6_p629_2_m() {
        return c6_p629_2_m;
    }

    public void setC6_p629_2_m(String c6_p629_2_m) {
        this.c6_p629_2_m = c6_p629_2_m;
    }

    public String getC6_p629_3_f() {
        if(c6_p629_3_f==null) return ""; else return c6_p629_3_f;
    }

    public void setC6_p629_3_f(String c6_p629_3_f) {
        this.c6_p629_3_f = c6_p629_3_f;
    }

    public String getC6_p629_3_m() {
        return c6_p629_3_m;
    }

    public void setC6_p629_3_m(String c6_p629_3_m) {
        this.c6_p629_3_m = c6_p629_3_m;
    }

    public String getC6_p629_4_f() {
        return c6_p629_4_f;
    }

    public void setC6_p629_4_f(String c6_p629_4_f) {
        this.c6_p629_4_f = c6_p629_4_f;
    }

    public String getC6_p629_4_m() {
        return c6_p629_4_m;
    }

    public void setC6_p629_4_m(String c6_p629_4_m) {
        this.c6_p629_4_m = c6_p629_4_m;
    }

    public String getC6_p630_1frec() {
        return c6_p630_1frec;
    }

    public void setC6_p630_1frec(String c6_p630_1frec) {
        this.c6_p630_1frec = c6_p630_1frec;
    }

    public String getC6_p630_1() {
        return c6_p630_1;
    }

    public void setC6_p630_1(String c6_p630_1) {
        this.c6_p630_1 = c6_p630_1;
    }

    public String getC6_p630_1med() {
        return c6_p630_1med;
    }

    public void setC6_p630_1med(String c6_p630_1med) {
        this.c6_p630_1med = c6_p630_1med;
    }

    public String getC6_p630_1o() {
        return c6_p630_1o;
    }

    public void setC6_p630_1o(String c6_p630_1o) {
        this.c6_p630_1o = c6_p630_1o;
    }

    public String getC6_p630_1mont() {
        return c6_p630_1mont;
    }

    public void setC6_p630_1mont(String c6_p630_1mont) {
        this.c6_p630_1mont = c6_p630_1mont;
    }

    public String getC6_p630_2() {
        return c6_p630_2;
    }

    public void setC6_p630_2(String c6_p630_2) {
        this.c6_p630_2 = c6_p630_2;
    }

    public String getC6_p630_2med() {
        return c6_p630_2med;
    }

    public void setC6_p630_2med(String c6_p630_2med) {
        this.c6_p630_2med = c6_p630_2med;
    }

    public String getC6_p630_2o() {
        return c6_p630_2o;
    }

    public void setC6_p630_2o(String c6_p630_2o) {
        this.c6_p630_2o = c6_p630_2o;
    }

    public String getC6_p630_2mont() {
        return c6_p630_2mont;
    }

    public void setC6_p630_2mont(String c6_p630_2mont) {
        this.c6_p630_2mont = c6_p630_2mont;
    }

    public String getC6_p630_1frec_o() {
        return c6_p630_1frec_o;
    }

    public void setC6_p630_1frec_o(String c6_p630_1frec_o) {
        this.c6_p630_1frec_o = c6_p630_1frec_o;
    }

    public String getC6_p630_2frec_o() {
        return c6_p630_2frec_o;
    }

    public void setC6_p630_2frec_o(String c6_p630_2frec_o) {
        this.c6_p630_2frec_o = c6_p630_2frec_o;
    }

    public String getObs_cap6() {
        return obs_cap6;
    }

    public void setObs_cap6(String obs_cap6) {
        this.obs_cap6 = obs_cap6;
    }
     */


    public String getIdInformante() {
        return idInformante;
    }

    public void setIdInformante(String idInformante) {
        this.idInformante = idInformante;
    }

    public String getIdHogar() {
        return idHogar;
    }

    public void setIdHogar(String idHogar) {
        this.idHogar = idHogar;
    }

    public String getIdVivienda() {
        return idVivienda;
    }

    public void setIdVivienda(String idVivienda) {
        this.idVivienda = idVivienda;
    }



    public int valor_numero(String num){
        if(num==null){
            return 0;
        }else if(num.equals("")){
            return 0;
        }else{
            return Integer.parseInt(num);
        }
    }



    public String getCOB600() {
        return COB600;
    }

    public void setCOB600(String COB600) {
        this.COB600 = COB600;
    }

    public String getInf_300() {
        return inf_300;
    }

    public void setInf_300(String inf_300) {
        this.inf_300 = inf_300;
    }

    public String getP326() {
        return p326;
    }

    public void setP326(String p326) {
        this.p326 = p326;
    }

    public String getP327() {
        return p327;
    }

    public void setP327(String p327) {
        this.p327 = p327;
    }

    public String getP327_o() {
        return p327_o;
    }

    public void setP327_o(String p327_o) {
        this.p327_o = p327_o;
    }

    public String getP328() {
        return p328;
    }

    public void setP328(String p328) {
        this.p328 = p328;
    }

    public String getP329_1() {
        return p329_1;
    }

    public void setP329_1(String p329_1) {
        this.p329_1 = p329_1;
    }

    public String getP329_2() {
        return p329_2;
    }

    public void setP329_2(String p329_2) {
        this.p329_2 = p329_2;
    }

    public String getP329_3() {
        return p329_3;
    }

    public void setP329_3(String p329_3) {
        this.p329_3 = p329_3;
    }

    public String getP329_4() {
        return p329_4;
    }

    public void setP329_4(String p329_4) {
        this.p329_4 = p329_4;
    }

    public String getP329_5() {
        return p329_5;
    }

    public void setP329_5(String p329_5) {
        this.p329_5 = p329_5;
    }

    public String getP329_6() {
        return p329_6;
    }

    public void setP329_6(String p329_6) {
        this.p329_6 = p329_6;
    }

    public String getP329_7() {
        return p329_7;
    }

    public void setP329_7(String p329_7) {
        this.p329_7 = p329_7;
    }

    public String getP329_8() {
        return p329_8;
    }

    public void setP329_8(String p329_8) {
        this.p329_8 = p329_8;
    }

    public String getP329_8_o() {
        return p329_8_o;
    }

    public void setP329_8_o(String p329_8_o) {
        this.p329_8_o = p329_8_o;
    }

    public String getP330() {
        return p330;
    }

    public void setP330(String p330) {
        this.p330 = p330;
    }

    public String getP331() {
        return p331;
    }

    public void setP331(String p331) {
        this.p331 = p331;
    }

    public String getP332() {
        return p332;
    }

    public void setP332(String p332) {
        this.p332 = p332;
    }

    public String getP333_1() {
        return p333_1;
    }

    public void setP333_1(String p333_1) {
        this.p333_1 = p333_1;
    }

    public String getP333_2() {
        return p333_2;
    }

    public void setP333_2(String p333_2) {
        this.p333_2 = p333_2;
    }

    public String getP333_3() {
        return p333_3;
    }

    public void setP333_3(String p333_3) {
        this.p333_3 = p333_3;
    }

    public String getP333_4() {
        return p333_4;
    }

    public void setP333_4(String p333_4) {
        this.p333_4 = p333_4;
    }

    public String getP333_5() {
        return p333_5;
    }

    public void setP333_5(String p333_5) {
        this.p333_5 = p333_5;
    }

    public String getP333_5_o() {
        return p333_5_o;
    }

    public void setP333_5_o(String p333_5_o) {
        this.p333_5_o = p333_5_o;
    }

    public String getP334() {
        return p334;
    }

    public void setP334(String p334) {
        this.p334 = p334;
    }

    public String getP335() {
        return p335;
    }

    public void setP335(String p335) {
        this.p335 = p335;
    }

    public String getP336_1() {
        return p336_1;
    }

    public void setP336_1(String p336_1) {
        this.p336_1 = p336_1;
    }

    public String getP337_1() {
        return p337_1;
    }

    public void setP337_1(String p337_1) {
        this.p337_1 = p337_1;
    }

    public String getP336_2() {
        return p336_2;
    }

    public void setP336_2(String p336_2) {
        this.p336_2 = p336_2;
    }

    public String getP337_2() {
        return p337_2;
    }

    public void setP337_2(String p337_2) {
        this.p337_2 = p337_2;
    }

    public String getP336_3() {
        return p336_3;
    }

    public void setP336_3(String p336_3) {
        this.p336_3 = p336_3;
    }

    public String getP337_3() {
        return p337_3;
    }

    public void setP337_3(String p337_3) {
        this.p337_3 = p337_3;
    }

    public String getP336_4() {
        return p336_4;
    }

    public void setP336_4(String p336_4) {
        this.p336_4 = p336_4;
    }

    public String getP337_4() {
        return p337_4;
    }

    public void setP337_4(String p337_4) {
        this.p337_4 = p337_4;
    }

    public String getP336_5() {
        return p336_5;
    }

    public void setP336_5(String p336_5) {
        this.p336_5 = p336_5;
    }

    public String getP337_5() {
        return p337_5;
    }

    public void setP337_5(String p337_5) {
        this.p337_5 = p337_5;
    }

    public String getP336_6() {
        return p336_6;
    }

    public void setP336_6(String p336_6) {
        this.p336_6 = p336_6;
    }

    public String getP337_6() {
        return p337_6;
    }

    public void setP337_6(String p337_6) {
        this.p337_6 = p337_6;
    }

    public String getP336_7() {
        return p336_7;
    }

    public void setP336_7(String p336_7) {
        this.p336_7 = p336_7;
    }

    public String getP336_7_o() {
        return p336_7_o;
    }

    public void setP336_7_o(String p336_7_o) {
        this.p336_7_o = p336_7_o;
    }

    public String getP337_7() {
        return p337_7;
    }

    public void setP337_7(String p337_7) {
        this.p337_7 = p337_7;
    }

    public String getP336_8() {
        return p336_8;
    }

    public void setP336_8(String p336_8) {
        this.p336_8 = p336_8;
    }

    public String getP336_8_o() {
        return p336_8_o;
    }

    public void setP336_8_o(String p336_8_o) {
        this.p336_8_o = p336_8_o;
    }

    public String getP337_8() {
        return p337_8;
    }

    public void setP337_8(String p337_8) {
        this.p337_8 = p337_8;
    }

    public String getObs_f() {
        return obs_f;
    }

    public void setObs_f(String obs_f) {
        this.obs_f = obs_f;
    }

    public ContentValues toValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo6_id,_id);
        contentValues.put(SQLConstantes.modulo6_id_informante,idInformante);
        contentValues.put(SQLConstantes.modulo6_id_hogar,idHogar);
        contentValues.put(SQLConstantes.modulo6_id_vivienda,idVivienda);
        /*
        contentValues.put(SQLConstantes.modulo6_c6_p601,c6_p601);
        contentValues.put(SQLConstantes.modulo6_c6_p602,c6_p602);
        contentValues.put(SQLConstantes.modulo6_c6_p603,c6_p603);
        contentValues.put(SQLConstantes.modulo6_c6_p604_1,c6_p604_1);
        contentValues.put(SQLConstantes.modulo6_c6_p604_2,c6_p604_2);
        contentValues.put(SQLConstantes.modulo6_c6_p604_3,c6_p604_3);
        contentValues.put(SQLConstantes.modulo6_c6_p604_4,c6_p604_4);
        contentValues.put(SQLConstantes.modulo6_c6_p604_5,c6_p604_5);
        contentValues.put(SQLConstantes.modulo6_c6_p604_6,c6_p604_6);
        contentValues.put(SQLConstantes.modulo6_c6_p604_7,c6_p604_7);
        contentValues.put(SQLConstantes.modulo6_c6_p604_8,c6_p604_8);
        contentValues.put(SQLConstantes.modulo6_c6_p604_9,c6_p604_9);
        contentValues.put(SQLConstantes.modulo6_c6_p604_10,c6_p604_10);
        contentValues.put(SQLConstantes.modulo6_c6_p604_11,c6_p604_11);
        contentValues.put(SQLConstantes.modulo6_c6_p604_o,c6_p604_o);
        contentValues.put(SQLConstantes.modulo6_c6_p605,c6_p605);
        contentValues.put(SQLConstantes.modulo6_c6_p606,c6_p606);
        contentValues.put(SQLConstantes.modulo6_c6_p607,c6_p607);
        contentValues.put(SQLConstantes.modulo6_c6_p608,c6_p608);
        contentValues.put(SQLConstantes.modulo6_c6_p608_o,c6_p608_o);
        contentValues.put(SQLConstantes.modulo6_c6_p609,c6_p609);
        contentValues.put(SQLConstantes.modulo6_c6_p610_pd,c6_p610_pd);
        contentValues.put(SQLConstantes.modulo6_c6_p610_pl,c6_p610_pl);
        contentValues.put(SQLConstantes.modulo6_c6_p610_pm,c6_p610_pm);
        contentValues.put(SQLConstantes.modulo6_c6_p610_pmi,c6_p610_pmi);
        contentValues.put(SQLConstantes.modulo6_c6_p610_pj,c6_p610_pj);
        contentValues.put(SQLConstantes.modulo6_c6_p610_pv,c6_p610_pv);
        contentValues.put(SQLConstantes.modulo6_c6_p610_ps,c6_p610_ps);
        contentValues.put(SQLConstantes.modulo6_c6_p610_pt,c6_p610_pt);
        contentValues.put(SQLConstantes.modulo6_c6_p610_sd,c6_p610_sd);
        contentValues.put(SQLConstantes.modulo6_c6_p610_sl,c6_p610_sl);
        contentValues.put(SQLConstantes.modulo6_c6_p610_sm,c6_p610_sm);
        contentValues.put(SQLConstantes.modulo6_c6_p610_smi,c6_p610_smi);
        contentValues.put(SQLConstantes.modulo6_c6_p610_sj,c6_p610_sj);
        contentValues.put(SQLConstantes.modulo6_c6_p610_sv,c6_p610_sv);
        contentValues.put(SQLConstantes.modulo6_c6_p610_ss,c6_p610_ss);
        contentValues.put(SQLConstantes.modulo6_c6_p610_st,c6_p610_st);
        contentValues.put(SQLConstantes.modulo6_c6_p610_t,c6_p610_t);
        contentValues.put(SQLConstantes.modulo6_c6_p611,c6_p611);
        contentValues.put(SQLConstantes.modulo6_c6_p611a,c6_p611a);
        contentValues.put(SQLConstantes.modulo6_c6_p611b,c6_p611b);
        contentValues.put(SQLConstantes.modulo6_c6_p612,c6_p612);
        contentValues.put(SQLConstantes.modulo6_c6_p612_nro,c6_p612_nro);
        contentValues.put(SQLConstantes.modulo6_c6_p613,c6_p613);
        contentValues.put(SQLConstantes.modulo6_c6_p614_mon,c6_p614_mon);
        contentValues.put(SQLConstantes.modulo6_c6_p614_esp,c6_p614_esp);
        contentValues.put(SQLConstantes.modulo6_c6_p615_mon,c6_p615_mon);
        contentValues.put(SQLConstantes.modulo6_c6_p615_esp,c6_p615_esp);
        contentValues.put(SQLConstantes.modulo6_c6_p616_mon,c6_p616_mon);
        contentValues.put(SQLConstantes.modulo6_c6_p616_esp,c6_p616_esp);
        contentValues.put(SQLConstantes.modulo6_c6_p616_nas,c6_p616_nas);
        contentValues.put(SQLConstantes.modulo6_c6_p617,c6_p617);
        contentValues.put(SQLConstantes.modulo6_c6_p617_dist,c6_p617_dist);
        contentValues.put(SQLConstantes.modulo6_c6_p617_prov,c6_p617_prov);
        contentValues.put(SQLConstantes.modulo6_c6_p617_dep,c6_p617_dep);
        contentValues.put(SQLConstantes.modulo6_c6_p618,c6_p618);
        contentValues.put(SQLConstantes.modulo6_c6_p619,c6_p619);
        contentValues.put(SQLConstantes.modulo6_c6_p619_o,c6_p619_o);
        contentValues.put(SQLConstantes.modulo6_c6_p620,c6_p620);
        contentValues.put(SQLConstantes.modulo6_c6_p621,c6_p621);
        contentValues.put(SQLConstantes.modulo6_c6_p622,c6_p622);
        contentValues.put(SQLConstantes.modulo6_c6_p622_o,c6_p622_o);
        contentValues.put(SQLConstantes.modulo6_c6_p623,c6_p623);
        contentValues.put(SQLConstantes.modulo6_c6_p623_o,c6_p623_o);
        contentValues.put(SQLConstantes.modulo6_c6_p624,c6_p624);
        contentValues.put(SQLConstantes.modulo6_c6_p625_1,c6_p625_1);
        contentValues.put(SQLConstantes.modulo6_c6_p625_2,c6_p625_2);
        contentValues.put(SQLConstantes.modulo6_c6_p625_3,c6_p625_3);
        contentValues.put(SQLConstantes.modulo6_c6_p625_4,c6_p625_4);
        contentValues.put(SQLConstantes.modulo6_c6_p625_5,c6_p625_5);
        contentValues.put(SQLConstantes.modulo6_c6_p625_6,c6_p625_6);
        contentValues.put(SQLConstantes.modulo6_c6_p625_o,c6_p625_o);
        contentValues.put(SQLConstantes.modulo6_c6_p626,c6_p626);
        contentValues.put(SQLConstantes.modulo6_c6_p627,c6_p627);
        contentValues.put(SQLConstantes.modulo6_c6_p628,c6_p628);
        contentValues.put(SQLConstantes.modulo6_c6_p629_1,c6_p629_1);
        contentValues.put(SQLConstantes.modulo6_c6_p629_2,c6_p629_2);
        contentValues.put(SQLConstantes.modulo6_c6_p629_3,c6_p629_3);
        contentValues.put(SQLConstantes.modulo6_c6_p629_4,c6_p629_4);
        contentValues.put(SQLConstantes.modulo6_c6_p629_o,c6_p629_o);
        contentValues.put(SQLConstantes.modulo6_c6_p629_1_f,c6_p629_1_f);
        contentValues.put(SQLConstantes.modulo6_c6_p629_1_m,c6_p629_1_m);
        contentValues.put(SQLConstantes.modulo6_c6_p629_2_f,c6_p629_2_f);
        contentValues.put(SQLConstantes.modulo6_c6_p629_2_m,c6_p629_2_m);
        contentValues.put(SQLConstantes.modulo6_c6_p629_3_f,c6_p629_3_f);
        contentValues.put(SQLConstantes.modulo6_c6_p629_3_m,c6_p629_3_m);
        contentValues.put(SQLConstantes.modulo6_c6_p629_4_f,c6_p629_4_f);
        contentValues.put(SQLConstantes.modulo6_c6_p629_4_m,c6_p629_4_m);
        contentValues.put(SQLConstantes.modulo6_c6_p630_1,c6_p630_1);
        contentValues.put(SQLConstantes.modulo6_c6_p630_1med,c6_p630_1med);
        contentValues.put(SQLConstantes.modulo6_c6_p630_1o,c6_p630_1o);
        contentValues.put(SQLConstantes.modulo6_c6_p630_1frec,c6_p630_1frec);
        contentValues.put(SQLConstantes.modulo6_c6_p630_1frec_o,c6_p630_1frec_o);
        contentValues.put(SQLConstantes.modulo6_c6_p630_1mont,c6_p630_1mont);
        contentValues.put(SQLConstantes.modulo6_c6_p630_2,c6_p630_2);
        contentValues.put(SQLConstantes.modulo6_c6_p630_2med,c6_p630_2med);
        contentValues.put(SQLConstantes.modulo6_c6_p630_2o,c6_p630_2o);
        contentValues.put(SQLConstantes.modulo6_c6_p630_2frec,c6_p630_2frec);
        contentValues.put(SQLConstantes.modulo6_c6_p630_2frec_o,c6_p630_2frec_o);
        contentValues.put(SQLConstantes.modulo6_c6_p630_2mont,c6_p630_2mont);
        contentValues.put(SQLConstantes.modulo6_obs_cap6,obs_cap6);
         */
        contentValues.put(SQLConstantes.seccion_f_p326,p326);
        contentValues.put(SQLConstantes.seccion_f_p327,p327);
        contentValues.put(SQLConstantes.seccion_f_p327_o,p327_o);
        contentValues.put(SQLConstantes.seccion_f_p328,p328);
        contentValues.put(SQLConstantes.seccion_f_p329_1,p329_1);
        contentValues.put(SQLConstantes.seccion_f_p329_2,p329_2);
        contentValues.put(SQLConstantes.seccion_f_p329_3,p329_3);
        contentValues.put(SQLConstantes.seccion_f_p329_4,p329_4);
        contentValues.put(SQLConstantes.seccion_f_p329_5,p329_5);
        contentValues.put(SQLConstantes.seccion_f_p329_6,p329_6);
        contentValues.put(SQLConstantes.seccion_f_p329_7,p329_7);
        contentValues.put(SQLConstantes.seccion_f_p329_8,p329_8);
        contentValues.put(SQLConstantes.seccion_f_p329_8_o,p329_8_o);
        contentValues.put(SQLConstantes.seccion_f_p330,p330);
        contentValues.put(SQLConstantes.seccion_f_p331,p331);
        contentValues.put(SQLConstantes.seccion_f_p332,p332);
        contentValues.put(SQLConstantes.seccion_f_p333_1,p333_1);
        contentValues.put(SQLConstantes.seccion_f_p333_2,p333_2);
        contentValues.put(SQLConstantes.seccion_f_p333_3,p333_3);
        contentValues.put(SQLConstantes.seccion_f_p333_4,p333_4);
        contentValues.put(SQLConstantes.seccion_f_p333_5,p333_5);
        contentValues.put(SQLConstantes.seccion_f_p333_5_o,p333_5_o);
        contentValues.put(SQLConstantes.seccion_f_p334,p334);
        contentValues.put(SQLConstantes.seccion_f_p335,p335);
        contentValues.put(SQLConstantes.seccion_f_p336_1,p336_1);
        contentValues.put(SQLConstantes.seccion_f_p337_1,p337_1);
        contentValues.put(SQLConstantes.seccion_f_p336_2,p336_2);
        contentValues.put(SQLConstantes.seccion_f_p337_2,p337_2);
        contentValues.put(SQLConstantes.seccion_f_p336_3,p336_3);
        contentValues.put(SQLConstantes.seccion_f_p337_3,p337_3);
        contentValues.put(SQLConstantes.seccion_f_p336_4,p336_4);
        contentValues.put(SQLConstantes.seccion_f_p337_4,p337_4);
        contentValues.put(SQLConstantes.seccion_f_p336_5,p336_5);
        contentValues.put(SQLConstantes.seccion_f_p337_5,p337_5);
        contentValues.put(SQLConstantes.seccion_f_p336_6,p336_6);
        contentValues.put(SQLConstantes.seccion_f_p337_6,p337_6);
        contentValues.put(SQLConstantes.seccion_f_p336_7,p336_7);
        contentValues.put(SQLConstantes.seccion_f_p336_7_o,p336_7_o);
        contentValues.put(SQLConstantes.seccion_f_p337_7,p337_7);
        contentValues.put(SQLConstantes.seccion_f_p336_8,p336_8);
        contentValues.put(SQLConstantes.seccion_f_p336_8_o,p336_8_o);
        contentValues.put(SQLConstantes.seccion_f_p337_8,p337_8);
        contentValues.put(SQLConstantes.seccion_f_obs_f,obs_f);
        contentValues.put(SQLConstantes.modulo6_COB600,COB600);
        return contentValues;
    }
}
