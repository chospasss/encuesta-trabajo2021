package com.example.ricindigus.trabajo2020.fragments.modulo1;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo1V;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InputFilterSoloLetras;
import com.example.ricindigus.trabajo2020.util.NumericKeyBoardTransformationMethod;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentP101P107 extends FragmentPagina {

    Context context;




    //nuevos
    LinearLayout layout_a_p001,layout_a_p002,layout_a_p003,layout_a_p003a,layout_a_p004,layout_a_p005,layout_a_p006;
    RadioGroup radiogroup_a_P001,radiogroup_a_P002,radiogroup_a_P003,radiogroup_a_P003_a,radiogroup_a_P004;
    EditText edittext_a_P001_O,edittext_a_P002_O,edittext_a_P003_O,edittext_a_P004_O;

    CheckBox checkbox_a_P005_1,checkbox_a_P005_2,checkbox_a_P005_3,checkbox_a_P005_4,checkbox_a_P005_5,checkbox_a_P005_6;
    EditText edittext_a_p005_sp5_O;

    CheckBox checkbox_a_P006_1,checkbox_a_P006_2,checkbox_a_P006_3,checkbox_a_P006_4,checkbox_a_P006_5,checkbox_a_P006_6,checkbox_a_P006_7,checkbox_a_P006_8,checkbox_a_P006_9,checkbox_a_P006_10,checkbox_a_P006_11,checkbox_a_P006_12;
    EditText edittext_a_p006_sp11_O;



    private String idHogar;
    private String idVivienda;


    // nuevos
    private int p101;
    private String p101_o;
    private int  p102;
    private String p102_o;
    private int p103;
    private String p103_o;
    private int p103a;
    private int p104;
    private String p104_o;
    private String  p105_1;
    private String  p105_2;
    private String p105_3;
    private String  p105_4;
    private String  p105_5;
    private String  p105_5_o;
    private String  p105_6;
    private String p106_1;
    private String p106_2;
    private String p106_3;
    private String p106_4;
    private String p106_5;
    private String  p106_6;
    private String  p106_7;
    private String  p106_8;
    private String p106_9;
    private String p106_10;
    private String p106_11;
    private String p106_11_o;
    private String p106_12;


    public FragmentP101P107() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentP101P107(String idHogar, String idVivienda, Context context) {
        this.idHogar = idHogar;
        this.idVivienda = idVivienda;
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_p101_p107, container, false);

        //Layaut
        layout_a_p001= (LinearLayout) rootView.findViewById(R.id.layout_a_p001);
        layout_a_p002= (LinearLayout) rootView.findViewById(R.id.layout_a_p002);
        layout_a_p003= (LinearLayout) rootView.findViewById(R.id.layout_a_p003);
        layout_a_p003a= (LinearLayout) rootView.findViewById(R.id.layout_a_p003a);
        layout_a_p004= (LinearLayout) rootView.findViewById(R.id.layout_a_p004);
        layout_a_p005= (LinearLayout) rootView.findViewById(R.id.layout_a_p005);
        layout_a_p006= (LinearLayout) rootView.findViewById(R.id.layout_a_p006);



        //pregunta 1
        radiogroup_a_P001 =rootView.findViewById(R.id.radiogroup_a_P001);
        edittext_a_P001_O = rootView.findViewById(R.id.edittext_a_P001_O);

        //pregunta 2
        radiogroup_a_P002 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P002);
        edittext_a_P002_O = (EditText) rootView.findViewById(R.id.edittext_a_P002_O);

        //pregunta 3
        radiogroup_a_P003 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P003);
        edittext_a_P003_O = (EditText) rootView.findViewById(R.id.edittext_a_P003_O);

        //pregunta 3
        radiogroup_a_P003_a =  (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P003_a);

        //pregunta 4
        radiogroup_a_P004 =(RadioGroup) rootView.findViewById(R.id.radiogroup_a_P004);
        edittext_a_P004_O = (EditText) rootView.findViewById(R.id.edittext_a_P004_O);


        //pregunta 5
        checkbox_a_P005_1 =(CheckBox) rootView.findViewById(R.id.checkbox_a_P005_1);
        checkbox_a_P005_2 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P005_2);
        checkbox_a_P005_3 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P005_3);
        checkbox_a_P005_4 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P005_4);
        checkbox_a_P005_5 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P005_5);
        edittext_a_p005_sp5_O = (EditText) rootView.findViewById(R.id.edittext_a_p005_sp5_O);
        checkbox_a_P005_6 = (CheckBox) rootView.findViewById(R.id.checkbox_a_P005_6);


        //pregunta 6
        checkbox_a_P006_1 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P006_1);
        checkbox_a_P006_2 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P006_2);
        checkbox_a_P006_3 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P006_3);
        checkbox_a_P006_4 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P006_4);
        checkbox_a_P006_5 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P006_5);
        checkbox_a_P006_6 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P006_6);
        checkbox_a_P006_7 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P006_7);
        checkbox_a_P006_8 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P006_8);
        checkbox_a_P006_9 =(CheckBox) rootView.findViewById(R.id.checkbox_a_P006_9);
        checkbox_a_P006_10 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P006_10);
        checkbox_a_P006_11 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P006_11);
        edittext_a_p006_sp11_O = (EditText) rootView.findViewById(R.id.edittext_a_p006_sp11_O);
        checkbox_a_P006_12 =(CheckBox) rootView.findViewById(R.id.checkbox_a_P006_12);


        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //NUEVAS PREGUNTAS
        radiogroup_a_P001.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,8,edittext_a_P001_O);
            }
        });

        radiogroup_a_P002.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,7,edittext_a_P002_O);
            }
        });

        radiogroup_a_P003.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,8,edittext_a_P003_O);
            }
        });

        radiogroup_a_P004.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,8,edittext_a_P004_O);
            }
        });

        controlarChecked(checkbox_a_P005_5,edittext_a_p005_sp5_O);
        controlarChecked(checkbox_a_P006_11,edittext_a_p006_sp11_O);



        //FIN DE NUEVAS PREGUNTAS



        cargarDatos();
    }

    private void controlarEspecifiqueRadio(RadioGroup group, int checkedId, int opcionEsp, EditText editTextEspecifique) {
        int seleccionado = group.indexOfChild(group.findViewById(checkedId));
        if(seleccionado == opcionEsp){
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_enabled);
            editTextEspecifique.setEnabled(true);
        }else{
            editTextEspecifique.setText("");
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_disabled);
            editTextEspecifique.setEnabled(false);
        }
    }

    private void configurarEditText(final EditText editText, final View view, int tipo,int longitud){
        switch (tipo){
            case 0:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud), new InputFilterSoloLetras()});break;
            case 1:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud)});break;
            case 2:editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)});
                editText.setTransformationMethod(new NumericKeyBoardTransformationMethod());break;
        }

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ocultarTeclado(editText);
                    view.requestFocus();
                    return true;
                }
                return false;
            }
        });
//        if (tipo == 2) {
//            editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(),new InputFilter.LengthFilter(longitud)});
//            editText.setInputType(18);
//            editText.setTransformationMethod(null);
//        }
    }

    @Override
    public void guardarDatos() {
        Data data = new Data(context);
        data.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.seccion_a_p101, p101);
        contentValues.put(SQLConstantes.seccion_a_p101_o, p101_o);
        contentValues.put(SQLConstantes.seccion_a_p102,p102);
        contentValues.put(SQLConstantes.seccion_a_p102_o,p102_o);
        contentValues.put(SQLConstantes.seccion_a_p103,p103);
        contentValues.put(SQLConstantes.seccion_a_p103_o,p103_o);
        contentValues.put(SQLConstantes.seccion_a_p103a,p103a);
        contentValues.put(SQLConstantes.seccion_a_p104,p104);
        contentValues.put(SQLConstantes.seccion_a_p104_o,p104_o);
        contentValues.put(SQLConstantes.seccion_a_p105_1,p105_1);
        contentValues.put(SQLConstantes.seccion_a_p105_2,p105_2);
        contentValues.put(SQLConstantes.seccion_a_p105_3,p105_3);
        contentValues.put(SQLConstantes.seccion_a_p105_4,p105_4);
        contentValues.put(SQLConstantes.seccion_a_p105_5,p105_5);
        contentValues.put(SQLConstantes.seccion_a_p105_5_o,p105_5_o);
        contentValues.put(SQLConstantes.seccion_a_p105_6,p105_6);
        contentValues.put(SQLConstantes.seccion_a_p106_1,p106_1);
        contentValues.put(SQLConstantes.seccion_a_p106_2,p106_2);
        contentValues.put(SQLConstantes.seccion_a_p106_3,p106_3);
        contentValues.put(SQLConstantes.seccion_a_p106_4,p106_4);
        contentValues.put(SQLConstantes.seccion_a_p106_5,p106_5);
        contentValues.put(SQLConstantes.seccion_a_p106_6,p106_6);
        contentValues.put(SQLConstantes.seccion_a_p106_7,p106_7);
        contentValues.put(SQLConstantes.seccion_a_p106_8,p106_8);
        contentValues.put(SQLConstantes.seccion_a_p106_9,p106_9);
        contentValues.put(SQLConstantes.seccion_a_p106_10,p106_10);
        contentValues.put(SQLConstantes.seccion_a_p106_11,p106_11);
        contentValues.put(SQLConstantes.seccion_a_p106_11_o,p106_11_o);
        contentValues.put(SQLConstantes.seccion_a_p106_12,p106_12);

        if(!data.existeElemento(getNombreTabla(),idVivienda)){
            Modulo1V modulo1V = new Modulo1V();
            modulo1V.set_id(idVivienda);
            data.insertarElemento(getNombreTabla(), modulo1V.toValues());
        }
        data.actualizarElemento(getNombreTabla(),contentValues,idVivienda);
        data.close();
    }

    @Override
    public void llenarVariables() {
       //Nuevas preguntas
        p101 = radiogroup_a_P001.indexOfChild(radiogroup_a_P001.findViewById(radiogroup_a_P001.getCheckedRadioButtonId()));
        p101_o = edittext_a_P001_O.getText().toString();

        p102 = radiogroup_a_P002.indexOfChild(radiogroup_a_P002.findViewById(radiogroup_a_P002.getCheckedRadioButtonId()));
        p102_o = edittext_a_P002_O.getText().toString();

        p103 = radiogroup_a_P003.indexOfChild(radiogroup_a_P003.findViewById(radiogroup_a_P003.getCheckedRadioButtonId()));
        p103_o = edittext_a_P003_O.getText().toString();

        p103a =radiogroup_a_P003_a.indexOfChild(radiogroup_a_P003_a.findViewById(radiogroup_a_P003_a.getCheckedRadioButtonId()));

        p104 = radiogroup_a_P004.indexOfChild(radiogroup_a_P004.findViewById(radiogroup_a_P004.getCheckedRadioButtonId()));
        p104_o = edittext_a_P004_O.getText().toString();

        if(checkbox_a_P005_1.isChecked()) p105_1 = "1"; else p105_1 = "0";
        if(checkbox_a_P005_2.isChecked()) p105_2 = "1"; else p105_2 = "0";
        if(checkbox_a_P005_3.isChecked()) p105_3 = "1"; else p105_3 = "0";
        if(checkbox_a_P005_4.isChecked()) p105_4 = "1"; else p105_4 = "0";
        if(checkbox_a_P005_5.isChecked()) p105_5 = "1"; else p105_5 = "0";
        p105_5_o = edittext_a_p005_sp5_O.getText().toString();
        if(checkbox_a_P005_6.isChecked()) p105_6 = "1"; else p105_6 = "0";


        if(checkbox_a_P006_1.isChecked()) p106_1 = "1"; else p106_1 = "0";
        if(checkbox_a_P006_2.isChecked()) p106_2 = "1"; else p106_2 = "0";
        if(checkbox_a_P006_3.isChecked()) p106_3 = "1"; else p106_3 = "0";
        if(checkbox_a_P006_4.isChecked()) p106_4 = "1"; else p106_4 = "0";
        if(checkbox_a_P006_5.isChecked()) p106_5 = "1"; else p106_5 = "0";
        if(checkbox_a_P006_6.isChecked()) p106_6 = "1"; else p106_6 = "0";
        if(checkbox_a_P006_7.isChecked()) p106_7 = "1"; else p106_7 = "0";
        if(checkbox_a_P006_8.isChecked()) p106_8 = "1"; else p106_8 = "0";
        if(checkbox_a_P006_9.isChecked()) p106_9 = "1"; else p106_9 = "0";
        if(checkbox_a_P006_10.isChecked()) p106_10 = "1"; else p106_10 = "0";
        if(checkbox_a_P006_11.isChecked()) p106_11 = "1"; else p106_11 = "0";
        p106_11_o = edittext_a_p006_sp11_O.getText().toString();
        if(checkbox_a_P006_12.isChecked()) p106_12 = "1"; else p106_12 = "0";


      //Fin de nuevas preguntas

    }

    @Override
    public void cargarDatos() {
        Data data = new Data(context);
        data.open();
        if (data.existeElemento(getNombreTabla(),idVivienda)) {
            Modulo1V modulo1V = data.getModulo1V(idVivienda);

            //Nuevas Preguntas
            if (!modulo1V.getP101().equals("-1"))
                ((RadioButton) radiogroup_a_P001.getChildAt(Integer.parseInt(modulo1V.getP101()))).setChecked(true);
            edittext_a_P001_O.setText(modulo1V.getP101_o());

            if (!modulo1V.getP102().equals("-1"))
                ((RadioButton) radiogroup_a_P002.getChildAt(Integer.parseInt(modulo1V.getP102()))).setChecked(true);
            edittext_a_P002_O.setText(modulo1V.getP102_o());

            if (!modulo1V.getP103().equals("-1"))
                ((RadioButton) radiogroup_a_P003.getChildAt(Integer.parseInt(modulo1V.getP103()))).setChecked(true);
            edittext_a_P003_O.setText(modulo1V.getP103_o());

            if (!modulo1V.getP103a().equals("-1"))
                ((RadioButton) radiogroup_a_P003_a.getChildAt(Integer.parseInt(modulo1V.getP103a()))).setChecked(true);


            if (!modulo1V.getP104().equals("-1"))
                ((RadioButton) radiogroup_a_P004.getChildAt(Integer.parseInt(modulo1V.getP104()))).setChecked(true);
            edittext_a_P004_O.setText(modulo1V.getP104_o());


            if(modulo1V.getP105_1().equals("1")) checkbox_a_P005_1.setChecked(true);
            if(modulo1V.getP105_1().equals("0")) checkbox_a_P005_1.setChecked(false);
            if(modulo1V.getP105_2().equals("1")) checkbox_a_P005_2.setChecked(true);
            if(modulo1V.getP105_2().equals("0")) checkbox_a_P005_2.setChecked(false);
            if(modulo1V.getP105_3().equals("1")) checkbox_a_P005_3.setChecked(true);
            if(modulo1V.getP105_3().equals("0")) checkbox_a_P005_3.setChecked(false);
            if(modulo1V.getP105_4().equals("1")) checkbox_a_P005_4.setChecked(true);
            if(modulo1V.getP105_4().equals("0")) checkbox_a_P005_4.setChecked(false);
            if(modulo1V.getP105_5().equals("1")) checkbox_a_P005_5.setChecked(true);
            if(modulo1V.getP105_5().equals("0")) checkbox_a_P005_5.setChecked(false);
            edittext_a_p005_sp5_O.setText(modulo1V.getP105_5_o());
            if(modulo1V.getP105_6().equals("1")) checkbox_a_P005_6.setChecked(true);
            if(modulo1V.getP105_6().equals("0")) checkbox_a_P005_6.setChecked(false);


            if(modulo1V.getP106_1().equals("1")) checkbox_a_P006_1.setChecked(true);
            if(modulo1V.getP106_1().equals("0")) checkbox_a_P006_1.setChecked(false);
            if(modulo1V.getP106_2().equals("1")) checkbox_a_P006_2.setChecked(true);
            if(modulo1V.getP106_2().equals("0")) checkbox_a_P006_2.setChecked(false);
            if(modulo1V.getP106_3().equals("1")) checkbox_a_P006_3.setChecked(true);
            if(modulo1V.getP106_3().equals("0")) checkbox_a_P006_3.setChecked(false);
            if(modulo1V.getP106_4().equals("1")) checkbox_a_P006_4.setChecked(true);
            if(modulo1V.getP106_4().equals("0")) checkbox_a_P006_4.setChecked(false);
            if(modulo1V.getP106_5().equals("1")) checkbox_a_P006_5.setChecked(true);
            if(modulo1V.getP106_5().equals("0")) checkbox_a_P006_5.setChecked(false);
            if(modulo1V.getP106_6().equals("1")) checkbox_a_P006_6.setChecked(true);
            if(modulo1V.getP106_6().equals("0")) checkbox_a_P006_6.setChecked(false);
            if(modulo1V.getP106_7().equals("1")) checkbox_a_P006_7.setChecked(true);
            if(modulo1V.getP106_7().equals("0")) checkbox_a_P006_7.setChecked(false);
            if(modulo1V.getP106_8().equals("1")) checkbox_a_P006_8.setChecked(true);
            if(modulo1V.getP106_8().equals("0")) checkbox_a_P006_8.setChecked(false);
            if(modulo1V.getP106_9().equals("1")) checkbox_a_P006_9.setChecked(true);
            if(modulo1V.getP106_9().equals("0")) checkbox_a_P006_9.setChecked(false);
            if(modulo1V.getP106_10().equals("1")) checkbox_a_P006_10.setChecked(true);
            if(modulo1V.getP106_10().equals("0")) checkbox_a_P006_10.setChecked(false);

            if(modulo1V.getP106_11().equals("1")) checkbox_a_P006_11.setChecked(true);
            if(modulo1V.getP106_11().equals("0")) checkbox_a_P006_11.setChecked(false);
            edittext_a_p006_sp11_O.setText(modulo1V.getP106_11_o());

            if(modulo1V.getP106_12().equals("1")) checkbox_a_P006_12.setChecked(true);
            if(modulo1V.getP106_12().equals("0")) checkbox_a_P006_12.setChecked(false);



            //Fin de Nuevas Preguntas

        }
        data.close();
    }

    @Override
    public void llenarVista() {

    }

    @Override
    public boolean validarDatos() {
        boolean verificar_p104_incorrecto=false, verificar_p105_incorrecto=false, no_sirve=false;
        llenarVariables();

        //NUEVAS PREGUNTAS

        if (p101 == -1){mostrarMensaje("PREGUNTA 101: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (p101 == 8){
                if (p101_o.trim().equals("")){mostrarMensaje("PREGUNTA 101: DEBE ESPECIFICAR");return false;}
            }
        }

        if (p102 == -1){mostrarMensaje("PREGUNTA 102sss: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (p102 == 8){
                if (p102_o.trim().equals("")){mostrarMensaje("PREGUNTA 102asassa: DEBE ESPECIFICAR");return false;}
            }
        }

        if (p103 == -1){mostrarMensaje("PREGUNTA 103sss: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (p103 == 8){
                if (p103_o.trim().equals("")){mostrarMensaje("PREGUNTA 103asassa: DEBE ESPECIFICAR");return false;}
            }
        }

        if (p103a == -1){mostrarMensaje("PREGUNTA 103asss: DEBE MARCAR UNA OPCIÓN"); return false;}

        if (p104 == -1){mostrarMensaje("PREGUNTA 104sss: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (p104 == 8){
                if (p104_o.trim().equals("")){mostrarMensaje("PREGUNTA 104asassa: DEBE ESPECIFICAR");return false;}
            }
        }

        if (p105_1.equals("0") && p105_2.equals("0") && p105_3.equals("0") && p105_4.equals("0") && p105_5.equals("0") && p105_6.equals("0")) {
            mostrarMensaje("PREGUNTA 105: DEBE SELECCIONAR ALGUNA OPCION");return false;
        }else{
            if (p105_5.equals("1")){
                if (p105_5_o.trim().equals("")){
                    mostrarMensaje("PREGUNTA 05: DEBE ESPECIFICAR");return false;
                }

            }
        }



        if (p106_1.equals("0") && p106_2.equals("0") && p106_3.equals("0") && p106_4.equals("0") && p106_5.equals("0") && p106_6.equals("0")
                && p106_7.equals("0") && p106_8.equals("0") && p106_9.equals("0")
                && p106_10.equals("0") && p106_11.equals("0") && p106_12.equals("0")) {
            mostrarMensaje("PREGUNTA 06: DEBE SELECCIONAR ALGUNA OPCION");return false;
        }else{
            if (p106_11.equals("1")){
                if (p106_11_o.trim().equals("")){
                    mostrarMensaje("PREGUNTA 06: DEBE ESPECIFICAR");return false;
                }

            }
        }



        //FIN DE NUEVAS PREGUNTAS

        return true;
    }

    @Override
    public String getNombreTabla() {
        return SQLConstantes.tablamodulo1v;
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void controlarChecked(CheckBox checkBox,final EditText editText){
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    editText.setBackgroundResource(R.drawable.input_text_enabled);
                    editText.setEnabled(true);
                }else{
                    editText.setText("");
                    editText.setBackgroundResource(R.drawable.input_text_disabled);
                    editText.setEnabled(false);
                }
            }
        });
    }

}
