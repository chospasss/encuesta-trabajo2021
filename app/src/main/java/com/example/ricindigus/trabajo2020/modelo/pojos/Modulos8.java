package com.example.ricindigus.trabajo2020.modelo.pojos;

import android.content.ContentValues;

import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;

public class Modulos8 {
    private String id;
    private String id_vivienda;
    private String numero_hogar;
    private String numero_residente;
    private String inf_300;
    private String p341_1;
    private String p341_2;
    private String p341_3;
    private String p341_4;
    private String p342;
    private String p342_o;
    private String p342a;
    private String p342b;
    private String p342b_o;
    private String p343;
    private String id_p344;
    private String id_p344_item;
    private String id_p344_item_o;
    private String p344_1;
    private String p345_1_1;
    private String p345_1_2;
    private String p345_1_3;
    private String p345_1_4;
    private String p345_1_5;
    private String p345_1_6;
    private String p345_1_7;
    private String p345_1_8;
    private String p345_1_8_o;
    private String p345_1_9;
    private String p344_2;
    private String p345_2_1;
    private String p345_2_2;
    private String p345_2_3;
    private String p345_2_4;
    private String p345_2_5;
    private String p345_2_6;
    private String p345_2_7;
    private String p345_2_8;
    private String p345_2_8_o;
    private String p345_2_9;
    private String p344_3;
    private String p345_3_1;
    private String p345_3_2;
    private String p345_3_3;
    private String p345_3_4;
    private String p345_3_5;
    private String p345_3_6;
    private String p345_3_7;
    private String p345_3_8;
    private String p345_3_8_o;
    private String p345_3_9;
    private String p344_4;
    private String p344_4_o;
    private String p345_4_1;
    private String p345_4_2;
    private String p345_4_3;
    private String p345_4_4;
    private String p345_4_5;
    private String p345_4_6;
    private String p345_4_7;
    private String p345_4_8;
    private String p345_4_8_o;
    private String p345_4_9;
    private String p346_1;
    private String p346_2;
    private String p346_3;
    private String p346_4;
    private String p346_5;
    private String p346_6;
    private String p346_6_o;
    private String obs_h;


    public Modulos8(){

         id="";
         id_vivienda="";
         numero_hogar="";
         numero_residente="";
         inf_300="";
         p341_1="";
         p341_2="";
         p341_3="";
         p341_4="";
         p342="";
         p342_o="";
         p342a="";
         p342b="";
         p342b_o="";
         p343="";
         id_p344="";
         id_p344_item="";
         id_p344_item_o="";
         p344_1="";
         p345_1_1="";
         p345_1_2="";
         p345_1_3="";
         p345_1_4="";
         p345_1_5="";
         p345_1_6="";
         p345_1_7="";
         p345_1_8="";
         p345_1_8_o="";
         p345_1_9="";
         p344_2="";
         p345_2_1="";
         p345_2_2="";
         p345_2_3="";
         p345_2_4="";
         p345_2_5="";
         p345_2_6="";
         p345_2_7="";
         p345_2_8="";
         p345_2_8_o="";
         p345_2_9="";
         p344_3="";
         p345_3_1="";
         p345_3_2="";
         p345_3_3="";
         p345_3_4="";
         p345_3_5="";
         p345_3_6="";
         p345_3_7="";
         p345_3_8="";
         p345_3_8_o="";
         p345_3_9="";
         p344_4="";
         p344_4_o="";
         p345_4_1="";
         p345_4_2="";
         p345_4_3="";
         p345_4_4="";
         p345_4_5="";
         p345_4_6="";
         p345_4_7="";
         p345_4_8="";
         p345_4_8_o="";
         p345_4_9="";
         p346_1="";
         p346_2="";
         p346_3="";
         p346_4="";
         p346_5="";
         p346_6="";
         p346_6_o="";
         obs_h="";

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_vivienda() {
        return id_vivienda;
    }

    public void setId_vivienda(String id_vivienda) {
        this.id_vivienda = id_vivienda;
    }

    public String getNumero_hogar() {
        return numero_hogar;
    }

    public void setNumero_hogar(String numero_hogar) {
        this.numero_hogar = numero_hogar;
    }

    public String getNumero_residente() {
        return numero_residente;
    }

    public void setNumero_residente(String numero_residente) {
        this.numero_residente = numero_residente;
    }

    public String getInf_300() {
        return inf_300;
    }

    public void setInf_300(String inf_300) {
        this.inf_300 = inf_300;
    }

    public String getP341_1() {
        return p341_1;
    }

    public void setP341_1(String p341_1) {
        this.p341_1 = p341_1;
    }

    public String getP341_2() {
        return p341_2;
    }

    public void setP341_2(String p341_2) {
        this.p341_2 = p341_2;
    }

    public String getP341_3() {
        return p341_3;
    }

    public void setP341_3(String p341_3) {
        this.p341_3 = p341_3;
    }

    public String getP341_4() {
        return p341_4;
    }

    public void setP341_4(String p341_4) {
        this.p341_4 = p341_4;
    }

    public String getP342() {
        return p342;
    }

    public void setP342(String p342) {
        this.p342 = p342;
    }

    public String getP342_o() {
        return p342_o;
    }

    public void setP342_o(String p342_o) {
        this.p342_o = p342_o;
    }

    public String getP342a() {
        return p342a;
    }

    public void setP342a(String p342a) {
        this.p342a = p342a;
    }

    public String getP342b() {
        return p342b;
    }

    public void setP342b(String p342b) {
        this.p342b = p342b;
    }

    public String getP342b_o() {
        return p342b_o;
    }

    public void setP342b_o(String p342b_o) {
        this.p342b_o = p342b_o;
    }

    public String getP343() {
        return p343;
    }

    public void setP343(String p343) {
        this.p343 = p343;
    }

    public String getId_p344() {
        return id_p344;
    }

    public void setId_p344(String id_p344) {
        this.id_p344 = id_p344;
    }

    public String getId_p344_item() {
        return id_p344_item;
    }

    public void setId_p344_item(String id_p344_item) {
        this.id_p344_item = id_p344_item;
    }

    public String getId_p344_item_o() {
        return id_p344_item_o;
    }

    public void setId_p344_item_o(String id_p344_item_o) {
        this.id_p344_item_o = id_p344_item_o;
    }

    public String getP344_1() {
        return p344_1;
    }

    public void setP344_1(String p344_1) {
        this.p344_1 = p344_1;
    }

    public String getP345_1_1() {
        return p345_1_1;
    }

    public void setP345_1_1(String p345_1_1) {
        this.p345_1_1 = p345_1_1;
    }

    public String getP345_1_2() {
        return p345_1_2;
    }

    public void setP345_1_2(String p345_1_2) {
        this.p345_1_2 = p345_1_2;
    }

    public String getP345_1_3() {
        return p345_1_3;
    }

    public void setP345_1_3(String p345_1_3) {
        this.p345_1_3 = p345_1_3;
    }

    public String getP345_1_4() {
        return p345_1_4;
    }

    public void setP345_1_4(String p345_1_4) {
        this.p345_1_4 = p345_1_4;
    }

    public String getP345_1_5() {
        return p345_1_5;
    }

    public void setP345_1_5(String p345_1_5) {
        this.p345_1_5 = p345_1_5;
    }

    public String getP345_1_6() {
        return p345_1_6;
    }

    public void setP345_1_6(String p345_1_6) {
        this.p345_1_6 = p345_1_6;
    }

    public String getP345_1_7() {
        return p345_1_7;
    }

    public void setP345_1_7(String p345_1_7) {
        this.p345_1_7 = p345_1_7;
    }

    public String getP345_1_8() {
        return p345_1_8;
    }

    public void setP345_1_8(String p345_1_8) {
        this.p345_1_8 = p345_1_8;
    }

    public String getP345_1_8_o() {
        return p345_1_8_o;
    }

    public void setP345_1_8_o(String p345_1_8_o) {
        this.p345_1_8_o = p345_1_8_o;
    }

    public String getP345_1_9() {
        return p345_1_9;
    }

    public void setP345_1_9(String p345_1_9) {
        this.p345_1_9 = p345_1_9;
    }

    public String getP344_2() {
        return p344_2;
    }

    public void setP344_2(String p344_2) {
        this.p344_2 = p344_2;
    }

    public String getP345_2_1() {
        return p345_2_1;
    }

    public void setP345_2_1(String p345_2_1) {
        this.p345_2_1 = p345_2_1;
    }

    public String getP345_2_2() {
        return p345_2_2;
    }

    public void setP345_2_2(String p345_2_2) {
        this.p345_2_2 = p345_2_2;
    }

    public String getP345_2_3() {
        return p345_2_3;
    }

    public void setP345_2_3(String p345_2_3) {
        this.p345_2_3 = p345_2_3;
    }

    public String getP345_2_4() {
        return p345_2_4;
    }

    public void setP345_2_4(String p345_2_4) {
        this.p345_2_4 = p345_2_4;
    }

    public String getP345_2_5() {
        return p345_2_5;
    }

    public void setP345_2_5(String p345_2_5) {
        this.p345_2_5 = p345_2_5;
    }

    public String getP345_2_6() {
        return p345_2_6;
    }

    public void setP345_2_6(String p345_2_6) {
        this.p345_2_6 = p345_2_6;
    }

    public String getP345_2_7() {
        return p345_2_7;
    }

    public void setP345_2_7(String p345_2_7) {
        this.p345_2_7 = p345_2_7;
    }

    public String getP345_2_8() {
        return p345_2_8;
    }

    public void setP345_2_8(String p345_2_8) {
        this.p345_2_8 = p345_2_8;
    }

    public String getP345_2_8_o() {
        return p345_2_8_o;
    }

    public void setP345_2_8_o(String p345_2_8_o) {
        this.p345_2_8_o = p345_2_8_o;
    }

    public String getP345_2_9() {
        return p345_2_9;
    }

    public void setP345_2_9(String p345_2_9) {
        this.p345_2_9 = p345_2_9;
    }

    public String getP344_3() {
        return p344_3;
    }

    public void setP344_3(String p344_3) {
        this.p344_3 = p344_3;
    }

    public String getP345_3_1() {
        return p345_3_1;
    }

    public void setP345_3_1(String p345_3_1) {
        this.p345_3_1 = p345_3_1;
    }

    public String getP345_3_2() {
        return p345_3_2;
    }

    public void setP345_3_2(String p345_3_2) {
        this.p345_3_2 = p345_3_2;
    }

    public String getP345_3_3() {
        return p345_3_3;
    }

    public void setP345_3_3(String p345_3_3) {
        this.p345_3_3 = p345_3_3;
    }

    public String getP345_3_4() {
        return p345_3_4;
    }

    public void setP345_3_4(String p345_3_4) {
        this.p345_3_4 = p345_3_4;
    }

    public String getP345_3_5() {
        return p345_3_5;
    }

    public void setP345_3_5(String p345_3_5) {
        this.p345_3_5 = p345_3_5;
    }

    public String getP345_3_6() {
        return p345_3_6;
    }

    public void setP345_3_6(String p345_3_6) {
        this.p345_3_6 = p345_3_6;
    }

    public String getP345_3_7() {
        return p345_3_7;
    }

    public void setP345_3_7(String p345_3_7) {
        this.p345_3_7 = p345_3_7;
    }

    public String getP345_3_8() {
        return p345_3_8;
    }

    public void setP345_3_8(String p345_3_8) {
        this.p345_3_8 = p345_3_8;
    }

    public String getP345_3_8_o() {
        return p345_3_8_o;
    }

    public void setP345_3_8_o(String p345_3_8_o) {
        this.p345_3_8_o = p345_3_8_o;
    }

    public String getP345_3_9() {
        return p345_3_9;
    }

    public void setP345_3_9(String p345_3_9) {
        this.p345_3_9 = p345_3_9;
    }

    public String getP344_4() {
        return p344_4;
    }

    public void setP344_4(String p344_4) {
        this.p344_4 = p344_4;
    }

    public String getP344_4_o() {
        return p344_4_o;
    }

    public void setP344_4_o(String p344_4_o) {
        this.p344_4_o = p344_4_o;
    }

    public String getP345_4_1() {
        return p345_4_1;
    }

    public void setP345_4_1(String p345_4_1) {
        this.p345_4_1 = p345_4_1;
    }

    public String getP345_4_2() {
        return p345_4_2;
    }

    public void setP345_4_2(String p345_4_2) {
        this.p345_4_2 = p345_4_2;
    }

    public String getP345_4_3() {
        return p345_4_3;
    }

    public void setP345_4_3(String p345_4_3) {
        this.p345_4_3 = p345_4_3;
    }

    public String getP345_4_4() {
        return p345_4_4;
    }

    public void setP345_4_4(String p345_4_4) {
        this.p345_4_4 = p345_4_4;
    }

    public String getP345_4_5() {
        return p345_4_5;
    }

    public void setP345_4_5(String p345_4_5) {
        this.p345_4_5 = p345_4_5;
    }

    public String getP345_4_6() {
        return p345_4_6;
    }

    public void setP345_4_6(String p345_4_6) {
        this.p345_4_6 = p345_4_6;
    }

    public String getP345_4_7() {
        return p345_4_7;
    }

    public void setP345_4_7(String p345_4_7) {
        this.p345_4_7 = p345_4_7;
    }

    public String getP345_4_8() {
        return p345_4_8;
    }

    public void setP345_4_8(String p345_4_8) {
        this.p345_4_8 = p345_4_8;
    }

    public String getP345_4_8_o() {
        return p345_4_8_o;
    }

    public void setP345_4_8_o(String p345_4_8_o) {
        this.p345_4_8_o = p345_4_8_o;
    }

    public String getP345_4_9() {
        return p345_4_9;
    }

    public void setP345_4_9(String p345_4_9) {
        this.p345_4_9 = p345_4_9;
    }

    public String getP346_1() {
        return p346_1;
    }

    public void setP346_1(String p346_1) {
        this.p346_1 = p346_1;
    }

    public String getP346_2() {
        return p346_2;
    }

    public void setP346_2(String p346_2) {
        this.p346_2 = p346_2;
    }

    public String getP346_3() {
        return p346_3;
    }

    public void setP346_3(String p346_3) {
        this.p346_3 = p346_3;
    }

    public String getP346_4() {
        return p346_4;
    }

    public void setP346_4(String p346_4) {
        this.p346_4 = p346_4;
    }

    public String getP346_5() {
        return p346_5;
    }

    public void setP346_5(String p346_5) {
        this.p346_5 = p346_5;
    }

    public String getP346_6() {
        return p346_6;
    }

    public void setP346_6(String p346_6) {
        this.p346_6 = p346_6;
    }

    public String getP346_6_o() {
        return p346_6_o;
    }

    public void setP346_6_o(String p346_6_o) {
        this.p346_6_o = p346_6_o;
    }

    public String getObs_h() {
        return obs_h;
    }

    public void setObs_h(String obs_h) {
        this.obs_h = obs_h;
    }


    public ContentValues toValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.seccion_h_id,id);
        contentValues.put(SQLConstantes.seccion_h_id_vivienda,id_vivienda);
        contentValues.put(SQLConstantes.seccion_h_numero_hogar,numero_hogar);
        contentValues.put(SQLConstantes.seccion_h_numero_residente,numero_residente);
        contentValues.put(SQLConstantes.seccion_h_inf_300,inf_300);
        contentValues.put(SQLConstantes.seccion_h_p341_1,p341_1);
        contentValues.put(SQLConstantes.seccion_h_p341_2,p341_2);
        contentValues.put(SQLConstantes.seccion_h_p341_3,p341_3);
        contentValues.put(SQLConstantes.seccion_h_p341_4,p341_4);
        contentValues.put(SQLConstantes.seccion_h_p342,p342);
        contentValues.put(SQLConstantes.seccion_h_p342_o,p342_o);
        contentValues.put(SQLConstantes.seccion_h_p342a,p342a);
        contentValues.put(SQLConstantes.seccion_h_p342b,p342b);
        contentValues.put(SQLConstantes.seccion_h_p342b_o,p342b_o);
        contentValues.put(SQLConstantes.seccion_h_p343,p343);
        contentValues.put(SQLConstantes.seccion_h_id_p344,id_p344);
        contentValues.put(SQLConstantes.seccion_h_id_p344_item,id_p344_item);
        contentValues.put(SQLConstantes.seccion_h_id_p344_item_o,id_p344_item_o);
        contentValues.put(SQLConstantes.seccion_h_p344_1,p344_1);
        contentValues.put(SQLConstantes.seccion_h_p345_1_1,p345_1_1);
        contentValues.put(SQLConstantes.seccion_h_p345_1_2,p345_1_2);
        contentValues.put(SQLConstantes.seccion_h_p345_1_3,p345_1_3);
        contentValues.put(SQLConstantes.seccion_h_p345_1_4,p345_1_4);
        contentValues.put(SQLConstantes.seccion_h_p345_1_5,p345_1_5);
        contentValues.put(SQLConstantes.seccion_h_p345_1_6,p345_1_6);
        contentValues.put(SQLConstantes.seccion_h_p345_1_7,p345_1_7);
        contentValues.put(SQLConstantes.seccion_h_p345_1_8,p345_1_8);
        contentValues.put(SQLConstantes.seccion_h_p345_1_8_o,p345_1_8_o);
        contentValues.put(SQLConstantes.seccion_h_p345_1_9,p345_1_9);
        contentValues.put(SQLConstantes.seccion_h_p344_2,p344_2);
        contentValues.put(SQLConstantes.seccion_h_p345_2_1,p345_2_1);
        contentValues.put(SQLConstantes.seccion_h_p345_2_2,p345_2_2);
        contentValues.put(SQLConstantes.seccion_h_p345_2_3,p345_2_3);
        contentValues.put(SQLConstantes.seccion_h_p345_2_4,p345_2_4);
        contentValues.put(SQLConstantes.seccion_h_p345_2_5,p345_2_5);
        contentValues.put(SQLConstantes.seccion_h_p345_2_6,p345_2_6);
        contentValues.put(SQLConstantes.seccion_h_p345_2_7,p345_2_7);
        contentValues.put(SQLConstantes.seccion_h_p345_2_8,p345_2_8);
        contentValues.put(SQLConstantes.seccion_h_p345_2_8_o,p345_2_8_o);
        contentValues.put(SQLConstantes.seccion_h_p345_2_9,p345_2_9);
        contentValues.put(SQLConstantes.seccion_h_p344_3,p344_3);
        contentValues.put(SQLConstantes.seccion_h_p345_3_1,p345_3_1);
        contentValues.put(SQLConstantes.seccion_h_p345_3_2,p345_3_2);
        contentValues.put(SQLConstantes.seccion_h_p345_3_3,p345_3_3);
        contentValues.put(SQLConstantes.seccion_h_p345_3_4,p345_3_4);
        contentValues.put(SQLConstantes.seccion_h_p345_3_5,p345_3_5);
        contentValues.put(SQLConstantes.seccion_h_p345_3_6,p345_3_6);
        contentValues.put(SQLConstantes.seccion_h_p345_3_7,p345_3_7);
        contentValues.put(SQLConstantes.seccion_h_p345_3_8,p345_3_8);
        contentValues.put(SQLConstantes.seccion_h_p345_3_8_o,p345_3_8_o);
        contentValues.put(SQLConstantes.seccion_h_p345_3_9,p345_3_9);
        contentValues.put(SQLConstantes.seccion_h_p344_4,p344_4);
        contentValues.put(SQLConstantes.seccion_h_p344_4_o,p344_4_o);
        contentValues.put(SQLConstantes.seccion_h_p345_4_1,p345_4_1);
        contentValues.put(SQLConstantes.seccion_h_p345_4_2,p345_4_2);
        contentValues.put(SQLConstantes.seccion_h_p345_4_3,p345_4_3);
        contentValues.put(SQLConstantes.seccion_h_p345_4_4,p345_4_4);
        contentValues.put(SQLConstantes.seccion_h_p345_4_5,p345_4_5);
        contentValues.put(SQLConstantes.seccion_h_p345_4_6,p345_4_6);
        contentValues.put(SQLConstantes.seccion_h_p345_4_7,p345_4_7);
        contentValues.put(SQLConstantes.seccion_h_p345_4_8,p345_4_8);
        contentValues.put(SQLConstantes.seccion_h_p345_4_8_o,p345_4_8_o);
        contentValues.put(SQLConstantes.seccion_h_p345_4_9,p345_4_9);
        contentValues.put(SQLConstantes.seccion_h_p346_1,p346_1);
        contentValues.put(SQLConstantes.seccion_h_p346_2,p346_2);
        contentValues.put(SQLConstantes.seccion_h_p346_3,p346_3);
        contentValues.put(SQLConstantes.seccion_h_p346_4,p346_4);
        contentValues.put(SQLConstantes.seccion_h_p346_5,p346_5);
        contentValues.put(SQLConstantes.seccion_h_p346_6,p346_6);
        contentValues.put(SQLConstantes.seccion_h_p346_6_o,p346_6_o);
        contentValues.put(SQLConstantes.seccion_h_obs_h,obs_h);
        return contentValues;
    }

}
