package com.example.ricindigus.trabajo2020.modelo.pojos;

import android.content.ContentValues;

public class Modulos6 {
    private String id;
    private String id_vivienda;
    private String numero_hogar;
    private String numero_residente;
    private String inf_300;
    private String p326;
    private String p327;
    private String p327_o;
    private String p328;
    private String p329_1;
    private String p329_2;
    private String p329_3;
    private String p329_4;
    private String p329_5;
    private String p329_6;
    private String p329_7;
    private String p329_8;
    private String p329_8_o;
    private String p330;
    private String p331;
    private String p332;
    private String p333_1;
    private String p333_2;
    private String p333_3;
    private String p333_4;
    private String p333_5;
    private String p333_5_o;
    private String p334;
    private String p335;
    private String p336_1;
    private String p337_1;
    private String p336_2;
    private String p337_2;
    private String p336_3;
    private String p337_3;
    private String p336_4;
    private String p337_4;
    private String p336_5;
    private String p337_5;
    private String p336_6;
    private String p337_6;
    private String p336_7;
    private String p336_7_o;
    private String p337_7;
    private String p336_8;
    private String p336_8_o;
    private String p337_8;
    private String obs_f;


    public Modulos6(){
         id= "";
         id_vivienda= "";
         numero_hogar= "";
         numero_residente= "";
         inf_300= "";
        p326= "";
        p327= "";
        p327_o= "";
        p328= "";
        p329_1= "";
        p329_2= "";
        p329_3= "";
        p329_4= "";
        p329_5= "";
        p329_6= "";
        p329_7= "";
        p329_8= "";
        p329_8_o= "";
        p330= "";
        p331= "";
        p332= "";
        p333_1= "";
        p333_2= "";
        p333_3= "";
        p333_4= "";
        p333_5= "";
        p333_5_o= "";
        p334= "";
        p335= "";
        p336_1= "";
        p337_1= "";
        p336_2= "";
        p337_2= "";
        p336_3= "";
        p337_3= "";
        p336_4= "";
        p337_4= "";
        p336_5= "";
        p337_5= "";
        p336_6= "";
        p337_6= "";
        p336_7= "";
        p336_7_o= "";
        p337_7= "";
        p336_8= "";
        p336_8_o= "";
        p337_8= "";
        obs_f= "";

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_vivienda() {
        return id_vivienda;
    }

    public void setId_vivienda(String id_vivienda) {
        this.id_vivienda = id_vivienda;
    }

    public String getNumero_hogar() {
        return numero_hogar;
    }

    public void setNumero_hogar(String numero_hogar) {
        this.numero_hogar = numero_hogar;
    }

    public String getNumero_residente() {
        return numero_residente;
    }

    public void setNumero_residente(String numero_residente) {
        this.numero_residente = numero_residente;
    }

    public String getInf_300() {
        return inf_300;
    }

    public void setInf_300(String inf_300) {
        this.inf_300 = inf_300;
    }

    public String getP326() {
        return p326;
    }

    public void setP326(String p326) {
        this.p326 = p326;
    }

    public String getP327() {
        return p327;
    }

    public void setP327(String p327) {
        this.p327 = p327;
    }

    public String getP327_o() {
        return p327_o;
    }

    public void setP327_o(String p327_o) {
        this.p327_o = p327_o;
    }

    public String getP328() {
        return p328;
    }

    public void setP328(String p328) {
        this.p328 = p328;
    }

    public String getP329_1() {
        return p329_1;
    }

    public void setP329_1(String p329_1) {
        this.p329_1 = p329_1;
    }

    public String getP329_2() {
        return p329_2;
    }

    public void setP329_2(String p329_2) {
        this.p329_2 = p329_2;
    }

    public String getP329_3() {
        return p329_3;
    }

    public void setP329_3(String p329_3) {
        this.p329_3 = p329_3;
    }

    public String getP329_4() {
        return p329_4;
    }

    public void setP329_4(String p329_4) {
        this.p329_4 = p329_4;
    }

    public String getP329_5() {
        return p329_5;
    }

    public void setP329_5(String p329_5) {
        this.p329_5 = p329_5;
    }

    public String getP329_6() {
        return p329_6;
    }

    public void setP329_6(String p329_6) {
        this.p329_6 = p329_6;
    }

    public String getP329_7() {
        return p329_7;
    }

    public void setP329_7(String p329_7) {
        this.p329_7 = p329_7;
    }

    public String getP329_8() {
        return p329_8;
    }

    public void setP329_8(String p329_8) {
        this.p329_8 = p329_8;
    }

    public String getP329_8_o() {
        return p329_8_o;
    }

    public void setP329_8_o(String p329_8_o) {
        this.p329_8_o = p329_8_o;
    }

    public String getP330() {
        return p330;
    }

    public void setP330(String p330) {
        this.p330 = p330;
    }

    public String getP331() {
        return p331;
    }

    public void setP331(String p331) {
        this.p331 = p331;
    }

    public String getP332() {
        return p332;
    }

    public void setP332(String p332) {
        this.p332 = p332;
    }

    public String getP333_1() {
        return p333_1;
    }

    public void setP333_1(String p333_1) {
        this.p333_1 = p333_1;
    }

    public String getP333_2() {
        return p333_2;
    }

    public void setP333_2(String p333_2) {
        this.p333_2 = p333_2;
    }

    public String getP333_3() {
        return p333_3;
    }

    public void setP333_3(String p333_3) {
        this.p333_3 = p333_3;
    }

    public String getP333_4() {
        return p333_4;
    }

    public void setP333_4(String p333_4) {
        this.p333_4 = p333_4;
    }

    public String getP333_5() {
        return p333_5;
    }

    public void setP333_5(String p333_5) {
        this.p333_5 = p333_5;
    }

    public String getP333_5_o() {
        return p333_5_o;
    }

    public void setP333_5_o(String p333_5_o) {
        this.p333_5_o = p333_5_o;
    }

    public String getP334() {
        return p334;
    }

    public void setP334(String p334) {
        this.p334 = p334;
    }

    public String getP335() {
        return p335;
    }

    public void setP335(String p335) {
        this.p335 = p335;
    }

    public String getP336_1() {
        return p336_1;
    }

    public void setP336_1(String p336_1) {
        this.p336_1 = p336_1;
    }

    public String getP337_1() {
        return p337_1;
    }

    public void setP337_1(String p337_1) {
        this.p337_1 = p337_1;
    }

    public String getP336_2() {
        return p336_2;
    }

    public void setP336_2(String p336_2) {
        this.p336_2 = p336_2;
    }

    public String getP337_2() {
        return p337_2;
    }

    public void setP337_2(String p337_2) {
        this.p337_2 = p337_2;
    }

    public String getP336_3() {
        return p336_3;
    }

    public void setP336_3(String p336_3) {
        this.p336_3 = p336_3;
    }

    public String getP337_3() {
        return p337_3;
    }

    public void setP337_3(String p337_3) {
        this.p337_3 = p337_3;
    }

    public String getP336_4() {
        return p336_4;
    }

    public void setP336_4(String p336_4) {
        this.p336_4 = p336_4;
    }

    public String getP337_4() {
        return p337_4;
    }

    public void setP337_4(String p337_4) {
        this.p337_4 = p337_4;
    }

    public String getP336_5() {
        return p336_5;
    }

    public void setP336_5(String p336_5) {
        this.p336_5 = p336_5;
    }

    public String getP337_5() {
        return p337_5;
    }

    public void setP337_5(String p337_5) {
        this.p337_5 = p337_5;
    }

    public String getP336_6() {
        return p336_6;
    }

    public void setP336_6(String p336_6) {
        this.p336_6 = p336_6;
    }

    public String getP337_6() {
        return p337_6;
    }

    public void setP337_6(String p337_6) {
        this.p337_6 = p337_6;
    }

    public String getP336_7() {
        return p336_7;
    }

    public void setP336_7(String p336_7) {
        this.p336_7 = p336_7;
    }

    public String getP336_7_o() {
        return p336_7_o;
    }

    public void setP336_7_o(String p336_7_o) {
        this.p336_7_o = p336_7_o;
    }

    public String getP337_7() {
        return p337_7;
    }

    public void setP337_7(String p337_7) {
        this.p337_7 = p337_7;
    }

    public String getP336_8() {
        return p336_8;
    }

    public void setP336_8(String p336_8) {
        this.p336_8 = p336_8;
    }

    public String getP336_8_o() {
        return p336_8_o;
    }

    public void setP336_8_o(String p336_8_o) {
        this.p336_8_o = p336_8_o;
    }

    public String getP337_8() {
        return p337_8;
    }

    public void setP337_8(String p337_8) {
        this.p337_8 = p337_8;
    }

    public String getObs_f() {
        return obs_f;
    }

    public void setObs_f(String obs_f) {
        this.obs_f = obs_f;
    }


    public ContentValues toValues(){
        ContentValues contentValues = new ContentValues();

        return contentValues;
    }



}
