package com.example.ricindigus.trabajo2020.fragments.modulo8;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo8;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InputFilterSoloLetras;
import com.example.ricindigus.trabajo2020.util.NumericKeyBoardTransformationMethod;

import java.util.ArrayList;
import java.util.List;


public class FragmentP044P046 extends FragmentPagina {
    String idEncuestado;
    Context context;
    String idInformante;
    String idHogar;
    String idVivienda;

    LinearLayout layout_h_p044_p045;
    LinearLayout layout_h_p046;

    RadioGroup  rg_h_p044_1;
    RadioGroup  rg_h_p044_2;
    RadioGroup  rg_h_p044_3;
    RadioGroup  rg_h_p044_4;
    EditText    et_h_p044_4_o;
    CheckBox cb_h_p045_1_1;
    CheckBox   cb_h_p045_1_2;
    CheckBox   cb_h_p045_1_3;
    CheckBox   cb_h_p045_1_4;
    CheckBox   cb_h_p045_1_5;
    CheckBox   cb_h_p045_1_6;
    CheckBox   cb_h_p045_1_7;
    CheckBox   cb_h_p045_1_8;
    CheckBox   cb_h_p045_1_9;
    EditText    et_h_p045_1_8_o;
    CheckBox   cb_h_p045_2_1;
    CheckBox   cb_h_p045_2_2;
    CheckBox   cb_h_p045_2_3;
    CheckBox   cb_h_p045_2_4;
    CheckBox   cb_h_p045_2_5;
    CheckBox   cb_h_p045_2_6;
    CheckBox   cb_h_p045_2_7;
    CheckBox   cb_h_p045_2_8;
    CheckBox   cb_h_p045_2_9;
    EditText    et_h_p045_2_8_o;
    CheckBox   cb_h_p045_3_1;
    CheckBox   cb_h_p045_3_2;
    CheckBox   cb_h_p045_3_3;
    CheckBox   cb_h_p045_3_4;
    CheckBox   cb_h_p045_3_5;
    CheckBox   cb_h_p045_3_6;
    CheckBox   cb_h_p045_3_7;
    CheckBox   cb_h_p045_3_8;
    CheckBox   cb_h_p045_3_9;
    EditText    et_h_p045_3_8_o;
    CheckBox   cb_h_p045_4_1;
    CheckBox   cb_h_p045_4_2;
    CheckBox   cb_h_p045_4_3;
    CheckBox   cb_h_p045_4_4;
    CheckBox   cb_h_p045_4_5;
    CheckBox   cb_h_p045_4_6;
    CheckBox   cb_h_p045_4_7;
    CheckBox   cb_h_p045_4_8;
    CheckBox   cb_h_p045_4_9;
    EditText    et_h_p045_4_8_o;
    RadioGroup   rg_h_p046_1;
    RadioGroup   rg_h_p046_2;
    RadioGroup   rg_h_p046_3;
    RadioGroup   rg_h_p046_4;
    RadioGroup   rg_h_p046_5;
    RadioGroup   rg_h_p046_6;
    EditText    et_h_p046_6_o;

    String p044_1;
    String p045_1_1;
    String p045_1_2;
    String p045_1_3;
    String p045_1_4;
    String p045_1_5;
    String p045_1_6;
    String p045_1_7;
    String p045_1_8;
    String p045_1_8_o;
    String p045_1_9;
    String p044_2;
    String p045_2_1;
    String p045_2_2;
    String p045_2_3;
    String p045_2_4;
    String p045_2_5;
    String p045_2_6;
    String p045_2_7;
    String p045_2_8;
    String p045_2_8_o;
    String p045_2_9;
    String p044_3;
    String p045_3_1;
    String p045_3_2;
    String p045_3_3;
    String p045_3_4;
    String p045_3_5;
    String p045_3_6;
    String p045_3_7;
    String p045_3_8;
    String p045_3_8_o;
    String p045_3_9;
    String p044_4;
    String p044_4_o;
    String p045_4_1;
    String p045_4_2;
    String p045_4_3;
    String p045_4_4;
    String p045_4_5;
    String p045_4_6;
    String p045_4_7;
    String p045_4_8;
    String p045_4_8_o;
    String p045_4_9;
    String p046_1;
    String p046_2;
    String p046_3;
    String p046_4;
    String p046_5;
    String p046_6;
    String p046_6_o;

    List<CheckBox> listaCheck1 = new ArrayList<>();
    List<CheckBox> listaCheck2= new ArrayList<>();
    List<CheckBox> listaCheck3= new ArrayList<>();
    List<CheckBox> listaCheck4= new ArrayList<>();

    public FragmentP044P046() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentP044P046(String idEncuestado, Context context) {
        this.idEncuestado = idEncuestado;
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_h_p044_p046, container, false);
            rg_h_p044_1 = rootView.findViewById(R.id.rg_h_P044_1);
            rg_h_p044_2 = rootView.findViewById(R.id.rg_h_P044_2);
            rg_h_p044_3 = rootView.findViewById(R.id.rg_h_P044_3);
            rg_h_p044_4 = rootView.findViewById(R.id.rg_h_P044_4);
            et_h_p044_4_o = rootView.findViewById(R.id.et_h_p044_4_O);
            cb_h_p045_1_1 = rootView.findViewById(R.id.cb_h_P045_1_1);
            cb_h_p045_1_2 = rootView.findViewById(R.id.cb_h_P045_1_2);
            cb_h_p045_1_3 = rootView.findViewById(R.id.cb_h_P045_1_3);
            cb_h_p045_1_4 = rootView.findViewById(R.id.cb_h_P045_1_4);
            cb_h_p045_1_5 = rootView.findViewById(R.id.cb_h_P045_1_5);
            cb_h_p045_1_6 = rootView.findViewById(R.id.cb_h_P045_1_6);
            cb_h_p045_1_7 = rootView.findViewById(R.id.cb_h_P045_1_7);
            cb_h_p045_1_8 = rootView.findViewById(R.id.cb_h_P045_1_8);
            cb_h_p045_1_9 = rootView.findViewById(R.id.cb_h_P045_1_9);
            et_h_p045_1_8_o = rootView.findViewById(R.id.et_h_p045_1_8_O);
            cb_h_p045_2_1 = rootView.findViewById(R.id.cb_h_P045_2_1);
            cb_h_p045_2_2 = rootView.findViewById(R.id.cb_h_P045_2_2);
            cb_h_p045_2_3 = rootView.findViewById(R.id.cb_h_P045_2_3);
            cb_h_p045_2_4 = rootView.findViewById(R.id.cb_h_P045_2_4);
            cb_h_p045_2_5 = rootView.findViewById(R.id.cb_h_P045_2_5);
            cb_h_p045_2_6 = rootView.findViewById(R.id.cb_h_P045_2_6);
            cb_h_p045_2_7 = rootView.findViewById(R.id.cb_h_P045_2_7);
            cb_h_p045_2_8 = rootView.findViewById(R.id.cb_h_P045_2_8);
            cb_h_p045_2_9 = rootView.findViewById(R.id.cb_h_P045_2_9);
            et_h_p045_2_8_o = rootView.findViewById(R.id.et_h_p045_2_8_O);
            cb_h_p045_3_1 = rootView.findViewById(R.id.cb_h_P045_3_1);
            cb_h_p045_3_2 = rootView.findViewById(R.id.cb_h_P045_3_2);
            cb_h_p045_3_3 = rootView.findViewById(R.id.cb_h_P045_3_3);
            cb_h_p045_3_4 = rootView.findViewById(R.id.cb_h_P045_3_4);
            cb_h_p045_3_5 = rootView.findViewById(R.id.cb_h_P045_3_5);
            cb_h_p045_3_6 = rootView.findViewById(R.id.cb_h_P045_3_6);
            cb_h_p045_3_7 = rootView.findViewById(R.id.cb_h_P045_3_7);
            cb_h_p045_3_8 = rootView.findViewById(R.id.cb_h_P045_3_8);
            cb_h_p045_3_9 = rootView.findViewById(R.id.cb_h_P045_3_9);
            et_h_p045_3_8_o = rootView.findViewById(R.id.et_h_p045_3_8_O);
            cb_h_p045_4_1 = rootView.findViewById(R.id.cb_h_P045_4_1);
            cb_h_p045_4_2 = rootView.findViewById(R.id.cb_h_P045_4_2);
            cb_h_p045_4_3 = rootView.findViewById(R.id.cb_h_P045_4_3);
            cb_h_p045_4_4 = rootView.findViewById(R.id.cb_h_P045_4_4);
            cb_h_p045_4_5 = rootView.findViewById(R.id.cb_h_P045_4_5);
            cb_h_p045_4_6 = rootView.findViewById(R.id.cb_h_P045_4_6);
            cb_h_p045_4_7 = rootView.findViewById(R.id.cb_h_P045_4_7);
            cb_h_p045_4_8 = rootView.findViewById(R.id.cb_h_P045_4_8);
            cb_h_p045_4_9 = rootView.findViewById(R.id.cb_h_P045_4_9);
            et_h_p045_4_8_o = rootView.findViewById(R.id.et_h_p045_4_8_O);
            rg_h_p046_1 = rootView.findViewById(R.id.rg_h_p046_1);
            rg_h_p046_2 = rootView.findViewById(R.id.rg_h_p046_2);
            rg_h_p046_3 = rootView.findViewById(R.id.rg_h_p046_3);
            rg_h_p046_4 = rootView.findViewById(R.id.rg_h_p046_4);
            rg_h_p046_5 = rootView.findViewById(R.id.rg_h_p046_5);
            rg_h_p046_6 = rootView.findViewById(R.id.rg_h_p046_6);
            et_h_p046_6_o = rootView.findViewById(R.id.et_h_p046_6_o);

            listaCheck1.add(cb_h_p045_1_1);
            listaCheck1.add(cb_h_p045_1_2);
            listaCheck1.add(cb_h_p045_1_3);
            listaCheck1.add(cb_h_p045_1_4);
            listaCheck1.add(cb_h_p045_1_5);
            listaCheck1.add(cb_h_p045_1_6);
            listaCheck1.add(cb_h_p045_1_7);
            listaCheck1.add(cb_h_p045_1_8);
            listaCheck1.add(cb_h_p045_1_9);
            listaCheck2.add(cb_h_p045_2_1);
            listaCheck2.add(cb_h_p045_2_2);
            listaCheck2.add(cb_h_p045_2_3);
            listaCheck2.add(cb_h_p045_2_4);
            listaCheck2.add(cb_h_p045_2_5);
            listaCheck2.add(cb_h_p045_2_6);
            listaCheck2.add(cb_h_p045_2_7);
            listaCheck2.add(cb_h_p045_2_8);
            listaCheck2.add(cb_h_p045_2_9);
            listaCheck3.add(cb_h_p045_3_1);
            listaCheck3.add(cb_h_p045_3_2);
            listaCheck3.add(cb_h_p045_3_3);
            listaCheck3.add(cb_h_p045_3_4);
            listaCheck3.add(cb_h_p045_3_5);
            listaCheck3.add(cb_h_p045_3_6);
            listaCheck3.add(cb_h_p045_3_7);
            listaCheck3.add(cb_h_p045_3_8);
            listaCheck3.add(cb_h_p045_3_9);
            listaCheck4.add(cb_h_p045_4_1);
            listaCheck4.add(cb_h_p045_4_2);
            listaCheck4.add(cb_h_p045_4_3);
            listaCheck4.add(cb_h_p045_4_4);
            listaCheck4.add(cb_h_p045_4_5);
            listaCheck4.add(cb_h_p045_4_6);
            listaCheck4.add(cb_h_p045_4_7);
            listaCheck4.add(cb_h_p045_4_8);
            listaCheck4.add(cb_h_p045_4_9);


            
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        controlarChecked(cb_h_p045_1_8,et_h_p045_1_8_o);
        controlarChecked(cb_h_p045_2_8,et_h_p045_2_8_o);
        controlarChecked(cb_h_p045_3_8,et_h_p045_3_8_o);
        controlarChecked(cb_h_p045_4_8,et_h_p045_4_8_o);
        rg_h_p044_4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                controlarEspecifiqueRadio(radioGroup,i,5,et_h_p044_4_o);
            }
        });
        rg_h_p044_4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                controlarEspecifiqueRadio(radioGroup,i,1,et_h_p044_4_o);
            }
        });
        rg_h_p046_6.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                controlarEspecifiqueRadio(radioGroup,i,1,et_h_p046_6_o);
            }
        });

        llenarVista();
        cargarDatos();
    }



    @Override
    public void guardarDatos() {
        Data data = new Data(context);
        data.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo8_id_informante,idInformante);
        contentValues.put(SQLConstantes.seccion_h_p344_1,p044_1);
        contentValues.put(SQLConstantes.seccion_h_p345_1_1,p045_1_1);
        contentValues.put(SQLConstantes.seccion_h_p345_1_2,p045_1_2);
        contentValues.put(SQLConstantes.seccion_h_p345_1_3,p045_1_3);
        contentValues.put(SQLConstantes.seccion_h_p345_1_4,p045_1_4);
        contentValues.put(SQLConstantes.seccion_h_p345_1_5,p045_1_5);
        contentValues.put(SQLConstantes.seccion_h_p345_1_6,p045_1_6);
        contentValues.put(SQLConstantes.seccion_h_p345_1_7,p045_1_7);
        contentValues.put(SQLConstantes.seccion_h_p345_1_8,p045_1_8);
        contentValues.put(SQLConstantes.seccion_h_p345_1_8_o,p045_1_8_o);
        contentValues.put(SQLConstantes.seccion_h_p345_1_9,p045_1_9);
        contentValues.put(SQLConstantes.seccion_h_p344_2,p044_2);
        contentValues.put(SQLConstantes.seccion_h_p345_2_1,p045_2_1);
        contentValues.put(SQLConstantes.seccion_h_p345_2_2,p045_2_2);
        contentValues.put(SQLConstantes.seccion_h_p345_2_3,p045_2_3);
        contentValues.put(SQLConstantes.seccion_h_p345_2_4,p045_2_4);
        contentValues.put(SQLConstantes.seccion_h_p345_2_5,p045_2_5);
        contentValues.put(SQLConstantes.seccion_h_p345_2_6,p045_2_6);
        contentValues.put(SQLConstantes.seccion_h_p345_2_7,p045_2_7);
        contentValues.put(SQLConstantes.seccion_h_p345_2_8,p045_2_8);
        contentValues.put(SQLConstantes.seccion_h_p345_2_8_o,p045_2_8_o);
        contentValues.put(SQLConstantes.seccion_h_p345_2_9,p045_2_9);
        contentValues.put(SQLConstantes.seccion_h_p344_3,p044_3);
        contentValues.put(SQLConstantes.seccion_h_p345_3_1,p045_3_1);
        contentValues.put(SQLConstantes.seccion_h_p345_3_2,p045_3_2);
        contentValues.put(SQLConstantes.seccion_h_p345_3_3,p045_3_3);
        contentValues.put(SQLConstantes.seccion_h_p345_3_4,p045_3_4);
        contentValues.put(SQLConstantes.seccion_h_p345_3_5,p045_3_5);
        contentValues.put(SQLConstantes.seccion_h_p345_3_6,p045_3_6);
        contentValues.put(SQLConstantes.seccion_h_p345_3_7,p045_3_7);
        contentValues.put(SQLConstantes.seccion_h_p345_3_8,p045_3_8);
        contentValues.put(SQLConstantes.seccion_h_p345_3_8_o,p045_3_8_o);
        contentValues.put(SQLConstantes.seccion_h_p345_3_9,p045_3_9);
        contentValues.put(SQLConstantes.seccion_h_p344_4,p044_4);
        contentValues.put(SQLConstantes.seccion_h_p344_4_o,p044_4_o);
        contentValues.put(SQLConstantes.seccion_h_p345_4_1,p045_4_1);
        contentValues.put(SQLConstantes.seccion_h_p345_4_2,p045_4_2);
        contentValues.put(SQLConstantes.seccion_h_p345_4_3,p045_4_3);
        contentValues.put(SQLConstantes.seccion_h_p345_4_4,p045_4_4);
        contentValues.put(SQLConstantes.seccion_h_p345_4_5,p045_4_5);
        contentValues.put(SQLConstantes.seccion_h_p345_4_6,p045_4_6);
        contentValues.put(SQLConstantes.seccion_h_p345_4_7,p045_4_7);
        contentValues.put(SQLConstantes.seccion_h_p345_4_8,p045_4_8);
        contentValues.put(SQLConstantes.seccion_h_p345_4_8_o,p045_4_8_o);
        contentValues.put(SQLConstantes.seccion_h_p345_4_9,p045_4_9);
        contentValues.put(SQLConstantes.seccion_h_p346_1,p046_1);
        contentValues.put(SQLConstantes.seccion_h_p346_2,p046_2);
        contentValues.put(SQLConstantes.seccion_h_p346_3,p046_3);
        contentValues.put(SQLConstantes.seccion_h_p346_4,p046_4);
        contentValues.put(SQLConstantes.seccion_h_p346_5,p046_5);
        contentValues.put(SQLConstantes.seccion_h_p346_6,p046_6);
        contentValues.put(SQLConstantes.seccion_h_p346_6_o,p046_6_o);

        if(!data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo8 modulo8 = new Modulo8();
            modulo8.set_id(idEncuestado);
            modulo8.setIdHogar(idHogar);
            modulo8.setIdVivienda(idVivienda);
            data.insertarElemento(getNombreTabla(),modulo8.toValues());
        }
        data.actualizarElemento(getNombreTabla(),contentValues,idEncuestado);
        data.close();
    }

    @Override
    public void llenarVariables() {
        p044_1    = rg_h_p044_1.indexOfChild(rg_h_p044_1.findViewById(rg_h_p044_1.getCheckedRadioButtonId()))+"";
        p044_2    = rg_h_p044_2.indexOfChild(rg_h_p044_2.findViewById(rg_h_p044_2.getCheckedRadioButtonId()))+"";
        p044_3    = rg_h_p044_3.indexOfChild(rg_h_p044_3.findViewById(rg_h_p044_3.getCheckedRadioButtonId()))+"";
        p044_4    = rg_h_p044_4.indexOfChild(rg_h_p044_4.findViewById(rg_h_p044_4.getCheckedRadioButtonId()))+"";
        if (cb_h_p045_1_1.isChecked()) p045_1_1= "1"; else p045_1_1="0";
        if (cb_h_p045_1_2.isChecked()) p045_1_2= "1"; else p045_1_2="0";
        if (cb_h_p045_1_3.isChecked()) p045_1_3= "1"; else p045_1_3="0";
        if (cb_h_p045_1_4.isChecked()) p045_1_4= "1"; else p045_1_4="0";
        if (cb_h_p045_1_5.isChecked()) p045_1_5= "1"; else p045_1_5="0";
        if (cb_h_p045_1_6.isChecked()) p045_1_6= "1"; else p045_1_6="0";
        if (cb_h_p045_1_7.isChecked()) p045_1_7= "1"; else p045_1_7="0";
        if (cb_h_p045_1_8.isChecked()) p045_1_8= "1"; else p045_1_8="0";
        if (cb_h_p045_1_9.isChecked()) p045_1_9= "1"; else p045_1_9="0";
        if (cb_h_p045_2_1.isChecked()) p045_2_1= "1"; else p045_2_1="0";
        if (cb_h_p045_2_2.isChecked()) p045_2_2= "1"; else p045_2_2="0";
        if (cb_h_p045_2_3.isChecked()) p045_2_3= "1"; else p045_2_3="0";
        if (cb_h_p045_2_4.isChecked()) p045_2_4= "1"; else p045_2_4="0";
        if (cb_h_p045_2_5.isChecked()) p045_2_5= "1"; else p045_2_5="0";
        if (cb_h_p045_2_6.isChecked()) p045_2_6= "1"; else p045_2_6="0";
        if (cb_h_p045_2_7.isChecked()) p045_2_7= "1"; else p045_2_7="0";
        if (cb_h_p045_2_8.isChecked()) p045_2_8= "1"; else p045_2_8="0";
        if (cb_h_p045_2_9.isChecked()) p045_2_9= "1"; else p045_2_9="0";
        if (cb_h_p045_3_1.isChecked()) p045_3_1= "1"; else p045_3_1="0";
        if (cb_h_p045_3_2.isChecked()) p045_3_2= "1"; else p045_3_2="0";
        if (cb_h_p045_3_3.isChecked()) p045_3_3= "1"; else p045_3_3="0";
        if (cb_h_p045_3_4.isChecked()) p045_3_4= "1"; else p045_3_4="0";
        if (cb_h_p045_3_5.isChecked()) p045_3_5= "1"; else p045_3_5="0";
        if (cb_h_p045_3_6.isChecked()) p045_3_6= "1"; else p045_3_6="0";
        if (cb_h_p045_3_7.isChecked()) p045_3_7= "1"; else p045_3_7="0";
        if (cb_h_p045_3_8.isChecked()) p045_3_8= "1"; else p045_3_8="0";
        if (cb_h_p045_3_9.isChecked()) p045_3_9= "1"; else p045_3_9="0";
        if (cb_h_p045_4_1.isChecked()) p045_4_1= "1"; else p045_4_1="0";
        if (cb_h_p045_4_2.isChecked()) p045_4_2= "1"; else p045_4_2="0";
        if (cb_h_p045_4_3.isChecked()) p045_4_3= "1"; else p045_4_3="0";
        if (cb_h_p045_4_4.isChecked()) p045_4_4= "1"; else p045_4_4="0";
        if (cb_h_p045_4_5.isChecked()) p045_4_5= "1"; else p045_4_5="0";
        if (cb_h_p045_4_6.isChecked()) p045_4_6= "1"; else p045_4_6="0";
        if (cb_h_p045_4_7.isChecked()) p045_4_7= "1"; else p045_4_7="0";
        if (cb_h_p045_4_8.isChecked()) p045_4_8= "1"; else p045_4_8="0";
        if (cb_h_p045_4_9.isChecked()) p045_4_9= "1"; else p045_4_9="0";
        p044_4_o = et_h_p044_4_o.getText().toString();
        p045_1_8_o = et_h_p045_1_8_o.getText().toString();
        p045_2_8_o = et_h_p045_2_8_o.getText().toString();
        p045_3_8_o = et_h_p045_3_8_o.getText().toString();
        p045_4_8_o = et_h_p045_4_8_o.getText().toString();
        p046_1    = rg_h_p046_1.indexOfChild(rg_h_p046_1.findViewById(rg_h_p046_1.getCheckedRadioButtonId()))+"";
        p046_2    = rg_h_p046_2.indexOfChild(rg_h_p046_2.findViewById(rg_h_p046_2.getCheckedRadioButtonId()))+"";
        p046_3    = rg_h_p046_3.indexOfChild(rg_h_p046_3.findViewById(rg_h_p046_3.getCheckedRadioButtonId()))+"";
        p046_4    = rg_h_p046_4.indexOfChild(rg_h_p046_4.findViewById(rg_h_p046_4.getCheckedRadioButtonId()))+"";
        p046_5    = rg_h_p046_5.indexOfChild(rg_h_p046_5.findViewById(rg_h_p046_5.getCheckedRadioButtonId()))+"";
        p046_6    = rg_h_p046_6.indexOfChild(rg_h_p046_6.findViewById(rg_h_p046_6.getCheckedRadioButtonId()))+"";
        p046_6_o  = et_h_p046_6_o.getText().toString();
    }

    @Override
    public void cargarDatos() {
        Data data = new Data(context);
        data.open();
        if (data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo8 modulo8 = data.getModulo8(idEncuestado);
            if(!modulo8.getP344_1().equals("-1") && !modulo8.getP344_1().equals(""))((RadioButton)rg_h_p044_1.getChildAt(Integer.parseInt(modulo8.getP344_1()))).setChecked(true);
            if(!modulo8.getP344_2().equals("-1") && !modulo8.getP344_2().equals(""))((RadioButton)rg_h_p044_2.getChildAt(Integer.parseInt(modulo8.getP344_2()))).setChecked(true);
            if(!modulo8.getP344_3().equals("-1") && !modulo8.getP344_3().equals(""))((RadioButton)rg_h_p044_3.getChildAt(Integer.parseInt(modulo8.getP344_3()))).setChecked(true);
            if(!modulo8.getP344_4().equals("-1") && !modulo8.getP344_4().equals(""))((RadioButton)rg_h_p044_4.getChildAt(Integer.parseInt(modulo8.getP344_4()))).setChecked(true);
            if (modulo8.getP345_1_1().equals("1")) cb_h_p045_1_1.setChecked(true);
            if (modulo8.getP345_1_2().equals("1")) cb_h_p045_1_2.setChecked(true);
            if (modulo8.getP345_1_3().equals("1")) cb_h_p045_1_3.setChecked(true);
            if (modulo8.getP345_1_4().equals("1")) cb_h_p045_1_4.setChecked(true);
            if (modulo8.getP345_1_5().equals("1")) cb_h_p045_1_5.setChecked(true);
            if (modulo8.getP345_1_6().equals("1")) cb_h_p045_1_6.setChecked(true);
            if (modulo8.getP345_1_7().equals("1")) cb_h_p045_1_7.setChecked(true);
            if (modulo8.getP345_1_8().equals("1")) cb_h_p045_1_8.setChecked(true);
            if (modulo8.getP345_1_9().equals("1")) cb_h_p045_1_9.setChecked(true);
            if (modulo8.getP345_2_1().equals("1")) cb_h_p045_2_1.setChecked(true);
            if (modulo8.getP345_2_2().equals("1")) cb_h_p045_2_2.setChecked(true);
            if (modulo8.getP345_2_3().equals("1")) cb_h_p045_2_3.setChecked(true);
            if (modulo8.getP345_2_4().equals("1")) cb_h_p045_2_4.setChecked(true);
            if (modulo8.getP345_2_5().equals("1")) cb_h_p045_2_5.setChecked(true);
            if (modulo8.getP345_2_6().equals("1")) cb_h_p045_2_6.setChecked(true);
            if (modulo8.getP345_2_7().equals("1")) cb_h_p045_2_7.setChecked(true);
            if (modulo8.getP345_2_8().equals("1")) cb_h_p045_2_8.setChecked(true);
            if (modulo8.getP345_2_9().equals("1")) cb_h_p045_2_9.setChecked(true);
            if (modulo8.getP345_3_1().equals("1")) cb_h_p045_3_1.setChecked(true);
            if (modulo8.getP345_3_2().equals("1")) cb_h_p045_3_2.setChecked(true);
            if (modulo8.getP345_3_3().equals("1")) cb_h_p045_3_3.setChecked(true);
            if (modulo8.getP345_3_4().equals("1")) cb_h_p045_3_4.setChecked(true);
            if (modulo8.getP345_3_5().equals("1")) cb_h_p045_3_5.setChecked(true);
            if (modulo8.getP345_3_6().equals("1")) cb_h_p045_3_6.setChecked(true);
            if (modulo8.getP345_3_7().equals("1")) cb_h_p045_3_7.setChecked(true);
            if (modulo8.getP345_3_8().equals("1")) cb_h_p045_3_8.setChecked(true);
            if (modulo8.getP345_3_9().equals("1")) cb_h_p045_3_9.setChecked(true);
            if (modulo8.getP345_4_1().equals("1")) cb_h_p045_4_1.setChecked(true);
            if (modulo8.getP345_4_2().equals("1")) cb_h_p045_4_2.setChecked(true);
            if (modulo8.getP345_4_3().equals("1")) cb_h_p045_4_3.setChecked(true);
            if (modulo8.getP345_4_4().equals("1")) cb_h_p045_4_4.setChecked(true);
            if (modulo8.getP345_4_5().equals("1")) cb_h_p045_4_5.setChecked(true);
            if (modulo8.getP345_4_6().equals("1")) cb_h_p045_4_6.setChecked(true);
            if (modulo8.getP345_4_7().equals("1")) cb_h_p045_4_7.setChecked(true);
            if (modulo8.getP345_4_8().equals("1")) cb_h_p045_4_8.setChecked(true);
            if (modulo8.getP345_4_9().equals("1")) cb_h_p045_4_9.setChecked(true);
            et_h_p045_1_8_o.setText(modulo8.getP345_1_8_o());
            et_h_p045_2_8_o.setText(modulo8.getP345_2_8_o());
            et_h_p045_3_8_o.setText(modulo8.getP345_3_8_o());
            et_h_p045_4_8_o.setText(modulo8.getP345_4_8_o());
            if(!modulo8.getP346_1().equals("-1") && !modulo8.getP346_1().equals(""))((RadioButton)rg_h_p046_1.getChildAt(Integer.parseInt(modulo8.getP346_1()))).setChecked(true);
            if(!modulo8.getP346_2().equals("-1") && !modulo8.getP346_2().equals(""))((RadioButton)rg_h_p046_2.getChildAt(Integer.parseInt(modulo8.getP346_2()))).setChecked(true);
            if(!modulo8.getP346_3().equals("-1") && !modulo8.getP346_3().equals(""))((RadioButton)rg_h_p046_3.getChildAt(Integer.parseInt(modulo8.getP346_3()))).setChecked(true);
            if(!modulo8.getP346_4().equals("-1") && !modulo8.getP346_4().equals(""))((RadioButton)rg_h_p046_4.getChildAt(Integer.parseInt(modulo8.getP346_4()))).setChecked(true);
            if(!modulo8.getP346_5().equals("-1") && !modulo8.getP346_5().equals(""))((RadioButton)rg_h_p046_5.getChildAt(Integer.parseInt(modulo8.getP346_5()))).setChecked(true);
            if(!modulo8.getP346_6().equals("-1") && !modulo8.getP346_6().equals(""))((RadioButton)rg_h_p046_6.getChildAt(Integer.parseInt(modulo8.getP346_6()))).setChecked(true);
            et_h_p046_6_o.setText(modulo8.getP346_6_o());
        }
        data.close();


    }

    @Override
    public void llenarVista() {

    }

    @Override
    public boolean validarDatos() {

          //p045

        llenarVariables();
        return true;
    }

    @Override
    public String getNombreTabla() {
        return SQLConstantes.tablamodulo8;
    }

    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void controlarChecked(CheckBox checkBox,final EditText editText) {
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    editText.setBackgroundResource(R.drawable.input_text_enabled);
                    editText.setEnabled(true);
                } else {
                    editText.setText("");
                    editText.setBackgroundResource(R.drawable.input_text_disabled);
                    editText.setEnabled(false);
                }
            }
        });
    }

    private void configurarEditText(final EditText editText, final View view, int tipo,int longitud){
        switch (tipo){
            case 0:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud), new InputFilterSoloLetras()});break;
            case 1:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud)});break;
            case 2:editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)});
                editText.setTransformationMethod(new NumericKeyBoardTransformationMethod());break;
        }

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ocultarTeclado(editText);
                    view.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void controlarEspecifiqueRadio(RadioGroup group, int checkedId, int opcionEsp, EditText editTextEspecifique) {
        int seleccionado = group.indexOfChild(group.findViewById(checkedId));
        if(seleccionado == opcionEsp){
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_enabled);
            editTextEspecifique.setEnabled(true);
        }else{
            editTextEspecifique.setText("");
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_disabled);
            editTextEspecifique.setEnabled(false);
        }
    }

    public void ocultarOtrosLayouts(){

    }

    public boolean verificarCoberturaCapitulo(){

        return true;
    }

    /*METODOS FLUJOS Y REGLAS*/
    public  void enabledLista(boolean estado, @Nullable List<CheckBox> lista, @Nullable CheckBox checkBox) {
        if (lista != null) {
            for (CheckBox check:lista) {
                if(check!=null){
                    if (check==checkBox){
                        check.setEnabled(!estado);
                    }else{
                        check.setEnabled(estado);
                    }
                }
            }
        }

    }

    public final void checketLista(boolean estado, @Nullable List<CheckBox> lista, @Nullable CheckBox checkBox) {
        if (lista != null) {
            for (CheckBox check:lista) {
                if(check!=null){
                    if (check==checkBox){
                        check.setChecked(!estado);
                    }else{
                        check.setChecked(estado);
                    }
                }
            }
        }

    }

    public final boolean validarListCheckBox(@Nullable List<CheckBox> lista) {
        boolean estado = true;
        if (lista != null) {
            for (CheckBox check:lista) {
                    if (check!=null && check.isChecked()){
                        estado = false;
                    }
            }
        }
        return estado;
    }

    public final boolean validarSeleccionListCheckbox(@Nullable List<CheckBox> lista) {
        boolean estado = true;
        if (lista != null) {
            for (CheckBox check:lista) {
                if (!check.isSelected()){
                    estado = false;
                }
            }
        }
        return estado;
    }


}
