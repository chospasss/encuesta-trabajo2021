package com.example.ricindigus.trabajo2020.modelo.pojos;

import android.content.ContentValues;

import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;

public class Modulo4 {
    private String _id;
    private String idInformante;
    private String idHogar;
    private String idVivienda;
    /*
    rivate String c4_p401_1;
    private String c4_p401_2;
    private String c4_p401_3;
    private String c4_p401_4;
    private String c4_p401_5;
    private String c4_p401_o;
    private String c4_p402;
    private String c4_p403_1;
    private String c4_p403_2;
    private String c4_p403_3;
    private String c4_p403_4;
    private String c4_p403_5;
    private String c4_p403_6;
    private String c4_p403_7;
    private String c4_p403_8;
    private String c4_p403_9;
    private String c4_p403_10;
    private String c4_p403_11;
    private String c4_p403_12;
    private String c4_p403_13;
    private String c4_p403_14;
    private String c4_p403_o;
    private String c4_p404;
    private String c4_p405_1;
    private String c4_p405_2;
    private String c4_p405_3;
    private String c4_p405_4;
    private String c4_p405_5;
    private String c4_p405_6;
    private String c4_p405_7;
    private String c4_p406_1;
    private String c4_p406_2;
    private String c4_p406_3;
    private String c4_p406_4;
    private String c4_p406_5;
    private String c4_p406_6;
    private String c4_p406_7;
    private String c4_p406_8;
    private String c4_p406_o;
    private String c4_p407_1;
    private String c4_p407_2;
    private String c4_p407_3;
    private String c4_p407_4;
    private String c4_p407_5;
    private String c4_p407_6;
    private String c4_p407_7;
    private String c4_p407_8;
    private String c4_p407_9;
    private String c4_p407_10;
    private String c4_p407_11;
    private String c4_p407_12;
    private String c4_p407_13;
    private String c4_p407_o;
    private String c4_p408_1;
    private String c4_p408_2;
    private String c4_p408_3;
    private String c4_p408_4;
    private String c4_p408_5;
    private String c4_p408_6;
    private String c4_p409;
    private String c4_p410;
    private String c4_p411_1;
    private String c4_p411_2;
    private String c4_p411_3;
    private String c4_p411_4;
    private String c4_p411_5;
    private String c4_p411_6;
    private String c4_p411_7;
    private String c4_p411_8;
    private String c4_p411_9;
    private String c4_p411_10;
    private String c4_p411_11;
    private String c4_p411_12;
    private String c4_p411_13;
    private String c4_p411_14;
    private String c4_p411_o;
    private String c4_p412;
    private String c4_p413;
    private String c4_p414;
    private String c4_p415;
    private String c4_p416_1;
    private String c4_p416_2;
    private String c4_p416_3;
    private String c4_p416_4;
    private String c4_p416_5;
    private String c4_p416_6;
    private String c4_p416_7;
    private String c4_p416_8;
    private String c4_p416_o;
    private String obs_cap4;
     */
    private String COB400;
    private String  inf_300;
    private String  p307;
    private String  p308a;
    private String  p308a_cod;
    private String  p308b;
    private String  p308b_cod;
    private String  p308c;
    private String  p308c_cod;
    private String  p308d;
    private String  p308d_cod;
    private String  p309;
    private String  p310;
    private String  p311;
    private String  p312_1;
    private String  p312_2;
    private String  p312_3;
    private String  p312_4;
    private String  p312_5;
    private String  p312_6;
    private String  p312_7;
    private String  p312_8;
    private String  p312_9;
    private String  p312_10;
    private String  p312_11;
    private String  p312_11_o;
    private String  p313;
    private String  p314;
    private String  p314a;
    private String  p314b;
    private String  obs_d;

    public Modulo4() {
        _id = "";
        idHogar = "";
        idVivienda = "";
        idInformante = "";
        /*
        c4_p401_1 = "";
        c4_p401_2 = "";
        c4_p401_3 = "";
        c4_p401_4 = "";
        c4_p401_5 = "";
        c4_p401_o = "";
        c4_p402 = "";
        c4_p403_1 = "";
        c4_p403_2 = "";
        c4_p403_3 = "";
        c4_p403_4 = "";
        c4_p403_5 = "";
        c4_p403_6 = "";
        c4_p403_7 = "";
        c4_p403_8 = "";
        c4_p403_9 = "";
        c4_p403_10 = "";
        c4_p403_11 = "";
        c4_p403_12 = "";
        c4_p403_13 = "";
        c4_p403_14 = "";
        c4_p403_o = "";
        c4_p404 = "";
        c4_p405_1 = "";
        c4_p405_2 = "";
        c4_p405_3 = "";
        c4_p405_4 = "";
        c4_p405_5 = "";
        c4_p405_6 = "";
        c4_p405_7 = "";
        c4_p406_1 = "";
        c4_p406_2 = "";
        c4_p406_3 = "";
        c4_p406_4 = "";
        c4_p406_5 = "";
        c4_p406_6 = "";
        c4_p406_7 = "";
        c4_p406_8 = "";
        c4_p406_o = "";
        c4_p407_1 = "";
        c4_p407_2 = "";
        c4_p407_3 = "";
        c4_p407_4 = "";
        c4_p407_5 = "";
        c4_p407_6 = "";
        c4_p407_7 = "";
        c4_p407_8 = "";
        c4_p407_9 = "";
        c4_p407_10 = "";
        c4_p407_11 = "";
        c4_p407_12 = "";
        c4_p407_13 = "";
        c4_p407_o = "";
        c4_p408_1 = "";
        c4_p408_2 = "";
        c4_p408_3 = "";
        c4_p408_4 = "";
        c4_p408_5 = "";
        c4_p408_6 = "";
        c4_p409 = "";
        c4_p410 = "";
        c4_p411_1 = "";
        c4_p411_2 = "";
        c4_p411_3 = "";
        c4_p411_4 = "";
        c4_p411_5 = "";
        c4_p411_6 = "";
        c4_p411_7 = "";
        c4_p411_8 = "";
        c4_p411_9 = "";
        c4_p411_10 = "";
        c4_p411_11 = "";
        c4_p411_12 = "";
        c4_p411_13 = "";
        c4_p411_14 = "";
        c4_p411_o = "";
        c4_p412 = "";
        c4_p413 = "";
        c4_p414 = "";
        c4_p415 = "";
        c4_p416_1 = "";
        c4_p416_2 = "";
        c4_p416_3 = "";
        c4_p416_4 = "";
        c4_p416_5 = "";
        c4_p416_6 = "";
        c4_p416_7 = "";
        c4_p416_8 = "";
        c4_p416_o = "";
        obs_cap4 = "";
         */
        COB400 = "0";
        inf_300= "";
        p307= "";
        p308a= "";
        p308a_cod= "";
        p308b= "";
        p308b_cod= "";
        p308c= "";
        p308c_cod= "";
        p308d= "";
        p308d_cod= "";
        p309= "";
        p310= "";
        p311= "";
        p312_1= "";
        p312_2= "";
        p312_3= "";
        p312_4= "";
        p312_5= "";
        p312_6= "";
        p312_7= "";
        p312_8= "";
        p312_9= "";
        p312_10= "";
        p312_11= "";
        p312_11_o= "";
        p313= "";
        p314= "";
        p314a= "";
        p314b= "";
        obs_d= "";

    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getIdInformante() {
        return idInformante;
    }

    public void setIdInformante(String idInformante) {
        this.idInformante = idInformante;
    }

    public String getIdHogar() {
        return idHogar;
    }

    public void setIdHogar(String idHogar) {
        this.idHogar = idHogar;
    }

    public String getIdVivienda() {
        return idVivienda;
    }

    public void setIdVivienda(String idVivienda) {
        this.idVivienda = idVivienda;
    }

    /*
    public String getC4_p401_1() {
        return c4_p401_1;
    }

    public void setC4_p401_1(String c4_p401_1) {
        this.c4_p401_1 = c4_p401_1;
    }

    public String getC4_p401_2() {
        return c4_p401_2;
    }

    public void setC4_p401_2(String c4_p401_2) {
        this.c4_p401_2 = c4_p401_2;
    }

    public String getC4_p401_3() {
        return c4_p401_3;
    }

    public void setC4_p401_3(String c4_p401_3) {
        this.c4_p401_3 = c4_p401_3;
    }

    public String getC4_p401_4() {
        return c4_p401_4;
    }

    public void setC4_p401_4(String c4_p401_4) {
        this.c4_p401_4 = c4_p401_4;
    }

    public String getC4_p401_5() {
        return c4_p401_5;
    }

    public void setC4_p401_5(String c4_p401_5) {
        this.c4_p401_5 = c4_p401_5;
    }

    public String getC4_p401_o() {
        return c4_p401_o;
    }

    public void setC4_p401_o(String c4_p401_o) {
        this.c4_p401_o = c4_p401_o;
    }

    public String getC4_p402() {
        return c4_p402;
    }

    public void setC4_p402(String c4_p402) {
        this.c4_p402 = c4_p402;
    }

    public String getC4_p403_1() {
        return c4_p403_1;
    }

    public void setC4_p403_1(String c4_p403_1) {
        this.c4_p403_1 = c4_p403_1;
    }

    public String getC4_p403_2() {
        return c4_p403_2;
    }

    public void setC4_p403_2(String c4_p403_2) {
        this.c4_p403_2 = c4_p403_2;
    }

    public String getC4_p403_3() {
        return c4_p403_3;
    }

    public void setC4_p403_3(String c4_p403_3) {
        this.c4_p403_3 = c4_p403_3;
    }

    public String getC4_p403_4() {
        return c4_p403_4;
    }

    public void setC4_p403_4(String c4_p403_4) {
        this.c4_p403_4 = c4_p403_4;
    }

    public String getC4_p403_5() {
        return c4_p403_5;
    }

    public void setC4_p403_5(String c4_p403_5) {
        this.c4_p403_5 = c4_p403_5;
    }

    public String getC4_p403_6() {
        return c4_p403_6;
    }

    public void setC4_p403_6(String c4_p403_6) {
        this.c4_p403_6 = c4_p403_6;
    }

    public String getC4_p403_7() {
        return c4_p403_7;
    }

    public void setC4_p403_7(String c4_p403_7) {
        this.c4_p403_7 = c4_p403_7;
    }

    public String getC4_p403_8() {
        return c4_p403_8;
    }

    public void setC4_p403_8(String c4_p403_8) {
        this.c4_p403_8 = c4_p403_8;
    }

    public String getC4_p403_9() {
        return c4_p403_9;
    }

    public void setC4_p403_9(String c4_p403_9) {
        this.c4_p403_9 = c4_p403_9;
    }

    public String getC4_p403_10() {
        return c4_p403_10;
    }

    public void setC4_p403_10(String c4_p403_10) {
        this.c4_p403_10 = c4_p403_10;
    }

    public String getC4_p403_11() {
        return c4_p403_11;
    }

    public void setC4_p403_11(String c4_p403_11) {
        this.c4_p403_11 = c4_p403_11;
    }

    public String getC4_p403_12() {
        return c4_p403_12;
    }

    public void setC4_p403_12(String c4_p403_12) {
        this.c4_p403_12 = c4_p403_12;
    }

    public String getC4_p403_13() {
        return c4_p403_13;
    }

    public void setC4_p403_13(String c4_p403_13) {
        this.c4_p403_13 = c4_p403_13;
    }

    public String getC4_p403_14() {
        return c4_p403_14;
    }

    public void setC4_p403_14(String c4_p403_14) {
        this.c4_p403_14 = c4_p403_14;
    }

    public String getC4_p403_o() {
        return c4_p403_o;
    }

    public void setC4_p403_o(String c4_p403_o) {
        this.c4_p403_o = c4_p403_o;
    }

    public String getC4_p404() {
        return c4_p404;
    }

    public void setC4_p404(String c4_p404) {
        this.c4_p404 = c4_p404;
    }

    public String getC4_p405_1() {
        return c4_p405_1;
    }

    public void setC4_p405_1(String c4_p405_1) {
        this.c4_p405_1 = c4_p405_1;
    }

    public String getC4_p405_2() {
        return c4_p405_2;
    }

    public void setC4_p405_2(String c4_p405_2) {
        this.c4_p405_2 = c4_p405_2;
    }

    public String getC4_p405_3() {
        return c4_p405_3;
    }

    public void setC4_p405_3(String c4_p405_3) {
        this.c4_p405_3 = c4_p405_3;
    }

    public String getC4_p405_4() {
        return c4_p405_4;
    }

    public void setC4_p405_4(String c4_p405_4) {
        this.c4_p405_4 = c4_p405_4;
    }

    public String getC4_p405_5() {
        return c4_p405_5;
    }

    public void setC4_p405_5(String c4_p405_5) {
        this.c4_p405_5 = c4_p405_5;
    }

    public String getC4_p405_6() {
        return c4_p405_6;
    }

    public void setC4_p405_6(String c4_p405_6) {
        this.c4_p405_6 = c4_p405_6;
    }

    public String getC4_p405_7() {
        return c4_p405_7;
    }

    public void setC4_p405_7(String c4_p405_7) {
        this.c4_p405_7 = c4_p405_7;
    }

    public String getC4_p406_1() {
        return c4_p406_1;
    }

    public void setC4_p406_1(String c4_p406_1) {
        this.c4_p406_1 = c4_p406_1;
    }

    public String getC4_p406_2() {
        return c4_p406_2;
    }

    public void setC4_p406_2(String c4_p406_2) {
        this.c4_p406_2 = c4_p406_2;
    }

    public String getC4_p406_3() {
        return c4_p406_3;
    }

    public void setC4_p406_3(String c4_p406_3) {
        this.c4_p406_3 = c4_p406_3;
    }

    public String getC4_p406_4() {
        return c4_p406_4;
    }

    public void setC4_p406_4(String c4_p406_4) {
        this.c4_p406_4 = c4_p406_4;
    }

    public String getC4_p406_5() {
        return c4_p406_5;
    }

    public void setC4_p406_5(String c4_p406_5) {
        this.c4_p406_5 = c4_p406_5;
    }

    public String getC4_p406_6() {
        return c4_p406_6;
    }

    public void setC4_p406_6(String c4_p406_6) {
        this.c4_p406_6 = c4_p406_6;
    }

    public String getC4_p406_7() {
        return c4_p406_7;
    }

    public void setC4_p406_7(String c4_p406_7) {
        this.c4_p406_7 = c4_p406_7;
    }

    public String getC4_p406_8() {
        return c4_p406_8;
    }

    public void setC4_p406_8(String c4_p406_8) {
        this.c4_p406_8 = c4_p406_8;
    }

    public String getC4_p406_o() {
        return c4_p406_o;
    }

    public void setC4_p406_o(String c4_p406_o) {
        this.c4_p406_o = c4_p406_o;
    }

    public String getC4_p407_1() {
        return c4_p407_1;
    }

    public void setC4_p407_1(String c4_p407_1) {
        this.c4_p407_1 = c4_p407_1;
    }

    public String getC4_p407_2() {
        return c4_p407_2;
    }

    public void setC4_p407_2(String c4_p407_2) {
        this.c4_p407_2 = c4_p407_2;
    }

    public String getC4_p407_3() {
        return c4_p407_3;
    }

    public void setC4_p407_3(String c4_p407_3) {
        this.c4_p407_3 = c4_p407_3;
    }

    public String getC4_p407_4() {
        return c4_p407_4;
    }

    public void setC4_p407_4(String c4_p407_4) {
        this.c4_p407_4 = c4_p407_4;
    }

    public String getC4_p407_5() {
        return c4_p407_5;
    }

    public void setC4_p407_5(String c4_p407_5) {
        this.c4_p407_5 = c4_p407_5;
    }

    public String getC4_p407_6() {
        return c4_p407_6;
    }

    public void setC4_p407_6(String c4_p407_6) {
        this.c4_p407_6 = c4_p407_6;
    }

    public String getC4_p407_7() {
        return c4_p407_7;
    }

    public void setC4_p407_7(String c4_p407_7) {
        this.c4_p407_7 = c4_p407_7;
    }

    public String getC4_p407_8() {
        return c4_p407_8;
    }

    public void setC4_p407_8(String c4_p407_8) {
        this.c4_p407_8 = c4_p407_8;
    }

    public String getC4_p407_9() {
        return c4_p407_9;
    }

    public void setC4_p407_9(String c4_p407_9) {
        this.c4_p407_9 = c4_p407_9;
    }

    public String getC4_p407_10() {
        return c4_p407_10;
    }

    public void setC4_p407_10(String c4_p407_10) {
        this.c4_p407_10 = c4_p407_10;
    }

    public String getC4_p407_11() {
        return c4_p407_11;
    }

    public void setC4_p407_11(String c4_p407_11) {
        this.c4_p407_11 = c4_p407_11;
    }

    public String getC4_p407_12() {
        return c4_p407_12;
    }

    public void setC4_p407_12(String c4_p407_12) {
        this.c4_p407_12 = c4_p407_12;
    }

    public String getC4_p407_13() {
        return c4_p407_13;
    }

    public void setC4_p407_13(String c4_p407_13) {
        this.c4_p407_13 = c4_p407_13;
    }

    public String getC4_p407_o() {
        return c4_p407_o;
    }

    public void setC4_p407_o(String c4_p407_o) {
        this.c4_p407_o = c4_p407_o;
    }

    public String getC4_p408_1() {
        return c4_p408_1;
    }

    public void setC4_p408_1(String c4_p408_1) {
        this.c4_p408_1 = c4_p408_1;
    }

    public String getC4_p408_2() {
        return c4_p408_2;
    }

    public void setC4_p408_2(String c4_p408_2) {
        this.c4_p408_2 = c4_p408_2;
    }

    public String getC4_p408_3() {
        return c4_p408_3;
    }

    public void setC4_p408_3(String c4_p408_3) {
        this.c4_p408_3 = c4_p408_3;
    }

    public String getC4_p408_4() {
        return c4_p408_4;
    }

    public void setC4_p408_4(String c4_p408_4) {
        this.c4_p408_4 = c4_p408_4;
    }

    public String getC4_p408_5() {
        return c4_p408_5;
    }

    public void setC4_p408_5(String c4_p408_5) {
        this.c4_p408_5 = c4_p408_5;
    }

    public String getC4_p408_6() {
        return c4_p408_6;
    }

    public void setC4_p408_6(String c4_p408_6) {
        this.c4_p408_6 = c4_p408_6;
    }

    public String getC4_p409() {
        return c4_p409;
    }

    public void setC4_p409(String c4_p409) {
        this.c4_p409 = c4_p409;
    }

    public String getC4_p410() {
        return c4_p410;
    }

    public void setC4_p410(String c4_p410) {
        this.c4_p410 = c4_p410;
    }

    public String getC4_p411_1() {
        return c4_p411_1;
    }

    public void setC4_p411_1(String c4_p411_1) {
        this.c4_p411_1 = c4_p411_1;
    }

    public String getC4_p411_2() {
        return c4_p411_2;
    }

    public void setC4_p411_2(String c4_p411_2) {
        this.c4_p411_2 = c4_p411_2;
    }

    public String getC4_p411_3() {
        return c4_p411_3;
    }

    public void setC4_p411_3(String c4_p411_3) {
        this.c4_p411_3 = c4_p411_3;
    }

    public String getC4_p411_4() {
        return c4_p411_4;
    }

    public void setC4_p411_4(String c4_p411_4) {
        this.c4_p411_4 = c4_p411_4;
    }

    public String getC4_p411_5() {
        return c4_p411_5;
    }

    public void setC4_p411_5(String c4_p411_5) {
        this.c4_p411_5 = c4_p411_5;
    }

    public String getC4_p411_6() {
        return c4_p411_6;
    }

    public void setC4_p411_6(String c4_p411_6) {
        this.c4_p411_6 = c4_p411_6;
    }

    public String getC4_p411_7() {
        return c4_p411_7;
    }

    public void setC4_p411_7(String c4_p411_7) {
        this.c4_p411_7 = c4_p411_7;
    }

    public String getC4_p411_8() {
        return c4_p411_8;
    }

    public void setC4_p411_8(String c4_p411_8) {
        this.c4_p411_8 = c4_p411_8;
    }

    public String getC4_p411_9() {
        return c4_p411_9;
    }

    public void setC4_p411_9(String c4_p411_9) {
        this.c4_p411_9 = c4_p411_9;
    }

    public String getC4_p411_10() {
        return c4_p411_10;
    }

    public void setC4_p411_10(String c4_p411_10) {
        this.c4_p411_10 = c4_p411_10;
    }

    public String getC4_p411_11() {
        return c4_p411_11;
    }

    public void setC4_p411_11(String c4_p411_11) {
        this.c4_p411_11 = c4_p411_11;
    }

    public String getC4_p411_12() {
        return c4_p411_12;
    }

    public void setC4_p411_12(String c4_p411_12) {
        this.c4_p411_12 = c4_p411_12;
    }

    public String getC4_p411_13() {
        return c4_p411_13;
    }

    public void setC4_p411_13(String c4_p411_13) {
        this.c4_p411_13 = c4_p411_13;
    }

    public String getC4_p411_14() {
        return c4_p411_14;
    }

    public void setC4_p411_14(String c4_p411_14) {
        this.c4_p411_14 = c4_p411_14;
    }

    public String getC4_p411_o() {
        return c4_p411_o;
    }

    public void setC4_p411_o(String c4_p411_o) {
        this.c4_p411_o = c4_p411_o;
    }

    public String getC4_p412() {
        return c4_p412;
    }

    public void setC4_p412(String c4_p412) {
        this.c4_p412 = c4_p412;
    }

    public String getC4_p413() {
        return c4_p413;
    }

    public void setC4_p413(String c4_p413) {
        this.c4_p413 = c4_p413;
    }

    public String getC4_p414() {
        return c4_p414;
    }

    public void setC4_p414(String c4_p414) {
        this.c4_p414 = c4_p414;
    }

    public String getC4_p415() {
        return c4_p415;
    }

    public void setC4_p415(String c4_p415) {
        this.c4_p415 = c4_p415;
    }

    public String getC4_p416_1() {
        return c4_p416_1;
    }

    public void setC4_p416_1(String c4_p416_1) {
        this.c4_p416_1 = c4_p416_1;
    }

    public String getC4_p416_2() {
        return c4_p416_2;
    }

    public void setC4_p416_2(String c4_p416_2) {
        this.c4_p416_2 = c4_p416_2;
    }

    public String getC4_p416_3() {
        return c4_p416_3;
    }

    public void setC4_p416_3(String c4_p416_3) {
        this.c4_p416_3 = c4_p416_3;
    }

    public String getC4_p416_4() {
        return c4_p416_4;
    }

    public void setC4_p416_4(String c4_p416_4) {
        this.c4_p416_4 = c4_p416_4;
    }

    public String getC4_p416_5() {
        return c4_p416_5;
    }

    public void setC4_p416_5(String c4_p416_5) {
        this.c4_p416_5 = c4_p416_5;
    }

    public String getC4_p416_6() {
        return c4_p416_6;
    }

    public void setC4_p416_6(String c4_p416_6) {
        this.c4_p416_6 = c4_p416_6;
    }

    public String getC4_p416_7() {
        return c4_p416_7;
    }

    public void setC4_p416_7(String c4_p416_7) {
        this.c4_p416_7 = c4_p416_7;
    }

    public String getC4_p416_8() {
        return c4_p416_8;
    }

    public void setC4_p416_8(String c4_p416_8) {
        this.c4_p416_8 = c4_p416_8;
    }

    public String getC4_p416_o() {
        return c4_p416_o;
    }

    public void setC4_p416_o(String c4_p416_o) {
        this.c4_p416_o = c4_p416_o;
    }

    public String getObs_cap4() {
        return obs_cap4;
    }

    public void setObs_cap4(String obs_cap4) {
        this.obs_cap4 = obs_cap4;
    }
     */


    public String getCOB400() {
        return COB400;
    }

    public void setCOB400(String COB400) {
        this.COB400 = COB400;
    }

    public String getInf_300() {
        return inf_300;
    }

    public void setInf_300(String inf_300) {
        this.inf_300 = inf_300;
    }

    public String getP307() {
        return p307;
    }

    public void setP307(String p307) {
        this.p307 = p307;
    }

    public String getP308a() {
        return p308a;
    }

    public void setP308a(String p308a) {
        this.p308a = p308a;
    }

    public String getP308a_cod() {
        return p308a_cod;
    }

    public void setP308a_cod(String p308a_cod) {
        this.p308a_cod = p308a_cod;
    }

    public String getP308b() {
        return p308b;
    }

    public void setP308b(String p308b) {
        this.p308b = p308b;
    }

    public String getP308b_cod() {
        return p308b_cod;
    }

    public void setP308b_cod(String p308b_cod) {
        this.p308b_cod = p308b_cod;
    }

    public String getP308c() {
        return p308c;
    }

    public void setP308c(String p308c) {
        this.p308c = p308c;
    }

    public String getP308c_cod() {
        return p308c_cod;
    }

    public void setP308c_cod(String p308c_cod) {
        this.p308c_cod = p308c_cod;
    }

    public String getP308d() {
        return p308d;
    }

    public void setP308d(String p308d) {
        this.p308d = p308d;
    }

    public String getP308d_cod() {
        return p308d_cod;
    }

    public void setP308d_cod(String p308d_cod) {
        this.p308d_cod = p308d_cod;
    }

    public String getP309() {
        return p309;
    }

    public void setP309(String p309) {
        this.p309 = p309;
    }

    public String getP310() {
        return p310;
    }

    public void setP310(String p310) {
        this.p310 = p310;
    }

    public String getP311() {
        return p311;
    }

    public void setP311(String p311) {
        this.p311 = p311;
    }

    public String getP312_1() {
        return p312_1;
    }

    public void setP312_1(String p312_1) {
        this.p312_1 = p312_1;
    }

    public String getP312_2() {
        return p312_2;
    }

    public void setP312_2(String p312_2) {
        this.p312_2 = p312_2;
    }

    public String getP312_3() {
        return p312_3;
    }

    public void setP312_3(String p312_3) {
        this.p312_3 = p312_3;
    }

    public String getP312_4() {
        return p312_4;
    }

    public void setP312_4(String p312_4) {
        this.p312_4 = p312_4;
    }

    public String getP312_5() {
        return p312_5;
    }

    public void setP312_5(String p312_5) {
        this.p312_5 = p312_5;
    }

    public String getP312_6() {
        return p312_6;
    }

    public void setP312_6(String p312_6) {
        this.p312_6 = p312_6;
    }

    public String getP312_7() {
        return p312_7;
    }

    public void setP312_7(String p312_7) {
        this.p312_7 = p312_7;
    }

    public String getP312_8() {
        return p312_8;
    }

    public void setP312_8(String p312_8) {
        this.p312_8 = p312_8;
    }

    public String getP312_9() {
        return p312_9;
    }

    public void setP312_9(String p312_9) {
        this.p312_9 = p312_9;
    }

    public String getP312_10() {
        return p312_10;
    }

    public void setP312_10(String p312_10) {
        this.p312_10 = p312_10;
    }

    public String getP312_11() {
        return p312_11;
    }

    public void setP312_11(String p312_11) {
        this.p312_11 = p312_11;
    }

    public String getP312_11_o() {
        return p312_11_o;
    }

    public void setP312_11_o(String p312_11_o) {
        this.p312_11_o = p312_11_o;
    }

    public String getP313() {
        return p313;
    }

    public void setP313(String p313) {
        this.p313 = p313;
    }

    public String getP314() {
        return p314;
    }

    public void setP314(String p314) {
        this.p314 = p314;
    }

    public String getP314a() {
        return p314a;
    }

    public void setP314a(String p314a) {
        this.p314a = p314a;
    }

    public String getP314b() {
        return p314b;
    }

    public void setP314b(String p314b) {
        this.p314b = p314b;
    }

    public String getObs_d() {
        return obs_d;
    }

    public void setObs_d(String obs_d) {
        this.obs_d = obs_d;
    }

    public ContentValues toValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo4_id,_id);
        contentValues.put(SQLConstantes.modulo4_id_informante,idInformante);
        contentValues.put(SQLConstantes.modulo4_id_hogar,idHogar);
        contentValues.put(SQLConstantes.modulo4_id_vivienda,idVivienda);
        /*
        contentValues.put(SQLConstantes.modulo4_c4_p401_1,c4_p401_1);
        contentValues.put(SQLConstantes.modulo4_c4_p401_2,c4_p401_2);
        contentValues.put(SQLConstantes.modulo4_c4_p401_3,c4_p401_3);
        contentValues.put(SQLConstantes.modulo4_c4_p401_4,c4_p401_4);
        contentValues.put(SQLConstantes.modulo4_c4_p401_5,c4_p401_5);
        contentValues.put(SQLConstantes.modulo4_c4_p401_o,c4_p401_o);
        contentValues.put(SQLConstantes.modulo4_c4_p402,c4_p402);
        contentValues.put(SQLConstantes.modulo4_c4_p403_1,c4_p403_1);
        contentValues.put(SQLConstantes.modulo4_c4_p403_2,c4_p403_2);
        contentValues.put(SQLConstantes.modulo4_c4_p403_3,c4_p403_3);
        contentValues.put(SQLConstantes.modulo4_c4_p403_4,c4_p403_4);
        contentValues.put(SQLConstantes.modulo4_c4_p403_5,c4_p403_5);
        contentValues.put(SQLConstantes.modulo4_c4_p403_6,c4_p403_6);
        contentValues.put(SQLConstantes.modulo4_c4_p403_7,c4_p403_7);
        contentValues.put(SQLConstantes.modulo4_c4_p403_8,c4_p403_8);
        contentValues.put(SQLConstantes.modulo4_c4_p403_9,c4_p403_9);
        contentValues.put(SQLConstantes.modulo4_c4_p403_10,c4_p403_10);
        contentValues.put(SQLConstantes.modulo4_c4_p403_11,c4_p403_11);
        contentValues.put(SQLConstantes.modulo4_c4_p403_12,c4_p403_12);
        contentValues.put(SQLConstantes.modulo4_c4_p403_13,c4_p403_13);
        contentValues.put(SQLConstantes.modulo4_c4_p403_14,c4_p403_14);
        contentValues.put(SQLConstantes.modulo4_c4_p403_o,c4_p403_o);
        contentValues.put(SQLConstantes.modulo4_c4_p404,c4_p404);
        contentValues.put(SQLConstantes.modulo4_c4_p405_1,c4_p405_1);
        contentValues.put(SQLConstantes.modulo4_c4_p405_2,c4_p405_2);
        contentValues.put(SQLConstantes.modulo4_c4_p405_3,c4_p405_3);
        contentValues.put(SQLConstantes.modulo4_c4_p405_4,c4_p405_4);
        contentValues.put(SQLConstantes.modulo4_c4_p405_5,c4_p405_5);
        contentValues.put(SQLConstantes.modulo4_c4_p405_6,c4_p405_6);
        contentValues.put(SQLConstantes.modulo4_c4_p405_7,c4_p405_7);
        contentValues.put(SQLConstantes.modulo4_c4_p406_1,c4_p406_1);
        contentValues.put(SQLConstantes.modulo4_c4_p406_2,c4_p406_2);
        contentValues.put(SQLConstantes.modulo4_c4_p406_3,c4_p406_3);
        contentValues.put(SQLConstantes.modulo4_c4_p406_4,c4_p406_4);
        contentValues.put(SQLConstantes.modulo4_c4_p406_5,c4_p406_5);
        contentValues.put(SQLConstantes.modulo4_c4_p406_6,c4_p406_6);
        contentValues.put(SQLConstantes.modulo4_c4_p406_7,c4_p406_7);
        contentValues.put(SQLConstantes.modulo4_c4_p406_8,c4_p406_8);
        contentValues.put(SQLConstantes.modulo4_c4_p406_o,c4_p406_o);
        contentValues.put(SQLConstantes.modulo4_c4_p407_1,c4_p407_1);
        contentValues.put(SQLConstantes.modulo4_c4_p407_2,c4_p407_2);
        contentValues.put(SQLConstantes.modulo4_c4_p407_3,c4_p407_3);
        contentValues.put(SQLConstantes.modulo4_c4_p407_4,c4_p407_4);
        contentValues.put(SQLConstantes.modulo4_c4_p407_5,c4_p407_5);
        contentValues.put(SQLConstantes.modulo4_c4_p407_6,c4_p407_6);
        contentValues.put(SQLConstantes.modulo4_c4_p407_7,c4_p407_7);
        contentValues.put(SQLConstantes.modulo4_c4_p407_8,c4_p407_8);
        contentValues.put(SQLConstantes.modulo4_c4_p407_9,c4_p407_9);
        contentValues.put(SQLConstantes.modulo4_c4_p407_10,c4_p407_10);
        contentValues.put(SQLConstantes.modulo4_c4_p407_11,c4_p407_11);
        contentValues.put(SQLConstantes.modulo4_c4_p407_12,c4_p407_12);
        contentValues.put(SQLConstantes.modulo4_c4_p407_13,c4_p407_13);
        contentValues.put(SQLConstantes.modulo4_c4_p407_o,c4_p407_o);
        contentValues.put(SQLConstantes.modulo4_c4_p408_1,c4_p408_1);
        contentValues.put(SQLConstantes.modulo4_c4_p408_2,c4_p408_2);
        contentValues.put(SQLConstantes.modulo4_c4_p408_3,c4_p408_3);
        contentValues.put(SQLConstantes.modulo4_c4_p408_4,c4_p408_4);
        contentValues.put(SQLConstantes.modulo4_c4_p408_5,c4_p408_5);
        contentValues.put(SQLConstantes.modulo4_c4_p408_6,c4_p408_6);
        contentValues.put(SQLConstantes.modulo4_c4_p409,c4_p409);
        contentValues.put(SQLConstantes.modulo4_c4_p410,c4_p410);
        contentValues.put(SQLConstantes.modulo4_c4_p411_1,c4_p411_1);
        contentValues.put(SQLConstantes.modulo4_c4_p411_2,c4_p411_2);
        contentValues.put(SQLConstantes.modulo4_c4_p411_3,c4_p411_3);
        contentValues.put(SQLConstantes.modulo4_c4_p411_4,c4_p411_4);
        contentValues.put(SQLConstantes.modulo4_c4_p411_5,c4_p411_5);
        contentValues.put(SQLConstantes.modulo4_c4_p411_6,c4_p411_6);
        contentValues.put(SQLConstantes.modulo4_c4_p411_7,c4_p411_7);
        contentValues.put(SQLConstantes.modulo4_c4_p411_8,c4_p411_8);
        contentValues.put(SQLConstantes.modulo4_c4_p411_9,c4_p411_9);
        contentValues.put(SQLConstantes.modulo4_c4_p411_10,c4_p411_10);
        contentValues.put(SQLConstantes.modulo4_c4_p411_11,c4_p411_11);
        contentValues.put(SQLConstantes.modulo4_c4_p411_12,c4_p411_12);
        contentValues.put(SQLConstantes.modulo4_c4_p411_13,c4_p411_13);
        contentValues.put(SQLConstantes.modulo4_c4_p411_14,c4_p411_14);
        contentValues.put(SQLConstantes.modulo4_c4_p411_o,c4_p411_o);
        contentValues.put(SQLConstantes.modulo4_c4_p412,c4_p412);
        contentValues.put(SQLConstantes.modulo4_c4_p413,c4_p413);
        contentValues.put(SQLConstantes.modulo4_c4_p414,c4_p414);
        contentValues.put(SQLConstantes.modulo4_c4_p415,c4_p415);
        contentValues.put(SQLConstantes.modulo4_c4_p416_1,c4_p416_1);
        contentValues.put(SQLConstantes.modulo4_c4_p416_2,c4_p416_2);
        contentValues.put(SQLConstantes.modulo4_c4_p416_3,c4_p416_3);
        contentValues.put(SQLConstantes.modulo4_c4_p416_4,c4_p416_4);
        contentValues.put(SQLConstantes.modulo4_c4_p416_5,c4_p416_5);
        contentValues.put(SQLConstantes.modulo4_c4_p416_6,c4_p416_6);
        contentValues.put(SQLConstantes.modulo4_c4_p416_7,c4_p416_7);
        contentValues.put(SQLConstantes.modulo4_c4_p416_8,c4_p416_8);
        contentValues.put(SQLConstantes.modulo4_c4_p416_o,c4_p416_o);
        contentValues.put(SQLConstantes.modulo4_obs_cap4,obs_cap4);
         */
        contentValues.put(SQLConstantes.modulo4_COB400,COB400);
        contentValues.put(SQLConstantes.seccion_d_inf_300,inf_300);
        contentValues.put(SQLConstantes.seccion_d_p307,p307);
        contentValues.put(SQLConstantes.seccion_d_p308a,p308a);
        contentValues.put(SQLConstantes.seccion_d_p308a_cod,p308a_cod);
        contentValues.put(SQLConstantes.seccion_d_p308b,p308b);
        contentValues.put(SQLConstantes.seccion_d_p308b_cod,p308b_cod);
        contentValues.put(SQLConstantes.seccion_d_p308c,p308c);
        contentValues.put(SQLConstantes.seccion_d_p308c_cod,p308c_cod);
        contentValues.put(SQLConstantes.seccion_d_p308d,p308d);
        contentValues.put(SQLConstantes.seccion_d_p308d_cod,p308d_cod);
        contentValues.put(SQLConstantes.seccion_d_p309,p309);
        contentValues.put(SQLConstantes.seccion_d_p310,p310);
        contentValues.put(SQLConstantes.seccion_d_p311,p311);
        contentValues.put(SQLConstantes.seccion_d_p312_1,p312_1);
        contentValues.put(SQLConstantes.seccion_d_p312_2,p312_2);
        contentValues.put(SQLConstantes.seccion_d_p312_3,p312_3);
        contentValues.put(SQLConstantes.seccion_d_p312_4,p312_4);
        contentValues.put(SQLConstantes.seccion_d_p312_5,p312_5);
        contentValues.put(SQLConstantes.seccion_d_p312_6,p312_6);
        contentValues.put(SQLConstantes.seccion_d_p312_7,p312_7);
        contentValues.put(SQLConstantes.seccion_d_p312_8,p312_8);
        contentValues.put(SQLConstantes.seccion_d_p312_9,p312_9);
        contentValues.put(SQLConstantes.seccion_d_p312_10,p312_10);
        contentValues.put(SQLConstantes.seccion_d_p312_11,p312_11);
        contentValues.put(SQLConstantes.seccion_d_p312_11_o,p312_11_o);
        contentValues.put(SQLConstantes.seccion_d_p313,p313);
        contentValues.put(SQLConstantes.seccion_d_p314,p314);
        contentValues.put(SQLConstantes.seccion_d_p314a,p314a);
        contentValues.put(SQLConstantes.seccion_d_p314b,p314b);
        contentValues.put(SQLConstantes.seccion_d_obs_d,obs_d);
        return contentValues;
    }
}
