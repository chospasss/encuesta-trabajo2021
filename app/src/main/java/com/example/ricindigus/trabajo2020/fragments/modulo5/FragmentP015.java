package com.example.ricindigus.trabajo2020.fragments.modulo5;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo5;
import com.example.ricindigus.trabajo2020.modelo.pojos.Residente;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InputFilterSoloLetras;
import com.example.ricindigus.trabajo2020.util.NumericKeyBoardTransformationMethod;


public class FragmentP015 extends FragmentPagina {
    String idEncuestado;
    Context context;
    String idInformante;
    String idHogar;
    String idVivienda;

    LinearLayout layout_e_p015;

    RadioGroup rg_e_P015_1;
    RadioGroup rg_e_P015_2;
    RadioGroup rg_e_P015_3;
    RadioGroup rg_e_P015_4;
    RadioGroup rg_e_P015_5;
    RadioGroup rg_e_P015_6;
    RadioGroup rg_e_P015_7;
    RadioGroup rg_e_P015_8;
    RadioGroup rg_e_P015_9;
    RadioGroup rg_e_P015_10;
    RadioGroup rg_e_P015_11;
    EditText   et_e_P015_11_o;
    
     String p015_1;
     String p015_2;
     String p015_3;
     String p015_4;
     String p015_5;
     String p015_6;
     String p015_7;
     String p015_8;
     String p015_9;
     String p015_10;
     String p015_11;
     String p015_11_o;



    public FragmentP015() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentP015(String idEncuestado, Context context) {
        this.idEncuestado = idEncuestado;
        this.context = context;
        Data data = new Data(context);
        data.open();
        Residente residente = data.getResidente(idEncuestado);
        idHogar = residente.getId_hogar();
        idVivienda = residente.getId_vivienda();
        idInformante = "";
        data.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_e_p015, container, false);
        layout_e_p015 = (LinearLayout) rootView.findViewById(R.id.layout_e_p015);
        rg_e_P015_1 = rootView.findViewById(R.id.rg_e_p015_1);
        rg_e_P015_2 = rootView.findViewById(R.id.rg_e_p015_2);
        rg_e_P015_3 = rootView.findViewById(R.id.rg_e_p015_3);
        rg_e_P015_4 = rootView.findViewById(R.id.rg_e_p015_4);
        rg_e_P015_5 = rootView.findViewById(R.id.rg_e_p015_5);
        rg_e_P015_6 = rootView.findViewById(R.id.rg_e_p015_6);
        rg_e_P015_7 = rootView.findViewById(R.id.rg_e_p015_7);
        rg_e_P015_8 = rootView.findViewById(R.id.rg_e_p015_8);
        rg_e_P015_9 = rootView.findViewById(R.id.rg_e_p015_9);
        rg_e_P015_10 = rootView.findViewById(R.id.rg_e_p015_10);
        rg_e_P015_11 = rootView.findViewById(R.id.rg_e_p015_11);
        et_e_P015_11_o = rootView.findViewById(R.id.et_e_p015_o);

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rg_e_P015_11.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                controlarEspecifiqueRadio(radioGroup,i,1,et_e_P015_11_o);
            }
        });

        llenarVista();
        cargarDatos();
    }



    @Override
    public void guardarDatos() {
        Data data = new Data(context);
        data.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo5_id_informante,idInformante);
        contentValues.put(SQLConstantes.seccion_e_p315_1,p015_1);
        contentValues.put(SQLConstantes.seccion_e_p315_2,p015_2);
        contentValues.put(SQLConstantes.seccion_e_p315_3,p015_3);
        contentValues.put(SQLConstantes.seccion_e_p315_4,p015_4);
        contentValues.put(SQLConstantes.seccion_e_p315_5,p015_5);
        contentValues.put(SQLConstantes.seccion_e_p315_6,p015_6);
        contentValues.put(SQLConstantes.seccion_e_p315_7,p015_7);
        contentValues.put(SQLConstantes.seccion_e_p315_8,p015_8);
        contentValues.put(SQLConstantes.seccion_e_p315_9,p015_9);
        contentValues.put(SQLConstantes.seccion_e_p315_10,p015_10);
        contentValues.put(SQLConstantes.seccion_e_p315_11,p015_11);
        contentValues.put(SQLConstantes.seccion_e_p315_11_o,p015_11_o);

        if(!data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo5 modulo5 = new Modulo5();
            modulo5.set_id(idEncuestado);
            modulo5.setIdHogar(idHogar);
            modulo5.setIdVivienda(idVivienda);
            data.insertarElemento(getNombreTabla(),modulo5.toValues());
        }
        data.actualizarElemento(getNombreTabla(),contentValues,idEncuestado);
        data.close();
    }

    @Override
    public void llenarVariables() {
        p015_1 = rg_e_P015_1.indexOfChild(rg_e_P015_1.findViewById(rg_e_P015_1.getCheckedRadioButtonId()))+"";
        p015_2 = rg_e_P015_2.indexOfChild(rg_e_P015_2.findViewById(rg_e_P015_2.getCheckedRadioButtonId()))+"";
        p015_3 = rg_e_P015_3.indexOfChild(rg_e_P015_3.findViewById(rg_e_P015_3.getCheckedRadioButtonId()))+"";
        p015_4 = rg_e_P015_4.indexOfChild(rg_e_P015_4.findViewById(rg_e_P015_4.getCheckedRadioButtonId()))+"";
        p015_5 = rg_e_P015_5.indexOfChild(rg_e_P015_5.findViewById(rg_e_P015_5.getCheckedRadioButtonId()))+"";
        p015_6 = rg_e_P015_6.indexOfChild(rg_e_P015_6.findViewById(rg_e_P015_6.getCheckedRadioButtonId()))+"";
        p015_7 = rg_e_P015_7.indexOfChild(rg_e_P015_7.findViewById(rg_e_P015_7.getCheckedRadioButtonId()))+"";
        p015_8 = rg_e_P015_8.indexOfChild(rg_e_P015_8.findViewById(rg_e_P015_8.getCheckedRadioButtonId()))+"";
        p015_9 = rg_e_P015_9.indexOfChild(rg_e_P015_9.findViewById(rg_e_P015_9.getCheckedRadioButtonId()))+"";
        p015_10 = rg_e_P015_10.indexOfChild(rg_e_P015_10.findViewById(rg_e_P015_10.getCheckedRadioButtonId()))+"";
        p015_11 = rg_e_P015_11.indexOfChild(rg_e_P015_11.findViewById(rg_e_P015_11.getCheckedRadioButtonId()))+"";
        p015_11_o = et_e_P015_11_o.getText().toString();
    }

    @Override
    public void cargarDatos() {
        Data data = new Data(context);
        data.open();
        if (data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo5 modulo5 = data.getModulo5(idEncuestado);
            if(!modulo5.getP315_1().equals("-1") && !modulo5.getP315_1().equals(""))((RadioButton)rg_e_P015_1.getChildAt(Integer.parseInt(modulo5.getP315_1()))).setChecked(true);
            if(!modulo5.getP315_2().equals("-1") && !modulo5.getP315_2().equals(""))((RadioButton)rg_e_P015_2.getChildAt(Integer.parseInt(modulo5.getP315_2()))).setChecked(true);
            if(!modulo5.getP315_3().equals("-1") && !modulo5.getP315_3().equals(""))((RadioButton)rg_e_P015_3.getChildAt(Integer.parseInt(modulo5.getP315_3()))).setChecked(true);
            if(!modulo5.getP315_4().equals("-1") && !modulo5.getP315_4().equals(""))((RadioButton)rg_e_P015_4.getChildAt(Integer.parseInt(modulo5.getP315_4()))).setChecked(true);
            if(!modulo5.getP315_5().equals("-1") && !modulo5.getP315_5().equals(""))((RadioButton)rg_e_P015_5.getChildAt(Integer.parseInt(modulo5.getP315_5()))).setChecked(true);
            if(!modulo5.getP315_6().equals("-1") && !modulo5.getP315_6().equals(""))((RadioButton)rg_e_P015_6.getChildAt(Integer.parseInt(modulo5.getP315_6()))).setChecked(true);
            if(!modulo5.getP315_7().equals("-1") && !modulo5.getP315_7().equals(""))((RadioButton)rg_e_P015_7.getChildAt(Integer.parseInt(modulo5.getP315_7()))).setChecked(true);
            if(!modulo5.getP315_8().equals("-1") && !modulo5.getP315_8().equals(""))((RadioButton)rg_e_P015_8.getChildAt(Integer.parseInt(modulo5.getP315_8()))).setChecked(true);
            if(!modulo5.getP315_9().equals("-1") && !modulo5.getP315_9().equals(""))((RadioButton)rg_e_P015_9.getChildAt(Integer.parseInt(modulo5.getP315_9()))).setChecked(true);
            if(!modulo5.getP315_10().equals("-1") && !modulo5.getP315_10().equals(""))((RadioButton)rg_e_P015_10.getChildAt(Integer.parseInt(modulo5.getP315_10()))).setChecked(true);
            if(!modulo5.getP315_11().equals("-1") && !modulo5.getP315_11().equals(""))((RadioButton)rg_e_P015_11.getChildAt(Integer.parseInt(modulo5.getP315_11()))).setChecked(true);
            et_e_P015_11_o.setText(modulo5.getP315_11_o());
        }
        data.close();
    }

    @Override
    public void llenarVista() {

    }

    @Override
    public boolean validarDatos() {
        llenarVariables();
        return true;
    }

    @Override
    public String getNombreTabla() {
        return SQLConstantes.tablamodulo5;
    }

    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void configurarEditText(final EditText editText, final View view, int tipo,int longitud){
        switch (tipo){
            case 0:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud), new InputFilterSoloLetras()});break;
            case 1:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud)});break;
            case 2:editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)});
                editText.setTransformationMethod(new NumericKeyBoardTransformationMethod());break;
        }

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ocultarTeclado(editText);
                    view.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void controlarEspecifiqueRadio(RadioGroup group, int checkedId, int opcionEsp, EditText editTextEspecifique) {
        int seleccionado = group.indexOfChild(group.findViewById(checkedId));
        if(seleccionado == opcionEsp){
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_enabled);
            editTextEspecifique.setEnabled(true);
        }else{
            editTextEspecifique.setText("");
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_disabled);
            editTextEspecifique.setEnabled(false);
        }
    }

    public void ocultarOtrosLayouts(){

    }

    public boolean verificarCoberturaCapitulo(){

        return true;
    }
}
