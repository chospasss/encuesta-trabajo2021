package com.example.ricindigus.trabajo2020.fragments.modulo4;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo4;
import com.example.ricindigus.trabajo2020.modelo.pojos.Residente;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InputFilterSoloLetras;
import com.example.ricindigus.trabajo2020.util.NumericKeyBoardTransformationMethod;


public class FragmentP012 extends FragmentPagina {
    String idEncuestado;
    Context context;
    String idInformante;
    String idHogar;
    String idVivienda;

    LinearLayout layout_d_p012;

    RadioGroup radiogroup_d_p012_1,radiogroup_d_p012_2,radiogroup_d_p012_3,radiogroup_d_p012_4,radiogroup_d_p012_5,radiogroup_d_p012_6,radiogroup_d_p012_7,radiogroup_d_p012_8,radiogroup_d_p012_9,radiogroup_d_p012_10;
    RadioGroup radiogroup_d_p012_11;
    EditText edittext_d_P012_O;

    //nuevos
    private int  p312_1;
    private int  p312_2;
    private int  p312_3;
    private int  p312_4;
    private int  p312_5;
    private int  p312_6;
    private int  p312_7;
    private int  p312_8;
    private int  p312_9;
    private int  p312_10;
    private int  p312_11;
    private String  p312_11_o;


    public FragmentP012() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentP012(String idEncuestado, Context context) {
        this.idEncuestado = idEncuestado;
        this.context = context;
        Data data = new Data(context);
        data.open();
        Residente residente = data.getResidente(idEncuestado);
        idHogar = residente.getId_hogar();
        idVivienda = residente.getId_vivienda();
        idInformante = "";
        data.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_d_p012, container, false);

        //nuevos
        layout_d_p012 = rootView.findViewById(R.id.layout_d_p012);

        radiogroup_d_p012_1 = rootView.findViewById(R.id.seccion_d_p012_radiogroup_1);
        radiogroup_d_p012_2 = rootView.findViewById(R.id.seccion_d_p012_radiogroup_2);
        radiogroup_d_p012_3 = rootView.findViewById(R.id.seccion_d_p012_radiogroup_3);
        radiogroup_d_p012_4 = rootView.findViewById(R.id.seccion_d_p012_radiogroup_4);
        radiogroup_d_p012_5 = rootView.findViewById(R.id.seccion_d_p012_radiogroup_5);
        radiogroup_d_p012_6 = rootView.findViewById(R.id.seccion_d_p012_radiogroup_6);
        radiogroup_d_p012_7 = rootView.findViewById(R.id.seccion_d_p012_radiogroup_7);
        radiogroup_d_p012_8 = rootView.findViewById(R.id.seccion_d_p012_radiogroup_8);
        radiogroup_d_p012_9 = rootView.findViewById(R.id.seccion_d_p012_radiogroup_9);
        radiogroup_d_p012_10 = rootView.findViewById(R.id.seccion_d_p012_radiogroup_10);

        radiogroup_d_p012_11 = rootView.findViewById(R.id.seccion_d_p012_radiogroup_11);
        edittext_d_P012_O = rootView.findViewById(R.id.seccion_d_p012_edittext);


        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //nuevos

        radiogroup_d_p012_11.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,1,edittext_d_P012_O);
            }
        });


        //fin de nuevos
        llenarVista();
        cargarDatos();
    }



    @Override
    public void guardarDatos() {
        Data data = new Data(context);
        data.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo4_id_informante,idInformante);
        contentValues.put(SQLConstantes.seccion_d_p312_1,p312_1);
        contentValues.put(SQLConstantes.seccion_d_p312_2,p312_2);
        contentValues.put(SQLConstantes.seccion_d_p312_3,p312_3);
        contentValues.put(SQLConstantes.seccion_d_p312_4,p312_4);
        contentValues.put(SQLConstantes.seccion_d_p312_5,p312_5);
        contentValues.put(SQLConstantes.seccion_d_p312_6,p312_6);
        contentValues.put(SQLConstantes.seccion_d_p312_7,p312_7);
        contentValues.put(SQLConstantes.seccion_d_p312_8,p312_8);
        contentValues.put(SQLConstantes.seccion_d_p312_9,p312_9);
        contentValues.put(SQLConstantes.seccion_d_p312_10,p312_10);
        contentValues.put(SQLConstantes.seccion_d_p312_11,p312_11);
        contentValues.put(SQLConstantes.seccion_d_p312_11_o,p312_11_o);

        data.actualizarElemento(getNombreTabla(),contentValues,idEncuestado);
        //Ya valido y guardo correctamente el fragment, ahora actualizamos el valor de la cobertura del fragment a correcto(1)
        data.actualizarValor(SQLConstantes.tablacoberturafragments,SQLConstantes.cobertura_fragments_cp405p407,"1",idEncuestado);
        //verificamos la cobertura del capitulo y actualizamos su valor de cobertura.
        if (verificarCoberturaCapitulo()) data.actualizarValor(getNombreTabla(),SQLConstantes.modulo4_COB400,"1",idEncuestado);
        else data.actualizarValor(getNombreTabla(),SQLConstantes.modulo4_COB400,"0",idEncuestado);
        data.actualizarValor(SQLConstantes.tablaresidentes,SQLConstantes.residentes_encuestado_cobertura,"0",idEncuestado);
        data.close();
    }

    @Override
    public void llenarVariables() {
//nuevos

        p312_1 = radiogroup_d_p012_1.indexOfChild(radiogroup_d_p012_1.findViewById(radiogroup_d_p012_1.getCheckedRadioButtonId()));

        p312_2 = radiogroup_d_p012_2.indexOfChild(radiogroup_d_p012_2.findViewById(radiogroup_d_p012_2.getCheckedRadioButtonId()));

        p312_3 = radiogroup_d_p012_3.indexOfChild(radiogroup_d_p012_3.findViewById(radiogroup_d_p012_3.getCheckedRadioButtonId()));

        p312_4 = radiogroup_d_p012_4.indexOfChild(radiogroup_d_p012_4.findViewById(radiogroup_d_p012_4.getCheckedRadioButtonId()));

        p312_5 = radiogroup_d_p012_5.indexOfChild(radiogroup_d_p012_5.findViewById(radiogroup_d_p012_5.getCheckedRadioButtonId()));

        p312_6 = radiogroup_d_p012_6.indexOfChild(radiogroup_d_p012_6.findViewById(radiogroup_d_p012_6.getCheckedRadioButtonId()));

        p312_7 = radiogroup_d_p012_7.indexOfChild(radiogroup_d_p012_7.findViewById(radiogroup_d_p012_7.getCheckedRadioButtonId()));

        p312_8 = radiogroup_d_p012_8.indexOfChild(radiogroup_d_p012_8.findViewById(radiogroup_d_p012_8.getCheckedRadioButtonId()));

        p312_9 = radiogroup_d_p012_9.indexOfChild(radiogroup_d_p012_9.findViewById(radiogroup_d_p012_9.getCheckedRadioButtonId()));

        p312_10 = radiogroup_d_p012_10.indexOfChild(radiogroup_d_p012_10.findViewById(radiogroup_d_p012_10.getCheckedRadioButtonId()));

        p312_11 = radiogroup_d_p012_11.indexOfChild(radiogroup_d_p012_11.findViewById(radiogroup_d_p012_11.getCheckedRadioButtonId()));
        p312_11_o = edittext_d_P012_O.getText().toString();

        //fin de nuevos
    }

    @Override
    public void cargarDatos() {

        Data data = new Data(context);
        data.open();
        if (data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo4 modulo4 = data.getModulo4(idEncuestado);

            //nuevos

            if( !modulo4.getP312_1().equals("-1") && !modulo4.getP312_1().equals(""))((RadioButton)radiogroup_d_p012_1.getChildAt(Integer.parseInt(modulo4.getP312_1()))).setChecked(true);
            if( !modulo4.getP312_2().equals("-1") && !modulo4.getP312_2().equals(""))((RadioButton)radiogroup_d_p012_2.getChildAt(Integer.parseInt(modulo4.getP312_2()))).setChecked(true);
            if( !modulo4.getP312_3().equals("-1") && !modulo4.getP312_3().equals(""))((RadioButton)radiogroup_d_p012_3.getChildAt(Integer.parseInt(modulo4.getP312_3()))).setChecked(true);
            if( !modulo4.getP312_4().equals("-1") && !modulo4.getP312_4().equals(""))((RadioButton)radiogroup_d_p012_4.getChildAt(Integer.parseInt(modulo4.getP312_4()))).setChecked(true);
            if( !modulo4.getP312_5().equals("-1") && !modulo4.getP312_5().equals(""))((RadioButton)radiogroup_d_p012_5.getChildAt(Integer.parseInt(modulo4.getP312_5()))).setChecked(true);
            if( !modulo4.getP312_6().equals("-1") && !modulo4.getP312_6().equals(""))((RadioButton)radiogroup_d_p012_6.getChildAt(Integer.parseInt(modulo4.getP312_6()))).setChecked(true);
            if( !modulo4.getP312_7().equals("-1") && !modulo4.getP312_7().equals(""))((RadioButton)radiogroup_d_p012_7.getChildAt(Integer.parseInt(modulo4.getP312_7()))).setChecked(true);
            if( !modulo4.getP312_8().equals("-1") && !modulo4.getP312_8().equals(""))((RadioButton)radiogroup_d_p012_8.getChildAt(Integer.parseInt(modulo4.getP312_8()))).setChecked(true);
            if( !modulo4.getP312_9().equals("-1") && !modulo4.getP312_9().equals(""))((RadioButton)radiogroup_d_p012_9.getChildAt(Integer.parseInt(modulo4.getP312_9()))).setChecked(true);
            if( !modulo4.getP312_10().equals("-1") && !modulo4.getP312_10().equals(""))((RadioButton)radiogroup_d_p012_10.getChildAt(Integer.parseInt(modulo4.getP312_10()))).setChecked(true);
            if( !modulo4.getP312_11().equals("-1") && !modulo4.getP312_11().equals(""))((RadioButton)radiogroup_d_p012_11.getChildAt(Integer.parseInt(modulo4.getP312_11()))).setChecked(true);

            edittext_d_P012_O.setText(modulo4.getP312_11_o());

            //fin de nuevos
        }
        data.close();

    }

    @Override
    public void llenarVista() {

    }

    @Override
    public boolean validarDatos() {
        llenarVariables();
        return true;
    }

    @Override
    public String getNombreTabla() {
        return SQLConstantes.tablamodulo4;
    }

    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void configurarEditText(final EditText editText, final View view, int tipo,int longitud){
        switch (tipo){
            case 0:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud), new InputFilterSoloLetras()});break;
            case 1:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud)});break;
            case 2:editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)});
                editText.setTransformationMethod(new NumericKeyBoardTransformationMethod());break;
        }

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ocultarTeclado(editText);
                    view.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void controlarEspecifiqueRadio(RadioGroup group, int checkedId, int opcionEsp, EditText editTextEspecifique) {
        int seleccionado = group.indexOfChild(group.findViewById(checkedId));
        if(seleccionado == opcionEsp){
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_enabled);
            editTextEspecifique.setEnabled(true);
        }else{
            editTextEspecifique.setText("");
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_disabled);
            editTextEspecifique.setEnabled(false);
        }
    }

    public void ocultarOtrosLayouts(){

    }

    public boolean verificarCoberturaCapitulo(){

        return true;
    }
}
