package com.example.ricindigus.trabajo2020.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ricindigus.trabajo2020.adapters.ExpandListAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.fragments.modulo3.FragmentP001P003;
import com.example.ricindigus.trabajo2020.fragments.modulo3.FragmentP004P006;
import com.example.ricindigus.trabajo2020.fragments.modulo3.FragmentP309;
import com.example.ricindigus.trabajo2020.fragments.modulo3.FragmentP310P312;
import com.example.ricindigus.trabajo2020.fragments.modulo3.FragmentP313P317;
import com.example.ricindigus.trabajo2020.fragments.modulo3.FragmentP318;
import com.example.ricindigus.trabajo2020.fragments.modulo4.FragmentP007P011;
import com.example.ricindigus.trabajo2020.fragments.modulo4.FragmentP012;
import com.example.ricindigus.trabajo2020.fragments.modulo4.FragmentP013P014;
import com.example.ricindigus.trabajo2020.fragments.modulo4.FragmentP401P404;
import com.example.ricindigus.trabajo2020.fragments.modulo4.FragmentP405P407;
import com.example.ricindigus.trabajo2020.fragments.modulo4.FragmentP408P410;
import com.example.ricindigus.trabajo2020.fragments.modulo4.FragmentP411P416;
import com.example.ricindigus.trabajo2020.fragments.modulo5.FragmentP015;
import com.example.ricindigus.trabajo2020.fragments.modulo5.FragmentP016;
import com.example.ricindigus.trabajo2020.fragments.modulo5.FragmentP017P020;
import com.example.ricindigus.trabajo2020.fragments.modulo5.FragmentP021P025;
import com.example.ricindigus.trabajo2020.fragments.modulo5.FragmentP501P505;
import com.example.ricindigus.trabajo2020.fragments.modulo5.FragmentP506P507;
import com.example.ricindigus.trabajo2020.fragments.modulo5.FragmentP508P511;
import com.example.ricindigus.trabajo2020.fragments.modulo5.FragmentP512P513;
import com.example.ricindigus.trabajo2020.fragments.modulo6.FragmentP026P029;
import com.example.ricindigus.trabajo2020.fragments.modulo6.FragmentP030P033;
import com.example.ricindigus.trabajo2020.fragments.modulo6.FragmentP034P036;
import com.example.ricindigus.trabajo2020.fragments.modulo6.FragmentP601P604;
import com.example.ricindigus.trabajo2020.fragments.modulo6.FragmentP605P608;
import com.example.ricindigus.trabajo2020.fragments.modulo6.FragmentP609P612;
import com.example.ricindigus.trabajo2020.fragments.modulo6.FragmentP613P617;
import com.example.ricindigus.trabajo2020.fragments.modulo6.FragmentP618P621;
import com.example.ricindigus.trabajo2020.fragments.modulo6.FragmentP622P625;
import com.example.ricindigus.trabajo2020.fragments.modulo6.FragmentP626P629;
import com.example.ricindigus.trabajo2020.fragments.modulo6.FragmentP630;
import com.example.ricindigus.trabajo2020.fragments.modulo7.FragmentP038P039;
import com.example.ricindigus.trabajo2020.fragments.modulo7.FragmentP040;
import com.example.ricindigus.trabajo2020.fragments.modulo7.FragmentP701P705;
import com.example.ricindigus.trabajo2020.fragments.modulo7.FragmentP706P709;
import com.example.ricindigus.trabajo2020.fragments.modulo8.FragmentP041P043;
import com.example.ricindigus.trabajo2020.fragments.modulo8.FragmentP044P046;
import com.example.ricindigus.trabajo2020.fragments.modulo8.FragmentP801P804;
import com.example.ricindigus.trabajo2020.fragments.modulo8.FragmentP805P808;
import com.example.ricindigus.trabajo2020.fragments.modulo8.FragmentP809P812;
import com.example.ricindigus.trabajo2020.fragments.modulo8.FragmentP813P816;
import com.example.ricindigus.trabajo2020.fragments.modulo8.FragmentP817P820;
import com.example.ricindigus.trabajo2020.fragments.modulo8.FragmentP821P823;
import com.example.ricindigus.trabajo2020.fragments.modulo9.FragmentP047P048;
import com.example.ricindigus.trabajo2020.fragments.modulo9.FragmentP049P051;
import com.example.ricindigus.trabajo2020.fragments.modulo9.FragmentP052P053;
import com.example.ricindigus.trabajo2020.fragments.modulo9.FragmentP054P056;
import com.example.ricindigus.trabajo2020.fragments.modulo9.FragmentP057P059;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Hogar;
import com.example.ricindigus.trabajo2020.modelo.pojos.Residente;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InterfazEncuesta;
import com.example.ricindigus.trabajo2020.util.TipoFragmentEncuestado;
import com.example.ricindigus.trabajo2020.util.TipoFragmentVivienda;

public class EncuestaActivity extends AppCompatActivity implements InterfazEncuesta{
    private ArrayList<String> listDataHeader;
    private ExpandableListView expListView;
    private HashMap<String, List<String>> listDataChild;
    private ExpandListAdapter listAdapter;
    private String idEncuestado;
    private String idVivienda;
    private String idHogar;
    private String numero;

    private TextView btnAtras;
    private TextView btnSiguiente;
    int tFragment = 1;
    FragmentPagina fragmentActual;
    int moduloActual = 3;
    boolean ir_modulo8=false;

    //CAMBIE EN LA LINEA 166 ESTO: setFragment(TipoFragmentVivienda.CARATULA,1); SOLUCIONADO LA PARTE DEL PERMISO DE LOS SALTOS

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encuesta);
        btnAtras = (TextView) findViewById(R.id.boton_anterior);
        btnSiguiente = (TextView) findViewById(R.id.boton_siguiente);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        idVivienda = getIntent().getExtras().getString("idVivienda");
        idHogar = getIntent().getExtras().getString("idHogar");
        idEncuestado = getIntent().getExtras().getString("idEncuestado");
        numero = getIntent().getExtras().getString("numero");

        Data data =  new Data(this);
        data.open();
        Hogar hogar = data.getHogar(idHogar);
        Residente residente = data.getResidente(idEncuestado);
        data.close();

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("RESIDENTE N° " + numero + " - HOGAR N° " + hogar.getNumero() + " - VIVIENDA N° " + hogar.getId_vivienda());

        if (residente.getC2_p205_a().equals("")) getSupportActionBar().setSubtitle(residente.getC2_p202() + "("+ residente.getC2_p205_m() +" meses)");
        else getSupportActionBar().setSubtitle(residente.getC2_p202() + "("+ residente.getC2_p205_a() +" años)");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        enableExpandableList();


        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSiguiente.setEnabled(false);
                ocultarTeclado(btnSiguiente);
                if(fragmentActual.validarDatos()){
                    fragmentActual.guardarDatos();
                    tFragment++;
                    if(tFragment == 22) salirEncuestaFinalizada();
                    else{
                        habilitarFragment(tFragment);
                        while(!setFragment(tFragment,1)){
                            tFragment++;
                            if(tFragment == 22) salirEncuestaFinalizada();
                            habilitarFragment(tFragment);
                        }
                    }

                }
                btnSiguiente.setEnabled(true);
            }
        });

        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnAtras.setEnabled(false);
                ocultarTeclado(btnAtras);
                tFragment--;
                while(!setFragment(tFragment,-1)){
                    tFragment--;
                }
                btnAtras.setEnabled(true);
            }
        });
        setFragment(TipoFragmentVivienda.CARATULA,1);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            salirActivityEncuestado();
        }
    }

    public void setIr_modulo8(boolean ir){ir_modulo8=ir;}

    public void habilitarFragment(int tipoFragment){
        Data data =  new Data(this);
        data.open();
        switch (tipoFragment){
            case TipoFragmentEncuestado.P001P003:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_c_p001p003,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_c_p001p003,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P004P006:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_c_p004p006,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_c_p004p006,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P007P011:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_d_p007p011,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_d_p007p011,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P012:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_d_p012,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_d_p012,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P013P014:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_d_p013p014,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_d_p013p014,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P015:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p015,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p015,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P016:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p016,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p016,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P017P020:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p017p020,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p017p020,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P021P025:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p021p025,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p021p025,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P026P029:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_f_p026p029,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_f_p026p029,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P030P033:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_f_p030p033,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_f_p030p033,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P034P036:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_f_p034p036,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_f_p034p036,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P038P039:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_g_p038p039,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_g_p038p039,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P040:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_g_p040,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_g_p040,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P041P043:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_h_p041p043,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_h_p041p043,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P044P046:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_h_p044p046,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_h_p044p046,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P047P048:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p047p048,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p047p048,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P049P051:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p049p051,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p049p051,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P052P053:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p052p053,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p052p053,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P054P056:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p054p056,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p054p056,"1",idEncuestado);
                break;
            case TipoFragmentEncuestado.P057P059:
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p057p059,idEncuestado).equals("0"))
                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p057p059,"1",idEncuestado);
                break;




//            case TipoFragmentEncuestado.P301P305:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p301p305,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p301p305,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P306P308:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p306p308,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p306p308,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P309:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p309,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p309,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P310P312:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p310p312,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p310p312,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P313P317:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p313p317,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p313p317,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P318:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p318,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p318,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P401P404:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p401p404,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p401p404,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P405P407:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p405p407,idEncuestado).equals("0"))
//                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p405p407,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P408P410:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p408p410,idEncuestado).equals("0"))
//                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p408p410,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P411P416:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p411p416,idEncuestado).equals("0"))
//                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p411p416,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P501P505:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p501p505,idEncuestado).equals("0"))
//                    data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p501p505,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P506P507:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p506p507,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p506p507,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P508P511:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p508p511,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p508p511,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P512P513:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p512p513,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p512p513,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P601P604:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p601p604,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p601p604,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P605P608:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p605p608,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p605p608,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P609P612:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p609p612,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p609p612,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P613P618:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p613p617,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p613p617,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P619P622:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p618p621,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p618p621,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P623P625:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p622p625,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p622p625,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P626P629:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p626p629,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p626p629,"1",idEncuestado);
//                break;


//            case TipoFragmentEncuestado.P630:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p630,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p630,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P701P705:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p701p705,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p701p705,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P706P709:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p706p709,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p706p709,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P801P804:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p801p804,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p801p804,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P805P808:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p805p808,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p805p808,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P809P812:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p809p812,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p809p812,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P813P816:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p813p816,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p813p816,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P817P820:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p817p820,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p817p820,"1",idEncuestado);
//                break;
//            case TipoFragmentEncuestado.P821P823:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p821p823,idEncuestado).equals("0"))
//                data.actualizarValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p821p823,"1",idEncuestado);
//                break;
        }
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public boolean setFragment(int tipoFragment, int direccion){
        Log.e("tipoFragment", "setFragment: "+tipoFragment );
        Log.e("direccion", "setFragment: "+direccion );
        if (seteoValido(tipoFragment)){
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            if(direccion != 0){
                if(direccion > 0){
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                }else{
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                }
            }
            btnAtras.setVisibility(View.VISIBLE);
            btnSiguiente.setVisibility(View.VISIBLE);
            if(tipoFragment == TipoFragmentEncuestado.P057P059) btnSiguiente.setText("Finalizar");
            else btnSiguiente.setText("Siguiente");
            switch (tipoFragment){
                case TipoFragmentEncuestado.P001P003:
                    btnAtras.setVisibility(View.GONE);
                    FragmentP001P003 fragmentP001P003 = new FragmentP001P003(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP001P003);
                    tFragment = TipoFragmentEncuestado.P001P003;
                    fragmentActual = fragmentP001P003;
                    moduloActual = 3;
                    break;
                case TipoFragmentEncuestado.P004P006:
                    FragmentP004P006 fragmentP004P006 = new FragmentP004P006(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP004P006);
                    tFragment = TipoFragmentEncuestado.P004P006;
                    fragmentActual = fragmentP004P006;
                    moduloActual = 3;
                    break;
                case TipoFragmentEncuestado.P007P011:
                    FragmentP007P011 fragmentP007P011 = new FragmentP007P011(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP007P011);
                    tFragment = TipoFragmentEncuestado.P007P011;
                    fragmentActual = fragmentP007P011;
                    moduloActual = 4;
                    break;
                case TipoFragmentEncuestado.P012:
                    FragmentP012 fragmentP012 = new FragmentP012(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP012);
                    tFragment = TipoFragmentEncuestado.P012;
                    fragmentActual = fragmentP012;
                    moduloActual = 4;
                    break;
                case TipoFragmentEncuestado.P013P014:
                    FragmentP013P014 fragmentP013P014 = new FragmentP013P014(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP013P014);
                    tFragment = TipoFragmentEncuestado.P013P014;
                    fragmentActual = fragmentP013P014;
                    moduloActual = 4;
                    break;
                case TipoFragmentEncuestado.P015:
                    FragmentP015 fragmentP015 = new FragmentP015(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP015);
                    tFragment = TipoFragmentEncuestado.P015;
                    fragmentActual = fragmentP015;
                    moduloActual = 5;
                    break;
                case TipoFragmentEncuestado.P016:
                    FragmentP016 fragmentP016 = new FragmentP016(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP016);
                    tFragment = TipoFragmentEncuestado.P016;
                    fragmentActual = fragmentP016;
                    moduloActual = 5;
                    break;
                case TipoFragmentEncuestado.P017P020:
                    FragmentP017P020 fragmentP017P020 = new FragmentP017P020(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP017P020);
                    tFragment = TipoFragmentEncuestado.P017P020;
                    fragmentActual = fragmentP017P020;
                    moduloActual = 5;
                    break;
                case TipoFragmentEncuestado.P021P025:
                    FragmentP021P025 fragmentP021P025 = new FragmentP021P025(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP021P025);
                    tFragment = TipoFragmentEncuestado.P021P025;
                    fragmentActual = fragmentP021P025;
                    moduloActual = 5;
                    break;
                case TipoFragmentEncuestado.P026P029:
                    FragmentP026P029 fragmentP026P029 = new FragmentP026P029(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP026P029);
                    tFragment = TipoFragmentEncuestado.P026P029;
                    fragmentActual = fragmentP026P029;
                    moduloActual = 6;
                    break;
                case TipoFragmentEncuestado.P030P033:
                    FragmentP030P033 fragmentP030P033 = new FragmentP030P033(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP030P033);
                    tFragment = TipoFragmentEncuestado.P030P033;
                    fragmentActual = fragmentP030P033;
                    moduloActual = 6;
                    break;
                case TipoFragmentEncuestado.P034P036:
                    FragmentP034P036 fragmentP034P036 = new FragmentP034P036(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP034P036);
                    tFragment = TipoFragmentEncuestado.P034P036;
                    fragmentActual = fragmentP034P036;
                    moduloActual = 6;
                    break;
                case TipoFragmentEncuestado.P038P039:
                    FragmentP038P039 fragmentP038P039 = new FragmentP038P039(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP038P039);
                    tFragment = TipoFragmentEncuestado.P038P039;
                    fragmentActual = fragmentP038P039;
                    moduloActual =7 ;
                    break;
                case TipoFragmentEncuestado.P040:
                    FragmentP040 fragmentP040 = new FragmentP040(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP040);
                    tFragment = TipoFragmentEncuestado.P040;
                    fragmentActual = fragmentP040;
                    moduloActual = 7;
                    break;
                case TipoFragmentEncuestado.P041P043:

                    FragmentP041P043 fragmentP041P043 = new FragmentP041P043(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP041P043);
                    tFragment = TipoFragmentEncuestado.P041P043;
                    fragmentActual = fragmentP041P043;
                    moduloActual = 8;
                    break;
                case TipoFragmentEncuestado.P044P046:

                    FragmentP044P046 fragmentP044P046 = new FragmentP044P046(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP044P046);
                    tFragment = TipoFragmentEncuestado.P044P046;
                    fragmentActual = fragmentP044P046;
                    moduloActual = 8;
                    break;
                case TipoFragmentEncuestado.P047P048:
                    FragmentP047P048 fragmentP047P048 = new FragmentP047P048(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP047P048);
                    tFragment = TipoFragmentEncuestado.P047P048;
                    fragmentActual = fragmentP047P048;
                    moduloActual = 9;
                    break;
                case TipoFragmentEncuestado.P049P051:
                    FragmentP049P051 fragmentP049P051 = new FragmentP049P051(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP049P051);
                    tFragment = TipoFragmentEncuestado.P049P051;
                    fragmentActual = fragmentP049P051;
                    moduloActual = 9;
                    break;
                case TipoFragmentEncuestado.P052P053:
                    FragmentP052P053 fragmentP052P053 = new FragmentP052P053(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP052P053);
                    tFragment = TipoFragmentEncuestado.P052P053;
                    fragmentActual = fragmentP052P053;
                    moduloActual = 9;
                    break;
                case TipoFragmentEncuestado.P054P056:
                    FragmentP054P056 fragmentP054P056 = new FragmentP054P056(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP054P056);
                    tFragment = TipoFragmentEncuestado.P054P056;
                    fragmentActual = fragmentP054P056;
                    moduloActual = 9;
                    break;
                case TipoFragmentEncuestado.P057P059:
                    FragmentP057P059 fragmentP057P059 = new FragmentP057P059(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP057P059);
                    tFragment = TipoFragmentEncuestado.P057P059;
                    fragmentActual = fragmentP057P059;
                    moduloActual = 9;
                    break;
//                case TipoFragmentEncuestado.P0:
//                    Log.e("cesar2", "onClick: "+ tFragment);
//                    FragmentP0 fragmentP0 = new FragmentP0(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP0);
//                    tFragment = TipoFragmentEncuestado.P0;
//                    fragmentActual = fragmentP0;
//                    moduloActual = 4;
//                    break;

//                case TipoFragmentEncuestado.P301P305:
//                    Log.e("cesar1", "onClick: "+ tFragment);
//                    btnAtras.setVisibility(View.GONE);
//                    FragmentP301P305 fragmentP301P305 = new FragmentP301P305(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP301P305);
//                    tFragment = TipoFragmentEncuestado.P301P305;
//                    fragmentActual = fragmentP301P305;
//                    moduloActual = 3;
//                    break;
//                case TipoFragmentEncuestado.P306P308:
//                    Log.e("cesar2", "onClick: "+ tFragment);
//                    FragmentP306P308 fragmentP306P308 = new FragmentP306P308(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP306P308);
//                    tFragment = TipoFragmentEncuestado.P306P308;
//                    fragmentActual = fragmentP306P308;
//                    moduloActual = 3;
//                    break;
//                case TipoFragmentEncuestado.P309:
//                    Log.e("cesar3", "onClick: "+ tFragment);
//                    FragmentP309 fragmentP309 = new FragmentP309(idEncuestado,idVivienda,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP309);
//                    tFragment = TipoFragmentEncuestado.P309;
//                    fragmentActual = fragmentP309;
//                    moduloActual = 3;
//                    break;
//                case TipoFragmentEncuestado.P310P312:
//                    FragmentP310P312 fragmentP310P312 = new FragmentP310P312(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP310P312);
//                    tFragment = TipoFragmentEncuestado.P310P312;
//                    fragmentActual = fragmentP310P312;
//                    moduloActual = 3;
//                    break;
//                case TipoFragmentEncuestado.P313P317:
//                    FragmentP313P317 fragmentP313P317 = new FragmentP313P317(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP313P317);
//                    tFragment = TipoFragmentEncuestado.P313P317;
//                    fragmentActual = fragmentP313P317;
//                    moduloActual = 3;
//                    break;
//                case TipoFragmentEncuestado.P318:
//                    FragmentP318 fragmentP318 = new FragmentP318(idEncuestado,idVivienda,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP318);
//                    tFragment = TipoFragmentEncuestado.P318;
//                    fragmentActual = fragmentP318;
//                    moduloActual = 3;
//                    break;
//                case TipoFragmentEncuestado.P401P404:
//                    FragmentP401P404 fragmentP401P404 = new FragmentP401P404(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP401P404);
//                    tFragment = TipoFragmentEncuestado.P401P404;
//                    fragmentActual = fragmentP401P404;
//                    moduloActual = 4;
//                    break;
//                case TipoFragmentEncuestado.P405P407:
//                    FragmentP405P407 fragmentP405P407 = new FragmentP405P407(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP405P407);
//                    tFragment = TipoFragmentEncuestado.P405P407;
//                    fragmentActual = fragmentP405P407;
//                    moduloActual = 4;
//                    break;
//                case TipoFragmentEncuestado.P408P410:
//                    FragmentP408P410 fragmentP408P410 = new FragmentP408P410(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP408P410);
//                    tFragment = TipoFragmentEncuestado.P408P410;
//                    fragmentActual = fragmentP408P410;
//                    moduloActual = 4;
//                    break;
//                case TipoFragmentEncuestado.P411P416:
//                    FragmentP411P416 fragmentP411P416 = new FragmentP411P416(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP411P416);
//                    tFragment = TipoFragmentEncuestado.P411P416;
//                    fragmentActual = fragmentP411P416;
//                    moduloActual = 4;
//                    break;
//                case TipoFragmentEncuestado.P501P505:
//                    FragmentP501P505 fragmentP501P505 = new FragmentP501P505(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP501P505);
//                    tFragment = TipoFragmentEncuestado.P501P505;
//                    fragmentActual = fragmentP501P505;
//                    moduloActual = 5;
//                    break;
//                case TipoFragmentEncuestado.P506P507:
//                    FragmentP506P507 fragmentP506P507 = new FragmentP506P507(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP506P507);
//                    tFragment = TipoFragmentEncuestado.P506P507;
//                    fragmentActual = fragmentP506P507;
//                    moduloActual = 5;
//                    break;
//                case TipoFragmentEncuestado.P508P511:
//                    FragmentP508P511 fragmentP508P511 = new FragmentP508P511(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP508P511);
//                    tFragment = TipoFragmentEncuestado.P508P511;
//                    fragmentActual = fragmentP508P511;
//                    moduloActual = 5;
//                    break;
//                case TipoFragmentEncuestado.P512P513:
//                    FragmentP512P513 fragmentP512P513 = new FragmentP512P513(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP512P513);
//                    tFragment = TipoFragmentEncuestado.P512P513;
//                    fragmentActual = fragmentP512P513;
//                    moduloActual = 5;
//                    break;
//                case TipoFragmentEncuestado.P601P604:
//                    FragmentP601P604 fragmentP601P604 = new FragmentP601P604(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP601P604);
//                    tFragment = TipoFragmentEncuestado.P601P604;
//                    fragmentActual = fragmentP601P604;
//                    moduloActual = 6;
//                    break;
//                case TipoFragmentEncuestado.P605P608:
//                    FragmentP605P608 fragmentP605P608 = new FragmentP605P608(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP605P608);
//                    tFragment = TipoFragmentEncuestado.P605P608;
//                    fragmentActual = fragmentP605P608;
//                    moduloActual = 6;
//                    break;
//                case TipoFragmentEncuestado.P609P612:
//                    FragmentP609P612 fragmentP609P612 = new FragmentP609P612(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP609P612);
//                    tFragment = TipoFragmentEncuestado.P609P612;
//                    fragmentActual = fragmentP609P612;
//                    moduloActual = 6;
//                    break;
//                case TipoFragmentEncuestado.P613P618:
//                    FragmentP613P617 fragmentP613P617 = new FragmentP613P617(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP613P617);
//                    tFragment = TipoFragmentEncuestado.P613P618;
//                    fragmentActual = fragmentP613P617;
//                    moduloActual = 6;
//                    break;
//                case TipoFragmentEncuestado.P619P622:
//                    FragmentP618P621 fragmentP618P621 = new FragmentP618P621(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP618P621);
//                    tFragment = TipoFragmentEncuestado.P619P622;
//                    fragmentActual = fragmentP618P621;
//                    moduloActual = 6;
//                    break;
//                case TipoFragmentEncuestado.P623P625:
//                    FragmentP622P625 fragmentP622P625 = new FragmentP622P625(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP622P625);
//                    tFragment = TipoFragmentEncuestado.P623P625;
//                    fragmentActual = fragmentP622P625;
//                    moduloActual = 6;
//                    break;
//                case TipoFragmentEncuestado.P626P629:
//                    FragmentP626P629 fragmentP626P629 = new FragmentP626P629(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP626P629);
//                    tFragment = TipoFragmentEncuestado.P626P629;
//                    fragmentActual = fragmentP626P629;
//                    moduloActual = 6;
//                    break;
//                case TipoFragmentEncuestado.P630:
//                    FragmentP630 fragmentP630 = new FragmentP630(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP630);
//                    tFragment = TipoFragmentEncuestado.P630;
//                    fragmentActual = fragmentP630;
//                    moduloActual = 6;
//                    break;
//                case TipoFragmentEncuestado.P701P705:
//                    FragmentP701P705 fragmentP701P705 = new FragmentP701P705(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP701P705);
//                    tFragment = TipoFragmentEncuestado.P701P705;
//                    fragmentActual = fragmentP701P705;
//                    moduloActual = 7;
//                    break;
//                case TipoFragmentEncuestado.P706P709:
//                    FragmentP706P709 fragmentP706P709 = new FragmentP706P709(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP706P709);
//                    tFragment = TipoFragmentEncuestado.P706P709;
//                    fragmentActual = fragmentP706P709;
//                    moduloActual = 7;
//                    break;
//                case TipoFragmentEncuestado.P801P804:
//                    Log.e("cesar66", "onClick: "+ tFragment);
//                    FragmentP801P804 fragmentP801P804 = new FragmentP801P804(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP801P804);
//                    tFragment = TipoFragmentEncuestado.P801P804;
//                    fragmentActual = fragmentP801P804;
//                    moduloActual = 8;
//                    break;
//                case TipoFragmentEncuestado.P805P808:
//                    FragmentP805P808 fragmentP805P808 = new FragmentP805P808(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP805P808);
//                    tFragment = TipoFragmentEncuestado.P805P808;
//                    fragmentActual = fragmentP805P808;
//                    moduloActual = 8;
//                    break;
//                case TipoFragmentEncuestado.P809P812:
//                    FragmentP809P812 fragmentP809P812 = new FragmentP809P812(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP809P812);
//                    tFragment = TipoFragmentEncuestado.P809P812;
//                    fragmentActual = fragmentP809P812;
//                    moduloActual = 8;
//                    break;
//                case TipoFragmentEncuestado.P813P816:
//                    FragmentP813P816 fragmentP813P816 = new FragmentP813P816(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP813P816);
//                    tFragment = TipoFragmentEncuestado.P813P816;
//                    fragmentActual = fragmentP813P816;
//                    moduloActual = 8;
//                    break;
//                case TipoFragmentEncuestado.P817P820:
//                    FragmentP817P820 fragmentP817P820 = new FragmentP817P820(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP817P820);
//                    tFragment = TipoFragmentEncuestado.P817P820;
//                    fragmentActual = fragmentP817P820;
//                    moduloActual = 8;
//                    break;
//                case TipoFragmentEncuestado.P821P823:
//                    FragmentP821P823 fragmentP821P823 = new FragmentP821P823(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP821P823);
//                    tFragment = TipoFragmentEncuestado.P821P823;
//                    fragmentActual = fragmentP821P823;
//                    moduloActual = 8;
//                    break;
//                case TipoFragmentEncuestado.P047P048:
//                    FragmentP047P048 fragmentP047P048 = new FragmentP047P048(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP047P048);
//                    tFragment = TipoFragmentEncuestado.P047P048;
//                    fragmentActual = fragmentP047P048;
//                    moduloActual = 8;
//                    break;
//                case TipoFragmentEncuestado.P049P051:
//                    FragmentP049P051 fragmentP049P051 = new FragmentP049P051(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP049P051);
//                    tFragment = TipoFragmentEncuestado.P049P051;
//                    fragmentActual = fragmentP049P051;
//                    moduloActual = 3;
//                    break;
//                case TipoFragmentEncuestado.P052P053:
//                    FragmentP052P053 fragmentP052P053 = new FragmentP052P053(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP052P053);
//                    tFragment = TipoFragmentEncuestado.P052P053;
//                    fragmentActual = fragmentP052P053;
//                    moduloActual = 3;
//                    break;
//                case TipoFragmentEncuestado.P054P056:
//                    FragmentP054P056 fragmentP054P056 = new FragmentP054P056(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP054P056);
//                    tFragment = TipoFragmentEncuestado.P054P056;
//                    fragmentActual = fragmentP054P056;
//                    moduloActual = 3;
//                    break;
//                case TipoFragmentEncuestado.P057P059:
//                    FragmentP057P059 fragmentP057P059 = new FragmentP057P059(idEncuestado, EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP057P059);
//                    tFragment = TipoFragmentEncuestado.P057P059;
//                    fragmentActual = fragmentP057P059;
//                    moduloActual = 3;
//                    break;


            }
            fragmentTransaction.commit();
            return true;
        }
        return false;

    }

    public boolean setFragment_ir_800(int tipoFragment, int direccion){
        if (seteoValido(tipoFragment)){
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            if(direccion != 0){
                if(direccion > 0){
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                }else{
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                }
            }
            btnAtras.setVisibility(View.VISIBLE);
            btnSiguiente.setVisibility(View.VISIBLE);
            if(tipoFragment == TipoFragmentEncuestado.P821P823) btnSiguiente.setText("Finalizar");
            else btnSiguiente.setText("Siguiente");
            switch (tipoFragment){
//                case TipoFragmentEncuestado.P301P305:
//                    Log.e("cesar1", "onClick: "+ tFragment);
//                    btnAtras.setVisibility(View.GONE);
//                    FragmentP301P305 fragmentP301P305 = new FragmentP301P305(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP301P305);
//                    tFragment = TipoFragmentEncuestado.P301P305;
//                    fragmentActual = fragmentP301P305;
//                    moduloActual = 3;
//                    break;
//                case TipoFragmentEncuestado.P306P308:
//                    Log.e("cesar2", "onClick: "+ tFragment);
//                    FragmentP306P308 fragmentP306P308 = new FragmentP306P308(idEncuestado,EncuestaActivity.this);
//                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP306P308);
//                    tFragment = TipoFragmentEncuestado.P306P308;
//                    fragmentActual = fragmentP306P308;
//                    moduloActual = 3;
//                    break;
                case TipoFragmentEncuestado.P309:
                    Log.e("cesar3", "onClick: "+ tFragment);
                    FragmentP309 fragmentP309 = new FragmentP309(idEncuestado,idVivienda,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP309);
                    tFragment = TipoFragmentEncuestado.P309;
                    fragmentActual = fragmentP309;
                    moduloActual = 3;
                    break;
                case TipoFragmentEncuestado.P310P312:
                    FragmentP310P312 fragmentP310P312 = new FragmentP310P312(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP310P312);
                    tFragment = TipoFragmentEncuestado.P310P312;
                    fragmentActual = fragmentP310P312;
                    moduloActual = 3;
                    break;
                case TipoFragmentEncuestado.P313P317:
                    FragmentP313P317 fragmentP313P317 = new FragmentP313P317(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP313P317);
                    tFragment = TipoFragmentEncuestado.P313P317;
                    fragmentActual = fragmentP313P317;
                    moduloActual = 3;
                    break;
                case TipoFragmentEncuestado.P318:
                    FragmentP318 fragmentP318 = new FragmentP318(idEncuestado,idVivienda,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP318);
                    tFragment = TipoFragmentEncuestado.P318;
                    fragmentActual = fragmentP318;
                    moduloActual = 3;
                    break;
                case TipoFragmentEncuestado.P401P404:
                    FragmentP401P404 fragmentP401P404 = new FragmentP401P404(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP401P404);
                    tFragment = TipoFragmentEncuestado.P401P404;
                    fragmentActual = fragmentP401P404;
                    moduloActual = 4;
                    break;
                case TipoFragmentEncuestado.P405P407:
                    FragmentP405P407 fragmentP405P407 = new FragmentP405P407(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP405P407);
                    tFragment = TipoFragmentEncuestado.P405P407;
                    fragmentActual = fragmentP405P407;
                    moduloActual = 4;
                    break;
                case TipoFragmentEncuestado.P408P410:
                    FragmentP408P410 fragmentP408P410 = new FragmentP408P410(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP408P410);
                    tFragment = TipoFragmentEncuestado.P408P410;
                    fragmentActual = fragmentP408P410;
                    moduloActual = 4;
                    break;
                case TipoFragmentEncuestado.P411P416:
                    FragmentP411P416 fragmentP411P416 = new FragmentP411P416(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP411P416);
                    tFragment = TipoFragmentEncuestado.P411P416;
                    fragmentActual = fragmentP411P416;
                    moduloActual = 4;
                    break;
                case TipoFragmentEncuestado.P501P505:
                    FragmentP501P505 fragmentP501P505 = new FragmentP501P505(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP501P505);
                    tFragment = TipoFragmentEncuestado.P501P505;
                    fragmentActual = fragmentP501P505;
                    moduloActual = 5;
                    break;
                case TipoFragmentEncuestado.P506P507:
                    FragmentP506P507 fragmentP506P507 = new FragmentP506P507(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP506P507);
                    tFragment = TipoFragmentEncuestado.P506P507;
                    fragmentActual = fragmentP506P507;
                    moduloActual = 5;
                    break;
                case TipoFragmentEncuestado.P508P511:
                    FragmentP508P511 fragmentP508P511 = new FragmentP508P511(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP508P511);
                    tFragment = TipoFragmentEncuestado.P508P511;
                    fragmentActual = fragmentP508P511;
                    moduloActual = 5;
                    break;
                case TipoFragmentEncuestado.P512P513:
                    FragmentP512P513 fragmentP512P513 = new FragmentP512P513(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP512P513);
                    tFragment = TipoFragmentEncuestado.P512P513;
                    fragmentActual = fragmentP512P513;
                    moduloActual = 5;
                    break;
                case TipoFragmentEncuestado.P601P604:
                    FragmentP601P604 fragmentP601P604 = new FragmentP601P604(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP601P604);
                    tFragment = TipoFragmentEncuestado.P601P604;
                    fragmentActual = fragmentP601P604;
                    moduloActual = 6;
                    break;
                case TipoFragmentEncuestado.P605P608:
                    FragmentP605P608 fragmentP605P608 = new FragmentP605P608(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP605P608);
                    tFragment = TipoFragmentEncuestado.P605P608;
                    fragmentActual = fragmentP605P608;
                    moduloActual = 6;
                    break;
                case TipoFragmentEncuestado.P609P612:
                    FragmentP609P612 fragmentP609P612 = new FragmentP609P612(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP609P612);
                    tFragment = TipoFragmentEncuestado.P609P612;
                    fragmentActual = fragmentP609P612;
                    moduloActual = 6;
                    break;
                case TipoFragmentEncuestado.P613P618:
                    FragmentP613P617 fragmentP613P617 = new FragmentP613P617(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP613P617);
                    tFragment = TipoFragmentEncuestado.P613P618;
                    fragmentActual = fragmentP613P617;
                    moduloActual = 6;
                    break;
                case TipoFragmentEncuestado.P619P622:
                    FragmentP618P621 fragmentP618P621 = new FragmentP618P621(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP618P621);
                    tFragment = TipoFragmentEncuestado.P619P622;
                    fragmentActual = fragmentP618P621;
                    moduloActual = 6;
                    break;
                case TipoFragmentEncuestado.P623P625:
                    FragmentP622P625 fragmentP622P625 = new FragmentP622P625(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP622P625);
                    tFragment = TipoFragmentEncuestado.P623P625;
                    fragmentActual = fragmentP622P625;
                    moduloActual = 6;
                    break;
                case TipoFragmentEncuestado.P626P629:
                    FragmentP626P629 fragmentP626P629 = new FragmentP626P629(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP626P629);
                    tFragment = TipoFragmentEncuestado.P626P629;
                    fragmentActual = fragmentP626P629;
                    moduloActual = 6;
                    break;
                case TipoFragmentEncuestado.P630:
                    FragmentP630 fragmentP630 = new FragmentP630(idEncuestado,EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP630);
                    tFragment = TipoFragmentEncuestado.P630;
                    fragmentActual = fragmentP630;
                    moduloActual = 6;
                    break;
                case TipoFragmentEncuestado.P701P705:
                    FragmentP701P705 fragmentP701P705 = new FragmentP701P705(idEncuestado, EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP701P705);
                    tFragment = TipoFragmentEncuestado.P701P705;
                    fragmentActual = fragmentP701P705;
                    moduloActual = 7;
                    break;
                case TipoFragmentEncuestado.P706P709:
                    FragmentP706P709 fragmentP706P709 = new FragmentP706P709(idEncuestado, EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP706P709);
                    tFragment = TipoFragmentEncuestado.P706P709;
                    fragmentActual = fragmentP706P709;
                    moduloActual = 7;
                    break;
                case TipoFragmentEncuestado.P801P804:
                    Log.e("cesar66", "onClick: "+ tFragment);
                    FragmentP801P804 fragmentP801P804 = new FragmentP801P804(idEncuestado, EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP801P804);
                    tFragment = TipoFragmentEncuestado.P801P804;
                    fragmentActual = fragmentP801P804;
                    moduloActual = 8;
                    break;
                case TipoFragmentEncuestado.P805P808:
                    FragmentP805P808 fragmentP805P808 = new FragmentP805P808(idEncuestado, EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP805P808);
                    tFragment = TipoFragmentEncuestado.P805P808;
                    fragmentActual = fragmentP805P808;
                    moduloActual = 8;
                    break;
                case TipoFragmentEncuestado.P809P812:
                    FragmentP809P812 fragmentP809P812 = new FragmentP809P812(idEncuestado, EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP809P812);
                    tFragment = TipoFragmentEncuestado.P809P812;
                    fragmentActual = fragmentP809P812;
                    moduloActual = 8;
                    break;
                case TipoFragmentEncuestado.P813P816:
                    FragmentP813P816 fragmentP813P816 = new FragmentP813P816(idEncuestado, EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP813P816);
                    tFragment = TipoFragmentEncuestado.P813P816;
                    fragmentActual = fragmentP813P816;
                    moduloActual = 8;
                    break;
                case TipoFragmentEncuestado.P817P820:
                    FragmentP817P820 fragmentP817P820 = new FragmentP817P820(idEncuestado, EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP817P820);
                    tFragment = TipoFragmentEncuestado.P817P820;
                    fragmentActual = fragmentP817P820;
                    moduloActual = 8;
                    break;
                case TipoFragmentEncuestado.P821P823:
                    FragmentP821P823 fragmentP821P823 = new FragmentP821P823(idEncuestado, EncuestaActivity.this);
                    fragmentTransaction.replace(R.id.fragment_layout, fragmentP821P823);
                    tFragment = TipoFragmentEncuestado.P821P823;
                    fragmentActual = fragmentP821P823;
                    moduloActual = 8;
                    break;
            }
            fragmentTransaction.commit();
            return true;
        }
       return false;

    }

    public boolean seteoValido(int tipoFragment){
        boolean valido = true;
        Data data =  new Data(this);
        data.open();
        switch (tipoFragment){
            case TipoFragmentEncuestado.P001P003:
                Log.e("cesar-P301P305", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_c_p001p003,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_c_p001p003,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P004P006:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_c_p004p006,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_c_p004p006,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P007P011:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_d_p007p011,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_d_p007p011,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P012:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_d_p012,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_d_p012,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P013P014:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_d_p013p014,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_d_p013p014,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P015:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p015,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p015,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P016:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p016,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p016,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P017P020:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p017p020,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p017p020,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P021P025:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p021p025,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_e_p021p025,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P026P029:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_f_p026p029,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_f_p026p029,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P030P033:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_f_p030p033,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_f_p030p033,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P034P036:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_f_p034p036,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_f_p034p036,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P038P039:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_g_p038p039,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_g_p038p039,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P040:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_g_p040,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_g_p040,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P041P043:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_h_p041p043,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_h_p041p043,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P044P046:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_h_p044p046,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_h_p044p046,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P047P048:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p047p048,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p047p048,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P049P051:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p049p051,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p049p051,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P052P053:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p052p053,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p052p053,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P054P056:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p054p056,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p054p056,idEncuestado).equals("-1")) valido = false;
                break;
            case TipoFragmentEncuestado.P057P059:
                Log.e("cesar-P306P308", "onClick: "+ tFragment);
                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p057p059,idEncuestado).equals("0") ||
                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_i_p057p059,idEncuestado).equals("-1")) valido = false;
                break;
//            case TipoFragmentEncuestado.P301P305:
//                Log.e("cesar-P301P305", "onClick: "+ tFragment);
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p301p305,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p301p305,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P306P308:
//                Log.e("cesar-P306P308", "onClick: "+ tFragment);
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p306p308,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p306p308,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P309:
//                Log.e("cesar-P309", "onClick: "+ tFragment);
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p309,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p309,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P310P312:
//                Log.e("cesar-P310P312", "onClick: "+ tFragment);
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p310p312,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p310p312,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P313P317:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p313p317,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p313p317,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P318:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p318,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p318,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P401P404:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p401p404,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p401p404,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P405P407:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p405p407,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p405p407,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P408P410:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p408p410,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p408p410,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P411P416:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p411p416,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p411p416,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P501P505:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p501p505,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p501p505,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P506P507:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p506p507,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p506p507,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P508P511:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p508p511,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p508p511,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P512P513:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p512p513,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p512p513,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P601P604:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p601p604,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p601p604,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P605P608:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p605p608,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p605p608,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P609P612:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p609p612,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p609p612,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P613P618:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p613p617,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p613p617,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P619P622:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p618p621,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p618p621,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P623P625:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p622p625,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p622p625,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P626P629:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p626p629,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p626p629,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P630:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p630,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p630,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P701P705:
//               if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p701p705,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p701p705,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P706P709:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p706p709,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p706p709,idEncuestado).equals("-1")) valido = false;
//                break;
//           case TipoFragmentEncuestado.P801P804:
//                Log.e("cesar-P801P804", "onClick: "+ tFragment);
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p801p804,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p801p804,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P805P808:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p805p808,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p805p808,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P809P812:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p809p812,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p809p812,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P813P816:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p813p816,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p813p816,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P817P820:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p817p820,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p817p820,idEncuestado).equals("-1")) valido = false;
//                break;
//            case TipoFragmentEncuestado.P821P823:
//                if (data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p821p823,idEncuestado).equals("0") ||
//                        data.getValor(SQLConstantes.tablafragments,SQLConstantes.fragments_p821p823,idEncuestado).equals("-1")) valido = false;
//                break;
        }
        return valido;
    }

    private void enableExpandableList() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        expListView = (ExpandableListView) findViewById(R.id.left_drawer);

        prepareListData(listDataHeader, listDataChild);
        listAdapter = new ExpandListAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                switch (groupPosition){
                    case 0:
                        switch (childPosition){
                            case 0: setFragment(TipoFragmentEncuestado.P001P003,0);break;
                            case 1: setFragment(TipoFragmentEncuestado.P004P006,0);break;
//                            case 0: setFragment(TipoFragmentEncuestado.P301P305,0);break;
//                            case 1: setFragment(TipoFragmentEncuestado.P306P308,0);break;
//                            case 2: setFragment(TipoFragmentEncuestado.P309,0);break;
//                            case 3: setFragment(TipoFragmentEncuestado.P310P312,0);break;
//                            case 4: setFragment(TipoFragmentEncuestado.P313P317,0);break;
//                            case 5: setFragment(TipoFragmentEncuestado.P318,0);break;
                        }break;
                    case 1:
                        switch (childPosition){
                            case 0: setFragment(TipoFragmentEncuestado.P007P011,0);break;
                            case 1: setFragment(TipoFragmentEncuestado.P012,0);break;
                            case 2: setFragment(TipoFragmentEncuestado.P013P014,0);break;
//                            case 0: setFragment(TipoFragmentEncuestado.P401P404,0);break;
//                            case 1: setFragment(TipoFragmentEncuestado.P405P407,0);break;
//                            case 2: setFragment(TipoFragmentEncuestado.P408P410,0);break;
//                            case 3: setFragment(TipoFragmentEncuestado.P411P416,0);break;
                        }break;
                    case 2:
                        switch (childPosition){
                            case 0: setFragment(TipoFragmentEncuestado.P015,0);break;
                            case 1: setFragment(TipoFragmentEncuestado.P016,0);break;
                            case 2: setFragment(TipoFragmentEncuestado.P017P020,0);break;
                            case 3: setFragment(TipoFragmentEncuestado.P021P025,0);break;
//                            case 0: setFragment(TipoFragmentEncuestado.P501P505,0);break;
//                            case 1: setFragment(TipoFragmentEncuestado.P506P507,0);break;
//                            case 2: setFragment(TipoFragmentEncuestado.P508P511,0);break;
//                            case 3: setFragment(TipoFragmentEncuestado.P512P513,0);break;
                        }break;
                    case 3:
                        switch (childPosition){
                            case 0: setFragment(TipoFragmentEncuestado.P026P029,0);break;
                            case 1: setFragment(TipoFragmentEncuestado.P030P033,0);break;
                            case 2: setFragment(TipoFragmentEncuestado.P034P036,0);break;

//                            case 0: setFragment(TipoFragmentEncuestado.P601P604,0);break;
//                            case 1: setFragment(TipoFragmentEncuestado.P605P608,0);break;
//                            case 2: setFragment(TipoFragmentEncuestado.P609P612,0);break;
//                            case 3: setFragment(TipoFragmentEncuestado.P613P618,0);break;
//                            case 4: setFragment(TipoFragmentEncuestado.P619P622,0);break;
//                            case 5: setFragment(TipoFragmentEncuestado.P623P625,0);break;
//                            case 6: setFragment(TipoFragmentEncuestado.P626P629,0);break;
//                            case 7: setFragment(TipoFragmentEncuestado.P630,0);break;
                        }break;
                    case 4:
                        switch (childPosition){
                            case 0: setFragment(TipoFragmentEncuestado.P038P039,0);break;
                            case 1: setFragment(TipoFragmentEncuestado.P040,0);break;
//                            case 0: setFragment(TipoFragmentEncuestado.P701P705,0);break;
//                            case 1: setFragment(TipoFragmentEncuestado.P706P709,0);break;
                        }break;
                    case 5:
                        switch (childPosition){
                            case 0: setFragment(TipoFragmentEncuestado.P041P043,0);break;
                            case 1: setFragment(TipoFragmentEncuestado.P044P046,0);break;
//                            case 0: setFragment(TipoFragmentEncuestado.P801P804,0);break;
//                            case 1: setFragment(TipoFragmentEncuestado.P805P808,0);break;
//                            case 2: setFragment(TipoFragmentEncuestado.P809P812,0);break;
//                            case 3: setFragment(TipoFragmentEncuestado.P813P816,0);break;
//                            case 4: setFragment(TipoFragmentEncuestado.P817P820,0);break;
//                            case 5: setFragment(TipoFragmentEncuestado.P821P823,0);break;
                        }break;
                    case 6:
                        switch (childPosition){
                            case 0: setFragment(TipoFragmentEncuestado.P047P048,0);break;
                            case 1: setFragment(TipoFragmentEncuestado.P049P051,0);break;
                            case 2: setFragment(TipoFragmentEncuestado.P052P053,0);break;
                            case 3: setFragment(TipoFragmentEncuestado.P054P056,0);break;
                            case 4: setFragment(TipoFragmentEncuestado.P057P059,0);break;
                        }break;
                }
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }

    private void prepareListData(List<String> listDataHeader, Map<String, List<String>> listDataChild) {
        listDataHeader.add(getString(R.string.seccion_c_titulo));
        listDataHeader.add(getString(R.string.seccion_d_titulo));
        listDataHeader.add(getString(R.string.seccion_e_titulo));
        listDataHeader.add(getString(R.string.seccion_f_titulo));
        listDataHeader.add(getString(R.string.seccion_g_titulo));
        listDataHeader.add(getString(R.string.seccion_h_titulo));
        listDataHeader.add(getString(R.string.seccion_i_titulo));

        List<String> modulo3 = new ArrayList<String>();
        modulo3.add(getString(R.string.modulo_c_submenu_1));
        modulo3.add(getString(R.string.modulo_c_submenu_2));
//        modulo3.add(getString(R.string.modulo_3_submenu_3));
//        modulo3.add(getString(R.string.modulo_3_submenu_4));
//        modulo3.add(getString(R.string.modulo_3_submenu_5));
//        modulo3.add(getString(R.string.modulo_3_submenu_6));

        List<String> modulo4 = new ArrayList<String>();
        modulo4.add(getString(R.string.modulo_d_submenu_1));
        modulo4.add(getString(R.string.modulo_d_submenu_2));
        modulo4.add(getString(R.string.modulo_d_submenu_3));
//        modulo4.add(getString(R.string.modulo_4_submenu_4));

        List<String> modulo5 = new ArrayList<String>();
        modulo5.add(getString(R.string.modulo_e_submenu_1));
        modulo5.add(getString(R.string.modulo_e_submenu_2));
        modulo5.add(getString(R.string.modulo_e_submenu_3));
        modulo5.add(getString(R.string.modulo_e_submenu_4));

        List<String> modulo6 = new ArrayList<String>();
        modulo6.add(getString(R.string.modulo_f_submenu_1));
        modulo6.add(getString(R.string.modulo_f_submenu_2));
        modulo6.add(getString(R.string.modulo_f_submenu_3));
//        modulo6.add(getString(R.string.modulo_6_submenu_4));
//        modulo6.add(getString(R.string.modulo_6_submenu_5));
//        modulo6.add(getString(R.string.modulo_6_submenu_6));
//        modulo6.add(getString(R.string.modulo_6_submenu_7));
//        modulo6.add(getString(R.string.modulo_6_submenu_8));

        List<String> modulo7 = new ArrayList<String>();
        modulo7.add(getString(R.string.modulo_g_submenu_1));
        modulo7.add(getString(R.string.modulo_g_submenu_2));

        List<String> modulo8 = new ArrayList<String>();
        modulo8.add(getString(R.string.modulo_h_submenu_1));
        modulo8.add(getString(R.string.modulo_h_submenu_2));
//        modulo8.add(getString(R.string.modulo_8_submenu_3));
//        modulo8.add(getString(R.string.modulo_8_submenu_4));
//        modulo8.add(getString(R.string.modulo_8_submenu_5));
//        modulo8.add(getString(R.string.modulo_8_submenu_6));

        List<String> modulo9 = new ArrayList<String>();
        modulo9.add(getString(R.string.modulo_i_submenu_1));
        modulo9.add(getString(R.string.modulo_i_submenu_2));
        modulo9.add(getString(R.string.modulo_i_submenu_3));
        modulo9.add(getString(R.string.modulo_i_submenu_4));
        modulo9.add(getString(R.string.modulo_i_submenu_5));

        listDataChild.put(listDataHeader.get(0), modulo3);
        listDataChild.put(listDataHeader.get(1), modulo4);
        listDataChild.put(listDataHeader.get(2), modulo5);
        listDataChild.put(listDataHeader.get(3), modulo6);
        listDataChild.put(listDataHeader.get(4), modulo7);
        listDataChild.put(listDataHeader.get(5), modulo8);
        listDataChild.put(listDataHeader.get(6), modulo9);


    }

    @Override
    public void setFragmentFromOtherFragment(int tipoFragment, String idHogar, String idEncuestado) {
        this.tFragment = tipoFragment;
        this.idHogar = idHogar;
        this.idEncuestado = idEncuestado;
        setFragment(tipoFragment,1);
        btnSiguiente.setVisibility(View.VISIBLE);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.encuesta, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_volver_residentes) {
            salirActivityEncuestado();
            return true;
        }else if (id == R.id.action_registrar_observacion) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            final View dialogView = this.getLayoutInflater().inflate(R.layout.dialog_observaciones, null);
            LinearLayout lytObservaciones = dialogView.findViewById(R.id.dialog_lytObservaciones);
            final EditText edtObservaciones = dialogView.findViewById(R.id.dialog_edtObservaciones);
            edtObservaciones.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
            dialog.setView(dialogView);
            dialog.setTitle("OBSERVACIONES MODULO " + moduloActual);
            dialog.setPositiveButton("Guardar", null);
            dialog.setNegativeButton("Cancelar", null);
            final AlertDialog alertDialog = dialog.create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialogInterface) {
                    Data data = new Data(EncuestaActivity.this);
                    data.open();
                    switch (moduloActual){
                        case 3:edtObservaciones.setText(data.getValor(SQLConstantes.tablamodulo3,SQLConstantes.modulo3_obs_cap3,idEncuestado));break;
                        //   case 4:edtObservaciones.setText(data.getValor(SQLConstantes.tablamodulo4,SQLConstantes.modulo4_obs_cap4,idEncuestado));break;
                       // case 5:edtObservaciones.setText(data.getValor(SQLConstantes.tablamodulo5,SQLConstantes.modulo5_obs_cap5,idEncuestado));break;
                       // case 6:edtObservaciones.setText(data.getValor(SQLConstantes.tablamodulo6,SQLConstantes.modulo6_obs_cap6,idEncuestado));break;
                       // case 7:edtObservaciones.setText(data.getValor(SQLConstantes.tablamodulo7,SQLConstantes.modulo7_obs_cap7,idEncuestado));break;
                        // case 8:edtObservaciones.setText(data.getValor(SQLConstantes.tablamodulo8,SQLConstantes.modulo8_obs_cap8,idEncuestado));break;
                    }
                    data.close();
                    Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Data data = new Data(EncuestaActivity.this);
                            data.open();
                            switch (moduloActual){
                                case 3: data.actualizarValor(SQLConstantes.tablamodulo3,SQLConstantes.modulo3_obs_cap3,edtObservaciones.getText().toString(),idEncuestado);
                                    break;
                                // case 4: data.actualizarValor(SQLConstantes.tablamodulo4,SQLConstantes.modulo4_obs_cap4,edtObservaciones.getText().toString(),idEncuestado);
                                //  break;
                              //  case 5: data.actualizarValor(SQLConstantes.tablamodulo5,SQLConstantes.modulo5_obs_cap5,edtObservaciones.getText().toString(),idEncuestado);
                              //      break;
                               // case 6: data.actualizarValor(SQLConstantes.tablamodulo6,SQLConstantes.modulo6_obs_cap6,edtObservaciones.getText().toString(),idEncuestado);
                              //      break;
                                //     case 7: data.actualizarValor(SQLConstantes.tablamodulo7,SQLConstantes.modulo7_obs_cap7,edtObservaciones.getText().toString(),idEncuestado);
                                //       break;
                                //    case 8: data.actualizarValor(SQLConstantes.tablamodulo8,SQLConstantes.modulo8_obs_cap8,edtObservaciones.getText().toString(),idEncuestado);
                                //        break;
                            }
                            data.close();
                            alertDialog.dismiss();
                        }
                    });
                }
            });
            alertDialog.show();
            return true;
        }else if(id == R.id.action_finalizar_encuestado){
            if (verificarCobertura()){
                Data data = new Data(EncuestaActivity.this);
                data.open();
                data.actualizarValor(SQLConstantes.tablaresidentes,SQLConstantes.residentes_encuestado_cobertura,"1",idEncuestado);
                data.close();
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void salirActivityEncuestado(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Está seguro que desea salir de la encuesta y volver a la lista de los residentes del hogar?")
                .setTitle("Aviso")
                .setCancelable(false)
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("Sí",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Data data = new Data(EncuestaActivity.this);
                                data.open();
                                if (verificarCoberturaSinMensaje())
                                    data.actualizarValor(SQLConstantes.tablaresidentes,SQLConstantes.residentes_encuestado_cobertura,"1",idEncuestado);
                                data.close();
                                finish();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void salirEncuestaFinalizada(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Se terminó la encuesta para este residente. ¿Desea continuar revisando los datos o finalizar la encuesta del residente?")
                .setTitle("Aviso")
                .setCancelable(false)
                .setNegativeButton("CONTINUAR",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                tFragment = 1;
                                setFragment(tFragment,1);
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("FINALIZAR",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (verificarCobertura()){
                                    Data data = new Data(EncuestaActivity.this);
                                    data.open();
                                    data.actualizarValor(SQLConstantes.tablaresidentes,SQLConstantes.residentes_encuestado_cobertura,"1",idEncuestado);
                                    data.close();
                                    finish();
                                }
                                else {
                                    dialog.dismiss();
                                }
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public boolean verificarCobertura(){
        Data data = new Data(EncuestaActivity.this);
        data.open();
        if (data.getValor(SQLConstantes.tablamodulo3,SQLConstantes.modulo3_COB300,idEncuestado).equals("0")){
            mostrarMensaje("Falta coberturar el modulo 3 para poder finalizar");
            //tFragment = TipoFragmentEncuestado.P301P305;
            setFragment(tFragment,1);
            return false;
        }
        if (data.getValor(SQLConstantes.tablamodulo4,SQLConstantes.modulo4_COB400,idEncuestado).equals("0")){
            mostrarMensaje("Falta coberturar el modulo 4 para poder finalizar");
            tFragment = TipoFragmentEncuestado.P401P404;
            setFragment(tFragment,1);
            return false;
        }
        if (data.getValor(SQLConstantes.tablamodulo5,SQLConstantes.modulo5_COB500,idEncuestado).equals("0")){
            mostrarMensaje("Falta coberturar el modulo 5 para poder finalizar");
            tFragment = TipoFragmentEncuestado.P501P505;
            setFragment(tFragment,1);
            return false;
        }
        if (data.getValor(SQLConstantes.tablamodulo6,SQLConstantes.modulo6_COB600,idEncuestado).equals("0")){
            mostrarMensaje("Falta coberturar el modulo 6 para poder finalizar");
            tFragment = TipoFragmentEncuestado.P601P604;
            setFragment(tFragment,1);
            return false;
        }
        if (data.getValor(SQLConstantes.tablamodulo7,SQLConstantes.modulo7_COB700,idEncuestado).equals("0")){
            mostrarMensaje("Falta coberturar el modulo 7 para poder finalizar");
            tFragment = TipoFragmentEncuestado.P701P705;
            setFragment(tFragment,1);
            return false;
        }
        if (data.getValor(SQLConstantes.tablamodulo8,SQLConstantes.modulo8_COB800,idEncuestado).equals("0")){
            mostrarMensaje("Falta coberturar el modulo 8 para poder finalizar");
            tFragment = TipoFragmentEncuestado.P801P804;
            setFragment(tFragment,1);
            return false;
        }
        data.close();
        return true;
    }

    public boolean verificarCoberturaSinMensaje(){
        Data data = new Data(EncuestaActivity.this);
        data.open();
        if((data.getValor(SQLConstantes.tablamodulo3,SQLConstantes.modulo3_COB300,idEncuestado).equals("0")) ||
        (data.getValor(SQLConstantes.tablamodulo4,SQLConstantes.modulo4_COB400,idEncuestado).equals("0"))||
         (data.getValor(SQLConstantes.tablamodulo5,SQLConstantes.modulo5_COB500,idEncuestado).equals("0"))||
        (data.getValor(SQLConstantes.tablamodulo6,SQLConstantes.modulo6_COB600,idEncuestado).equals("0"))||
        (data.getValor(SQLConstantes.tablamodulo7,SQLConstantes.modulo7_COB700,idEncuestado).equals("0"))||
        (data.getValor(SQLConstantes.tablamodulo8,SQLConstantes.modulo8_COB800,idEncuestado).equals("0")))
            return false;
        data.close();
        return true;
    }
    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(EncuestaActivity.this);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


}
