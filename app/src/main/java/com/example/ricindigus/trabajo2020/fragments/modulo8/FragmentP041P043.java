package com.example.ricindigus.trabajo2020.fragments.modulo8;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo8;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InputFilterSoloLetras;
import com.example.ricindigus.trabajo2020.util.NumericKeyBoardTransformationMethod;

import java.util.ArrayList;
import java.util.List;


public class FragmentP041P043 extends FragmentPagina {
    String idEncuestado;
    Context context;
    String idInformante;
    String idHogar;
    String idVivienda;

    LinearLayout layout_h_p041;
    LinearLayout layout_h_p042_p042a;
    LinearLayout layout_h_p042b;
    LinearLayout layout_h_p043;

    CheckBox    cb_h_p041_1;
    CheckBox    cb_h_p041_2;
    CheckBox    cb_h_p041_3;
    CheckBox    cb_h_p041_4;
    RadioGroup  rg_h_p042;
    EditText    et_h_p042_o;
    EditText    et_h_p042a;
    RadioGroup  rg_h_p042b;
    EditText    et_h_p042b_o;
    RadioGroup  rg_h_p043;

    String p041_1;
    String p041_2;
    String p041_3;
    String p041_4;
    String p042;
    String p042_o;
    String p042a;
    String p042b;
    String p042b_o;
    String p043;

    List<CheckBox> listaCheck1 = new ArrayList<>();

    public FragmentP041P043() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentP041P043(String idEncuestado, Context context) {
        this.idEncuestado = idEncuestado;
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_h_p041_p043, container, false);
        cb_h_p041_1  = rootView.findViewById(R.id.cb_h_P041_1);
        cb_h_p041_2  = rootView.findViewById(R.id.cb_h_P041_2);
        cb_h_p041_3  = rootView.findViewById(R.id.cb_h_P041_3);
        cb_h_p041_4  = rootView.findViewById(R.id.cb_h_P041_4);
        rg_h_p042    = rootView.findViewById(R.id.rg_h_P042);
        et_h_p042_o  = rootView.findViewById(R.id.et_h_P042_O);
        et_h_p042a   = rootView.findViewById(R.id.et_h_p042A);
        rg_h_p042b   = rootView.findViewById(R.id.rg_h_P042B);
        et_h_p042b_o = rootView.findViewById(R.id.et_h_P042B_O);
        rg_h_p043    = rootView.findViewById(R.id.rg_h_P043);

        listaCheck1.add(cb_h_p041_1);
        listaCheck1.add(cb_h_p041_2);
        listaCheck1.add(cb_h_p041_3);
        listaCheck1.add(cb_h_p041_4);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rg_h_p042.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                controlarEspecifiqueRadio(radioGroup,i,5,et_h_p042_o);
            }
        });
        rg_h_p042b.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                controlarEspecifiqueRadio(radioGroup,i,5,et_h_p042b_o);
            }
        });

        llenarVista();
        cargarDatos();
    }



    @Override
    public void guardarDatos() {
        Data data = new Data(context);
        data.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo8_id_informante,idInformante);
        contentValues.put(SQLConstantes.seccion_h_p341_1,p041_1);
        contentValues.put(SQLConstantes.seccion_h_p341_2,p041_2);
        contentValues.put(SQLConstantes.seccion_h_p341_3,p041_3);
        contentValues.put(SQLConstantes.seccion_h_p341_4,p041_4);
        contentValues.put(SQLConstantes.seccion_h_p342,p042);
        contentValues.put(SQLConstantes.seccion_h_p342_o,p042_o);
        contentValues.put(SQLConstantes.seccion_h_p342a,p042a);
        contentValues.put(SQLConstantes.seccion_h_p342b,p042b);
        contentValues.put(SQLConstantes.seccion_h_p342b_o,p042b_o);
        contentValues.put(SQLConstantes.seccion_h_p343,p043);

        if(!data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo8 modulo8 = new Modulo8();
            modulo8.set_id(idEncuestado);
            modulo8.setIdHogar(idHogar);
            modulo8.setIdVivienda(idVivienda);
            data.insertarElemento(getNombreTabla(),modulo8.toValues());
        }
        data.actualizarElemento(getNombreTabla(),contentValues,idEncuestado);
        data.close();
    }

    @Override
    public void llenarVariables() {
        if (cb_h_p041_1.isChecked()) p041_1= "1"; else p041_1="0";
        if (cb_h_p041_2.isChecked()) p041_2= "1"; else p041_2="0";
        if (cb_h_p041_3.isChecked()) p041_3= "1"; else p041_3="0";
        if (cb_h_p041_4.isChecked()) p041_4= "1"; else p041_4="0";
        p042    = rg_h_p042.indexOfChild(rg_h_p042.findViewById(rg_h_p042.getCheckedRadioButtonId()))+"";
        p042_o  = et_h_p042_o.getText().toString();
        p042a   = et_h_p042a.getText().toString();
        p042b   = rg_h_p042b.indexOfChild(rg_h_p042b.findViewById(rg_h_p042b.getCheckedRadioButtonId()))+"";
        p042b_o = et_h_p042b_o.getText().toString();
        p043    = rg_h_p043.indexOfChild(rg_h_p043.findViewById(rg_h_p043.getCheckedRadioButtonId()))+"";
    }

    @Override
    public void cargarDatos() {
        Data data = new Data(context);
        data.open();
        if (data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo8 modulo8 = data.getModulo8(idEncuestado);
            if (modulo8.getP341_1().equals("1")) cb_h_p041_1.setChecked(true);
            if (modulo8.getP341_2().equals("1")) cb_h_p041_2.setChecked(true);
            if (modulo8.getP341_3().equals("1")) cb_h_p041_3.setChecked(true);
            if (modulo8.getP341_4().equals("1")) cb_h_p041_4.setChecked(true);
            if(!modulo8.getP342().equals("-1") && !modulo8.getP342().equals(""))((RadioButton)rg_h_p042.getChildAt(Integer.parseInt(modulo8.getP342()))).setChecked(true);
            et_h_p042_o.setText(modulo8.getP342_o());
            et_h_p042a.setText(modulo8.getP342a());
            if(!modulo8.getP342b().equals("-1") && !modulo8.getP342b().equals(""))((RadioButton)rg_h_p042b.getChildAt(Integer.parseInt(modulo8.getP342b()))).setChecked(true);
            et_h_p042b_o.setText(modulo8.getP342b_o());
            if(!modulo8.getP343().equals("-1") && !modulo8.getP343().equals(""))((RadioButton)rg_h_p043.getChildAt(Integer.parseInt(modulo8.getP343()))).setChecked(true);
        }
        data.close();
    }

    @Override
    public void llenarVista() {

    }

    @Override
    public boolean validarDatos() {
        //P041
        if(validarSeleccionListCheckbox(listaCheck1)){
            mostrarMensaje("PREGUNTA 41: DEBE MARCAR UNA OPCIONx");
            return false;
        }
        //P042
        if (rg_h_p042.equals("-1")){mostrarMensaje("PREGUNTA 42: DEBE MARCAR UNA OPCIÓN"); return false;}
        llenarVariables();
        return true;
    }

    @Override
    public String getNombreTabla() {
        return SQLConstantes.tablamodulo8;
    }

    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void configurarEditText(final EditText editText, final View view, int tipo,int longitud){
        switch (tipo){
            case 0:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud), new InputFilterSoloLetras()});break;
            case 1:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud)});break;
            case 2:editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)});
                editText.setTransformationMethod(new NumericKeyBoardTransformationMethod());break;
        }

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ocultarTeclado(editText);
                    view.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void controlarEspecifiqueRadio(RadioGroup group, int checkedId, int opcionEsp, EditText editTextEspecifique) {
        int seleccionado = group.indexOfChild(group.findViewById(checkedId));
        if(seleccionado == opcionEsp){
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_enabled);
            editTextEspecifique.setEnabled(true);
        }else{
            editTextEspecifique.setText("");
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_disabled);
            editTextEspecifique.setEnabled(false);
        }
    }

    public void ocultarOtrosLayouts(){

    }

    public boolean verificarCoberturaCapitulo(){

        return true;
    }

    /*METODOS FLUJOS Y REGLAS*/
    public final boolean validarSeleccionListCheckbox(@Nullable List<CheckBox> lista) {
        boolean estado = true;
        if (lista != null) {
            for (CheckBox check:lista) {
                if (check.isChecked()){
                    estado = false;
                }
            }
        }
        return estado;
    }
}
