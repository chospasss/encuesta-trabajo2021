package com.example.ricindigus.trabajo2020.modelo.pojos;

import android.content.ContentValues;

import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;

public class Modulo7 {
    private String _id;
    private String idInformante;
    private String idHogar;
    private String idVivienda;
    /*
    private String c7_p701;
    private String c7_p702_1;
    private String c7_p702_2;
    private String c7_p702_3;
    private String c7_p702_4;
    private String c7_p702_5;
    private String c7_p702_6;
    private String c7_p702_7;
    private String c7_p702_8;
    private String c7_p702_9;
    private String c7_p702_10;
    private String c7_p702_o;
    private String c7_p703;
    private String c7_p704_1;
    private String c7_p704_2;
    private String c7_p704_3;
    private String c7_p704_4;
    private String c7_p704_5;
    private String c7_p704_6;
    private String c7_p704_o;
    private String c7_p705_1;
    private String c7_p705_2;
    private String c7_p705_3;
    private String c7_p705_4;
    private String c7_p705_5;
    private String c7_p705_6;
    private String c7_p705_7;
    private String c7_p705_o;
    private String c7_p706;
    private String c7_p707_1;
    private String c7_p707_2;
    private String c7_p707_3;
    private String c7_p707_4;
    private String c7_p707_5;
    private String c7_p707_6;
    private String c7_p707_7;
    private String c7_p707_8;
    private String c7_p707_9;
    private String c7_p707_o;
    private String c7_p708_1;
    private String c7_p708_2;
    private String c7_p708_3;
    private String c7_p708_4;
    private String c7_p708_5;
    private String c7_p709_1;
    private String c7_p709_2;
    private String c7_p709_3;
    private String c7_p709_4;
    private String c7_p709_5;
    private String c7_p709_6;
    private String c7_p709_7;
    private String c7_p709_8;
    private String c7_p709_9;
    private String c7_p709_10;
    private String c7_p709_o;
    private String obs_cap7;
     */
    private String COB700;
    private String inf_300;
    private String p338_1;
    private String p339_1;
    private String p338_2;
    private String p339_2;
    private String p338_3;
    private String p339_3;
    private String p338_4;
    private String p339_4;
    private String p338_5;
    private String p339_5;
    private String p338_6;
    private String p339_6;
    private String p338_7;
    private String p339_7;
    private String p338_8;
    private String p339_8;
    private String p338_9;
    private String p339_9;
    private String p338_10;
    private String p339_10;
    private String p340_1;
    private String p340_2;
    private String p340_3;
    private String p340_4;
    private String p340_5;
    private String p340_6;
    private String p340_7;
    private String p340_8;
    private String p340_9;
    private String p340_10;
    private String p340_11;
    private String p340_12;
    private String p340_13;
    private String p340_13_o;
    private String obs_g;

    public Modulo7() {
        _id = "";
        idHogar = "";
        idVivienda = "";
        idInformante= "";
        /*
        c7_p701= "";
        c7_p702_1= "";
        c7_p702_2= "";
        c7_p702_3= "";
        c7_p702_4= "";
        c7_p702_5= "";
        c7_p702_6= "";
        c7_p702_7= "";
        c7_p702_8= "";
        c7_p702_9= "";
        c7_p702_10= "";
        c7_p702_o= "";
        c7_p703= "";
        c7_p704_1= "";
        c7_p704_2= "";
        c7_p704_3= "";
        c7_p704_4= "";
        c7_p704_5= "";
        c7_p704_6= "";
        c7_p704_o= "";
        c7_p705_1= "";
        c7_p705_2= "";
        c7_p705_3= "";
        c7_p705_4= "";
        c7_p705_5= "";
        c7_p705_6= "";
        c7_p705_7= "";
        c7_p705_o= "";
        c7_p706= "";
        c7_p707_1= "";
        c7_p707_2= "";
        c7_p707_3= "";
        c7_p707_4= "";
        c7_p707_5= "";
        c7_p707_6= "";
        c7_p707_7= "";
        c7_p707_8= "";
        c7_p707_9= "";
        c7_p707_o= "";
        c7_p708_1= "";
        c7_p708_2= "";
        c7_p708_3= "";
        c7_p708_4= "";
        c7_p708_5= "";
        c7_p709_1= "";
        c7_p709_2= "";
        c7_p709_3= "";
        c7_p709_4= "";
        c7_p709_5= "";
        c7_p709_6= "";
        c7_p709_7= "";
        c7_p709_8= "";
        c7_p709_9= "";
        c7_p709_10= "";
        c7_p709_o= "";
        obs_cap7= "";
         */
        COB700= "0";
          inf_300="";
          p338_1="";
          p339_1="";
          p338_2="";
          p339_2="";
          p338_3="";
          p339_3="";
          p338_4="";
          p339_4="";
          p338_5="";
          p339_5="";
          p338_6="";
          p339_6="";
          p338_7="";
          p339_7="";
          p338_8="";
          p339_8="";
          p338_9="";
          p339_9="";
          p338_10="";
          p339_10="";
          p340_1="";
          p340_2="";
          p340_3="";
          p340_4="";
          p340_5="";
          p340_6="";
          p340_7="";
          p340_8="";
          p340_9="";
          p340_10="";
          p340_11="";
          p340_12="";
          p340_13="";
          p340_13_o="";
          obs_g="";

    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getIdInformante() {
        return idInformante;
    }

    public void setIdInformante(String idInformante) {
        this.idInformante = idInformante;
    }

    public String getIdHogar() {
        return idHogar;
    }

    public void setIdHogar(String idHogar) {
        this.idHogar = idHogar;
    }

    public String getIdVivienda() {
        return idVivienda;
    }

    public void setIdVivienda(String idVivienda) {
        this.idVivienda = idVivienda;
    }

    /*
     public String getC7_p701() {
        return c7_p701;
    }

    public void setC7_p701(String c7_p701) {
        this.c7_p701 = c7_p701;
    }

    public String getC7_p702_1() {
        return c7_p702_1;
    }

    public void setC7_p702_1(String c7_p702_1) {
        this.c7_p702_1 = c7_p702_1;
    }

    public String getC7_p702_2() {
        return c7_p702_2;
    }

    public void setC7_p702_2(String c7_p702_2) {
        this.c7_p702_2 = c7_p702_2;
    }

    public String getC7_p702_3() {
        return c7_p702_3;
    }

    public void setC7_p702_3(String c7_p702_3) {
        this.c7_p702_3 = c7_p702_3;
    }

    public String getC7_p702_4() {
        return c7_p702_4;
    }

    public void setC7_p702_4(String c7_p702_4) {
        this.c7_p702_4 = c7_p702_4;
    }

    public String getC7_p702_5() {
        return c7_p702_5;
    }

    public void setC7_p702_5(String c7_p702_5) {
        this.c7_p702_5 = c7_p702_5;
    }

    public String getC7_p702_6() {
        return c7_p702_6;
    }

    public void setC7_p702_6(String c7_p702_6) {
        this.c7_p702_6 = c7_p702_6;
    }

    public String getC7_p702_7() {
        return c7_p702_7;
    }

    public void setC7_p702_7(String c7_p702_7) {
        this.c7_p702_7 = c7_p702_7;
    }

    public String getC7_p702_8() {
        return c7_p702_8;
    }

    public void setC7_p702_8(String c7_p702_8) {
        this.c7_p702_8 = c7_p702_8;
    }

    public String getC7_p702_9() {
        return c7_p702_9;
    }

    public void setC7_p702_9(String c7_p702_9) {
        this.c7_p702_9 = c7_p702_9;
    }

    public String getC7_p702_10() {
        return c7_p702_10;
    }

    public void setC7_p702_10(String c7_p702_10) {
        this.c7_p702_10 = c7_p702_10;
    }

    public String getC7_p702_o() {
        return c7_p702_o;
    }

    public void setC7_p702_o(String c7_p702_o) {
        this.c7_p702_o = c7_p702_o;
    }

    public String getC7_p703() {
        return c7_p703;
    }

    public void setC7_p703(String c7_p703) {
        this.c7_p703 = c7_p703;
    }

    public String getC7_p704_1() {
        return c7_p704_1;
    }

    public void setC7_p704_1(String c7_p704_1) {
        this.c7_p704_1 = c7_p704_1;
    }

    public String getC7_p704_2() {
        return c7_p704_2;
    }

    public void setC7_p704_2(String c7_p704_2) {
        this.c7_p704_2 = c7_p704_2;
    }

    public String getC7_p704_3() {
        return c7_p704_3;
    }

    public void setC7_p704_3(String c7_p704_3) {
        this.c7_p704_3 = c7_p704_3;
    }

    public String getC7_p704_4() {
        return c7_p704_4;
    }

    public void setC7_p704_4(String c7_p704_4) {
        this.c7_p704_4 = c7_p704_4;
    }

    public String getC7_p704_5() {
        return c7_p704_5;
    }

    public void setC7_p704_5(String c7_p704_5) {
        this.c7_p704_5 = c7_p704_5;
    }

    public String getC7_p704_6() {
        return c7_p704_6;
    }

    public void setC7_p704_6(String c7_p704_6) {
        this.c7_p704_6 = c7_p704_6;
    }

    public String getC7_p704_o() {
        return c7_p704_o;
    }

    public void setC7_p704_o(String c7_p704_o) {
        this.c7_p704_o = c7_p704_o;
    }

    public String getC7_p705_1() {
        return c7_p705_1;
    }

    public void setC7_p705_1(String c7_p705_1) {
        this.c7_p705_1 = c7_p705_1;
    }

    public String getC7_p705_2() {
        return c7_p705_2;
    }

    public void setC7_p705_2(String c7_p705_2) {
        this.c7_p705_2 = c7_p705_2;
    }

    public String getC7_p705_3() {
        return c7_p705_3;
    }

    public void setC7_p705_3(String c7_p705_3) {
        this.c7_p705_3 = c7_p705_3;
    }

    public String getC7_p705_4() {
        return c7_p705_4;
    }

    public void setC7_p705_4(String c7_p705_4) {
        this.c7_p705_4 = c7_p705_4;
    }

    public String getC7_p705_5() {
        return c7_p705_5;
    }

    public void setC7_p705_5(String c7_p705_5) {
        this.c7_p705_5 = c7_p705_5;
    }

    public String getC7_p705_6() {
        return c7_p705_6;
    }

    public void setC7_p705_6(String c7_p705_6) {
        this.c7_p705_6 = c7_p705_6;
    }

    public String getC7_p705_7() {
        return c7_p705_7;
    }

    public void setC7_p705_7(String c7_p705_7) {
        this.c7_p705_7 = c7_p705_7;
    }

    public String getC7_p705_o() {
        return c7_p705_o;
    }

    public void setC7_p705_o(String c7_p705_o) {
        this.c7_p705_o = c7_p705_o;
    }

    public String getC7_p706() {
        return c7_p706;
    }

    public void setC7_p706(String c7_p706) {
        this.c7_p706 = c7_p706;
    }

    public String getC7_p707_1() {
        return c7_p707_1;
    }

    public void setC7_p707_1(String c7_p707_1) {
        this.c7_p707_1 = c7_p707_1;
    }

    public String getC7_p707_2() {
        return c7_p707_2;
    }

    public void setC7_p707_2(String c7_p707_2) {
        this.c7_p707_2 = c7_p707_2;
    }

    public String getC7_p707_3() {
        return c7_p707_3;
    }

    public void setC7_p707_3(String c7_p707_3) {
        this.c7_p707_3 = c7_p707_3;
    }

    public String getC7_p707_4() {
        return c7_p707_4;
    }

    public void setC7_p707_4(String c7_p707_4) {
        this.c7_p707_4 = c7_p707_4;
    }

    public String getC7_p707_5() {
        return c7_p707_5;
    }

    public void setC7_p707_5(String c7_p707_5) {
        this.c7_p707_5 = c7_p707_5;
    }

    public String getC7_p707_6() {
        return c7_p707_6;
    }

    public void setC7_p707_6(String c7_p707_6) {
        this.c7_p707_6 = c7_p707_6;
    }

    public String getC7_p707_7() {
        return c7_p707_7;
    }

    public void setC7_p707_7(String c7_p707_7) {
        this.c7_p707_7 = c7_p707_7;
    }

    public String getC7_p707_8() {
        return c7_p707_8;
    }

    public void setC7_p707_8(String c7_p707_8) {
        this.c7_p707_8 = c7_p707_8;
    }

    public String getC7_p707_9() {
        return c7_p707_9;
    }

    public void setC7_p707_9(String c7_p707_9) {
        this.c7_p707_9 = c7_p707_9;
    }

    public String getC7_p707_o() {
        return c7_p707_o;
    }

    public void setC7_p707_o(String c7_p707_o) {
        this.c7_p707_o = c7_p707_o;
    }

    public String getC7_p708_1() {
        return c7_p708_1;
    }

    public void setC7_p708_1(String c7_p708_1) {
        this.c7_p708_1 = c7_p708_1;
    }

    public String getC7_p708_2() {
        return c7_p708_2;
    }

    public void setC7_p708_2(String c7_p708_2) {
        this.c7_p708_2 = c7_p708_2;
    }

    public String getC7_p708_3() {
        return c7_p708_3;
    }

    public void setC7_p708_3(String c7_p708_3) {
        this.c7_p708_3 = c7_p708_3;
    }

    public String getC7_p708_4() {
        return c7_p708_4;
    }

    public void setC7_p708_4(String c7_p708_4) {
        this.c7_p708_4 = c7_p708_4;
    }

    public String getC7_p708_5() {
        return c7_p708_5;
    }

    public void setC7_p708_5(String c7_p708_5) {
        this.c7_p708_5 = c7_p708_5;
    }

    public String getC7_p709_1() {
        return c7_p709_1;
    }

    public void setC7_p709_1(String c7_p709_1) {
        this.c7_p709_1 = c7_p709_1;
    }

    public String getC7_p709_2() {
        return c7_p709_2;
    }

    public void setC7_p709_2(String c7_p709_2) {
        this.c7_p709_2 = c7_p709_2;
    }

    public String getC7_p709_3() {
        return c7_p709_3;
    }

    public void setC7_p709_3(String c7_p709_3) {
        this.c7_p709_3 = c7_p709_3;
    }

    public String getC7_p709_4() {
        return c7_p709_4;
    }

    public void setC7_p709_4(String c7_p709_4) {
        this.c7_p709_4 = c7_p709_4;
    }

    public String getC7_p709_5() {
        return c7_p709_5;
    }

    public void setC7_p709_5(String c7_p709_5) {
        this.c7_p709_5 = c7_p709_5;
    }

    public String getC7_p709_6() {
        return c7_p709_6;
    }

    public void setC7_p709_6(String c7_p709_6) {
        this.c7_p709_6 = c7_p709_6;
    }

    public String getC7_p709_7() {
        return c7_p709_7;
    }

    public void setC7_p709_7(String c7_p709_7) {
        this.c7_p709_7 = c7_p709_7;
    }

    public String getC7_p709_8() {
        return c7_p709_8;
    }

    public void setC7_p709_8(String c7_p709_8) {
        this.c7_p709_8 = c7_p709_8;
    }

    public String getC7_p709_9() {
        return c7_p709_9;
    }

    public void setC7_p709_9(String c7_p709_9) {
        this.c7_p709_9 = c7_p709_9;
    }

    public String getC7_p709_10() {
        return c7_p709_10;
    }

    public void setC7_p709_10(String c7_p709_10) {
        this.c7_p709_10 = c7_p709_10;
    }

    public String getC7_p709_o() {
        return c7_p709_o;
    }

    public void setC7_p709_o(String c7_p709_o) {
        this.c7_p709_o = c7_p709_o;
    }

    public String getObs_cap7() {
        return obs_cap7;
    }

    public void setObs_cap7(String obs_cap7) {
        this.obs_cap7 = obs_cap7;
    }
     */

    public String getInf_300() {
        return inf_300;
    }

    public void setInf_300(String inf_300) {
        this.inf_300 = inf_300;
    }

    public String getP338_1() {
        return p338_1;
    }

    public void setP338_1(String p338_1) {
        this.p338_1 = p338_1;
    }

    public String getP339_1() {
        return p339_1;
    }

    public void setP339_1(String p339_1) {
        this.p339_1 = p339_1;
    }

    public String getP338_2() {
        return p338_2;
    }

    public void setP338_2(String p338_2) {
        this.p338_2 = p338_2;
    }

    public String getP339_2() {
        return p339_2;
    }

    public void setP339_2(String p339_2) {
        this.p339_2 = p339_2;
    }

    public String getP338_3() {
        return p338_3;
    }

    public void setP338_3(String p338_3) {
        this.p338_3 = p338_3;
    }

    public String getP339_3() {
        return p339_3;
    }

    public void setP339_3(String p339_3) {
        this.p339_3 = p339_3;
    }

    public String getP338_4() {
        return p338_4;
    }

    public void setP338_4(String p338_4) {
        this.p338_4 = p338_4;
    }

    public String getP339_4() {
        return p339_4;
    }

    public void setP339_4(String p339_4) {
        this.p339_4 = p339_4;
    }

    public String getP338_5() {
        return p338_5;
    }

    public void setP338_5(String p338_5) {
        this.p338_5 = p338_5;
    }

    public String getP339_5() {
        return p339_5;
    }

    public void setP339_5(String p339_5) {
        this.p339_5 = p339_5;
    }

    public String getP338_6() {
        return p338_6;
    }

    public void setP338_6(String p338_6) {
        this.p338_6 = p338_6;
    }

    public String getP339_6() {
        return p339_6;
    }

    public void setP339_6(String p339_6) {
        this.p339_6 = p339_6;
    }

    public String getP338_7() {
        return p338_7;
    }

    public void setP338_7(String p338_7) {
        this.p338_7 = p338_7;
    }

    public String getP339_7() {
        return p339_7;
    }

    public void setP339_7(String p339_7) {
        this.p339_7 = p339_7;
    }

    public String getP338_8() {
        return p338_8;
    }

    public void setP338_8(String p338_8) {
        this.p338_8 = p338_8;
    }

    public String getP339_8() {
        return p339_8;
    }

    public void setP339_8(String p339_8) {
        this.p339_8 = p339_8;
    }

    public String getP338_9() {
        return p338_9;
    }

    public void setP338_9(String p338_9) {
        this.p338_9 = p338_9;
    }

    public String getP339_9() {
        return p339_9;
    }

    public void setP339_9(String p339_9) {
        this.p339_9 = p339_9;
    }

    public String getP338_10() {
        return p338_10;
    }

    public void setP338_10(String p338_10) {
        this.p338_10 = p338_10;
    }

    public String getP339_10() {
        return p339_10;
    }

    public void setP339_10(String p339_10) {
        this.p339_10 = p339_10;
    }

    public String getP340_1() {
        return p340_1;
    }

    public void setP340_1(String p340_1) {
        this.p340_1 = p340_1;
    }

    public String getP340_2() {
        return p340_2;
    }

    public void setP340_2(String p340_2) {
        this.p340_2 = p340_2;
    }

    public String getP340_3() {
        return p340_3;
    }

    public void setP340_3(String p340_3) {
        this.p340_3 = p340_3;
    }

    public String getP340_4() {
        return p340_4;
    }

    public void setP340_4(String p340_4) {
        this.p340_4 = p340_4;
    }

    public String getP340_5() {
        return p340_5;
    }

    public void setP340_5(String p340_5) {
        this.p340_5 = p340_5;
    }

    public String getP340_6() {
        return p340_6;
    }

    public void setP340_6(String p340_6) {
        this.p340_6 = p340_6;
    }

    public String getP340_7() {
        return p340_7;
    }

    public void setP340_7(String p340_7) {
        this.p340_7 = p340_7;
    }

    public String getP340_8() {
        return p340_8;
    }

    public void setP340_8(String p340_8) {
        this.p340_8 = p340_8;
    }

    public String getP340_9() {
        return p340_9;
    }

    public void setP340_9(String p340_9) {
        this.p340_9 = p340_9;
    }

    public String getP340_10() {
        return p340_10;
    }

    public void setP340_10(String p340_10) {
        this.p340_10 = p340_10;
    }

    public String getP340_11() {
        return p340_11;
    }

    public void setP340_11(String p340_11) {
        this.p340_11 = p340_11;
    }

    public String getP340_12() {
        return p340_12;
    }

    public void setP340_12(String p340_12) {
        this.p340_12 = p340_12;
    }

    public String getP340_13() {
        return p340_13;
    }

    public void setP340_13(String p340_13) {
        this.p340_13 = p340_13;
    }

    public String getP340_13_o() {
        return p340_13_o;
    }

    public void setP340_13_o(String p340_13_o) {
        this.p340_13_o = p340_13_o;
    }

    public String getObs_g() {
        return obs_g;
    }

    public void setObs_g(String obs_g) {
        this.obs_g = obs_g;
    }

    public String getCOB700() {
        return COB700;
    }

    public void setCOB700(String COB700) {
        this.COB700 = COB700;
    }

    public ContentValues toValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo7_id,_id);
        contentValues.put(SQLConstantes.modulo7_id_informante,idInformante);
        contentValues.put(SQLConstantes.modulo7_id_hogar,idHogar);
        contentValues.put(SQLConstantes.modulo7_id_vivienda,idVivienda);
        /*
        contentValues.put(SQLConstantes.modulo7_c7_p701,c7_p701);
        contentValues.put(SQLConstantes.modulo7_c7_p702_1,c7_p702_1);
        contentValues.put(SQLConstantes.modulo7_c7_p702_2,c7_p702_2);
        contentValues.put(SQLConstantes.modulo7_c7_p702_3,c7_p702_3);
        contentValues.put(SQLConstantes.modulo7_c7_p702_4,c7_p702_4);
        contentValues.put(SQLConstantes.modulo7_c7_p702_5,c7_p702_5);
        contentValues.put(SQLConstantes.modulo7_c7_p702_6,c7_p702_6);
        contentValues.put(SQLConstantes.modulo7_c7_p702_7,c7_p702_7);
        contentValues.put(SQLConstantes.modulo7_c7_p702_8,c7_p702_8);
        contentValues.put(SQLConstantes.modulo7_c7_p702_9,c7_p702_9);
        contentValues.put(SQLConstantes.modulo7_c7_p702_10,c7_p702_10);
        contentValues.put(SQLConstantes.modulo7_c7_p702_o,c7_p702_o);
        contentValues.put(SQLConstantes.modulo7_c7_p703,c7_p703);
        contentValues.put(SQLConstantes.modulo7_c7_p704_1,c7_p704_1);
        contentValues.put(SQLConstantes.modulo7_c7_p704_2,c7_p704_2);
        contentValues.put(SQLConstantes.modulo7_c7_p704_3,c7_p704_3);
        contentValues.put(SQLConstantes.modulo7_c7_p704_4,c7_p704_4);
        contentValues.put(SQLConstantes.modulo7_c7_p704_5,c7_p704_5);
        contentValues.put(SQLConstantes.modulo7_c7_p704_6,c7_p704_6);
        contentValues.put(SQLConstantes.modulo7_c7_p704_o,c7_p704_o);
        contentValues.put(SQLConstantes.modulo7_c7_p705_1,c7_p705_1);
        contentValues.put(SQLConstantes.modulo7_c7_p705_2,c7_p705_2);
        contentValues.put(SQLConstantes.modulo7_c7_p705_3,c7_p705_3);
        contentValues.put(SQLConstantes.modulo7_c7_p705_4,c7_p705_4);
        contentValues.put(SQLConstantes.modulo7_c7_p705_5,c7_p705_5);
        contentValues.put(SQLConstantes.modulo7_c7_p705_6,c7_p705_6);
        contentValues.put(SQLConstantes.modulo7_c7_p705_7,c7_p705_7);
        contentValues.put(SQLConstantes.modulo7_c7_p705_o,c7_p705_o);
        contentValues.put(SQLConstantes.modulo7_c7_p706,c7_p706);
        contentValues.put(SQLConstantes.modulo7_c7_p707_1,c7_p707_1);
        contentValues.put(SQLConstantes.modulo7_c7_p707_2,c7_p707_2);
        contentValues.put(SQLConstantes.modulo7_c7_p707_3,c7_p707_3);
        contentValues.put(SQLConstantes.modulo7_c7_p707_4,c7_p707_4);
        contentValues.put(SQLConstantes.modulo7_c7_p707_5,c7_p707_5);
        contentValues.put(SQLConstantes.modulo7_c7_p707_6,c7_p707_6);
        contentValues.put(SQLConstantes.modulo7_c7_p707_7,c7_p707_7);
        contentValues.put(SQLConstantes.modulo7_c7_p707_8,c7_p707_8);
        contentValues.put(SQLConstantes.modulo7_c7_p707_9,c7_p707_9);
        contentValues.put(SQLConstantes.modulo7_c7_p707_o,c7_p707_o);
        contentValues.put(SQLConstantes.modulo7_c7_p708_1,c7_p708_1);
        contentValues.put(SQLConstantes.modulo7_c7_p708_2,c7_p708_2);
        contentValues.put(SQLConstantes.modulo7_c7_p708_3,c7_p708_3);
        contentValues.put(SQLConstantes.modulo7_c7_p708_4,c7_p708_4);
        contentValues.put(SQLConstantes.modulo7_c7_p708_5,c7_p708_5);
        contentValues.put(SQLConstantes.modulo7_c7_p709_1,c7_p709_1);
        contentValues.put(SQLConstantes.modulo7_c7_p709_2,c7_p709_2);
        contentValues.put(SQLConstantes.modulo7_c7_p709_3,c7_p709_3);
        contentValues.put(SQLConstantes.modulo7_c7_p709_4,c7_p709_4);
        contentValues.put(SQLConstantes.modulo7_c7_p709_5,c7_p709_5);
        contentValues.put(SQLConstantes.modulo7_c7_p709_6,c7_p709_6);
        contentValues.put(SQLConstantes.modulo7_c7_p709_7,c7_p709_7);
        contentValues.put(SQLConstantes.modulo7_c7_p709_8,c7_p709_8);
        contentValues.put(SQLConstantes.modulo7_c7_p709_9,c7_p709_9);
        contentValues.put(SQLConstantes.modulo7_c7_p709_10,c7_p709_10);
        contentValues.put(SQLConstantes.modulo7_c7_p709_o,c7_p709_o);
        contentValues.put(SQLConstantes.modulo7_obs_cap7,obs_cap7);
         */
        contentValues.put(SQLConstantes.seccion_g_inf_300,inf_300);
        contentValues.put(SQLConstantes.seccion_g_p338_1,p338_1);
        contentValues.put(SQLConstantes.seccion_g_p339_1,p339_1);
        contentValues.put(SQLConstantes.seccion_g_p338_2,p338_2);
        contentValues.put(SQLConstantes.seccion_g_p339_2,p339_2);
        contentValues.put(SQLConstantes.seccion_g_p338_3,p338_3);
        contentValues.put(SQLConstantes.seccion_g_p339_3,p339_3);
        contentValues.put(SQLConstantes.seccion_g_p338_4,p338_4);
        contentValues.put(SQLConstantes.seccion_g_p339_4,p339_4);
        contentValues.put(SQLConstantes.seccion_g_p338_5,p338_5);
        contentValues.put(SQLConstantes.seccion_g_p339_5,p339_5);
        contentValues.put(SQLConstantes.seccion_g_p338_6,p338_6);
        contentValues.put(SQLConstantes.seccion_g_p339_6,p339_6);
        contentValues.put(SQLConstantes.seccion_g_p338_7,p338_7);
        contentValues.put(SQLConstantes.seccion_g_p339_7,p339_7);
        contentValues.put(SQLConstantes.seccion_g_p338_8,p338_8);
        contentValues.put(SQLConstantes.seccion_g_p339_8,p339_8);
        contentValues.put(SQLConstantes.seccion_g_p338_9,p338_9);
        contentValues.put(SQLConstantes.seccion_g_p339_9,p339_9);
        contentValues.put(SQLConstantes.seccion_g_p338_10,p338_10);
        contentValues.put(SQLConstantes.seccion_g_p339_10,p339_10);
        contentValues.put(SQLConstantes.seccion_g_p340_1,p340_1);
        contentValues.put(SQLConstantes.seccion_g_p340_2,p340_2);
        contentValues.put(SQLConstantes.seccion_g_p340_3,p340_3);
        contentValues.put(SQLConstantes.seccion_g_p340_4,p340_4);
        contentValues.put(SQLConstantes.seccion_g_p340_5,p340_5);
        contentValues.put(SQLConstantes.seccion_g_p340_6,p340_6);
        contentValues.put(SQLConstantes.seccion_g_p340_7,p340_7);
        contentValues.put(SQLConstantes.seccion_g_p340_8,p340_8);
        contentValues.put(SQLConstantes.seccion_g_p340_9,p340_9);
        contentValues.put(SQLConstantes.seccion_g_p340_10,p340_10);
        contentValues.put(SQLConstantes.seccion_g_p340_11,p340_11);
        contentValues.put(SQLConstantes.seccion_g_p340_12,p340_12);
        contentValues.put(SQLConstantes.seccion_g_p340_13,p340_13);
        contentValues.put(SQLConstantes.seccion_g_p340_13_o,p340_13_o);
        contentValues.put(SQLConstantes.seccion_g_obs_g,obs_g);
        contentValues.put(SQLConstantes.modulo7_COB700,COB700);

        return contentValues;
    }
}
