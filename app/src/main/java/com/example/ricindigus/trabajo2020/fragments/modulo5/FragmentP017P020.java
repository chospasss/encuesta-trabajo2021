package com.example.ricindigus.trabajo2020.fragments.modulo5;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo5;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InputFilterSoloLetras;
import com.example.ricindigus.trabajo2020.util.NumericKeyBoardTransformationMethod;


public class FragmentP017P020 extends FragmentPagina {
    String idEncuestado;
    Context context;
    String idInformante;
    String idHogar;
    String idVivienda;
    LinearLayout layout_e_p017;
    LinearLayout layout_e_p018;
    LinearLayout layout_e_p019;
    LinearLayout layout_e_p019a;
    LinearLayout layout_e_p020;
    EditText et_e_P017;
    EditText et_e_P018;
    EditText et_e_P019;
    RadioGroup rg_e_P019a;
    EditText   et_e_P019a;
    RadioGroup rg_e_P020;
    EditText   et_e_P020;

    String p017;
    String p018;
    String p019;
    String p019a;
    String p019a_o;
    String p020;
    String p020_o;


    public FragmentP017P020() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentP017P020(String idEncuestado, Context context) {
        this.idEncuestado = idEncuestado;
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_e_p017_p020, container, false);
        layout_e_p017 = (LinearLayout) rootView.findViewById(R.id.layout_e_p017);
        layout_e_p018 = (LinearLayout) rootView.findViewById(R.id.layout_e_p018);
        layout_e_p019 = (LinearLayout) rootView.findViewById(R.id.layout_e_p019);
        layout_e_p019a = (LinearLayout) rootView.findViewById(R.id.layout_e_p019_a);
        layout_e_p020 = (LinearLayout) rootView.findViewById(R.id.layout_e_p020);
        et_e_P017    = rootView.findViewById(R.id.et_e_p017);
        et_e_P018    = rootView.findViewById(R.id.et_e_p018);
        et_e_P019    = rootView.findViewById(R.id.et_e_p019);
        rg_e_P019a   = rootView.findViewById(R.id.rg_e_p019a);
        et_e_P019a   = rootView.findViewById(R.id.et_e_p019a_o);
        rg_e_P020    = rootView.findViewById(R.id.rg_e_p020);
        et_e_P020    = rootView.findViewById(R.id.et_e_p020_o);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rg_e_P019a.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                controlarEspecifiqueRadio(radioGroup,i,9,et_e_P019a);
            }
        });

        rg_e_P020.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                controlarEspecifiqueRadio(radioGroup,i,5,et_e_P020);
            }
        });


        llenarVista();
        cargarDatos();
    }



    @Override
    public void guardarDatos() {
        Data data = new Data(context);
        data.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo5_id_informante,idInformante);
        contentValues.put(SQLConstantes.seccion_e_p317,p017);
        contentValues.put(SQLConstantes.seccion_e_p318,p018);
        contentValues.put(SQLConstantes.seccion_e_p319,p019);
        contentValues.put(SQLConstantes.seccion_e_p319a,p019a);
        contentValues.put(SQLConstantes.seccion_e_p319a_o,p019a_o);
        contentValues.put(SQLConstantes.seccion_e_p320,p020);
        contentValues.put(SQLConstantes.seccion_e_p320_o,p020_o);

        data.actualizarElemento(getNombreTabla(),contentValues,idEncuestado);
        data.close();
    }

    @Override
    public void llenarVariables() {
        p017 = et_e_P017.getText().toString();
        p018 = et_e_P018.getText().toString();
        p019 = et_e_P019.getText().toString();
        p019a   = rg_e_P019a.indexOfChild(rg_e_P019a.findViewById(rg_e_P019a.getCheckedRadioButtonId()))+"";
        p019a_o = et_e_P019a.getText().toString();
        p020    = rg_e_P020.indexOfChild(rg_e_P020.findViewById(rg_e_P020.getCheckedRadioButtonId()))+"";
        p020_o  = et_e_P020.getText().toString();
    }

    @Override
    public void cargarDatos() {
        Data data = new Data(context);
        data.open();
        if (data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo5 modulo5 = data.getModulo5(idEncuestado);
            et_e_P017.setText(modulo5.getP317());
            et_e_P018.setText(modulo5.getP318());
            et_e_P019.setText(modulo5.getP319());
            if(!modulo5.getP319a().equals("-1") && !modulo5.getP319a().equals(""))((RadioButton)rg_e_P019a.getChildAt(Integer.parseInt(modulo5.getP319a()))).setChecked(true);
            et_e_P019a.setText(modulo5.getP319a_o());
            if(!modulo5.getP320().equals("-1") && !modulo5.getP320().equals(""))((RadioButton)rg_e_P020.getChildAt(Integer.parseInt(modulo5.getP320()))).setChecked(true);
            et_e_P020.setText(modulo5.getP320_o());
        }
        data.close();
    }

    @Override
    public void llenarVista() {

    }

    @Override
    public boolean validarDatos() {
        llenarVariables();
        return true;
    }

    @Override
    public String getNombreTabla() {
        return SQLConstantes.tablamodulo5;
    }

    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void configurarEditText(final EditText editText, final View view, int tipo,int longitud){
        switch (tipo){
            case 0:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud), new InputFilterSoloLetras()});break;
            case 1:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud)});break;
            case 2:editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)});
                editText.setTransformationMethod(new NumericKeyBoardTransformationMethod());break;
        }

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ocultarTeclado(editText);
                    view.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void controlarEspecifiqueRadio(RadioGroup group, int checkedId, int opcionEsp, EditText editTextEspecifique) {
        int seleccionado = group.indexOfChild(group.findViewById(checkedId));
        if(seleccionado == opcionEsp){
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_enabled);
            editTextEspecifique.setEnabled(true);
        }else{
            editTextEspecifique.setText("");
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_disabled);
            editTextEspecifique.setEnabled(false);
        }
    }

    public void ocultarOtrosLayouts(){

    }

    public boolean verificarCoberturaCapitulo(){

        return true;
    }
}
